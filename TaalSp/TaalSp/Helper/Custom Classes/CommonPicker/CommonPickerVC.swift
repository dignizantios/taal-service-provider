//
//  CommonPickerVC.swift
//  Refferal
//
//  Created by Abhay on 26/03/18.
//

import UIKit
import Alamofire
//import AlamofireSwiftyJSON
import SwiftyJSON




protocol CommonPickerDelegate
{
    func setValuePicker(selectedDict:JSON,Type:selectionType)
    func setMultiValuePicker(selectedDict:[JSON])
    func setAppearTabbar()
}
enum selectionType{
    case month
    case year
}

class CommonPickerVC: UIViewController {

    //MARK: - Outlet
    
    @IBOutlet var pickerVw: UIPickerView!
    @IBOutlet var btnDone: UIButton!
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var vwBtns: UIView!
    
    //MARK: - Variable
    
    var strValueStore = String()
    var arrayPicker: [JSON] = []
    var arrayTwoPicker: [JSON] = []
    var pickerDelegate : CommonPickerDelegate?
    var selectedYear = String()
    var selectedMonth = String()
    var isComeFromExpiryMonth = Bool()
    var strType = ""
    var noOfComponant = 1
    var objSelectedType = selectionType.month
    //MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("Array : \(self.arrayPicker)")
         print("Array2 : \(self.arrayTwoPicker)")
        vwBtns.backgroundColor = UIColor.appThemeBlueColor
        
        pickerVw.reloadAllComponents()
        pickerVw.delegate = self
        pickerVw.dataSource = self
        
        btnDone.setTitle("Done_key".uppercased().localized, for: .normal)
        btnDone.titleLabel?.font = themeFont(size: 20, fontname: .regular)
        btnDone.setTitleColor(.white, for: .normal)
        
        btnCancel.setTitle("Cancel_key".uppercased().localized, for: .normal)
        btnCancel.titleLabel?.font = themeFont(size: 20, fontname: .regular)
        btnCancel.setTitleColor(.white, for: .normal)
        
    }
    
}

extension CommonPickerVC
{

    //MARK: - IBAction
    
    @IBAction func btnCancelTapped(sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
        pickerDelegate?.setAppearTabbar()
        
    }
    
    
    @IBAction func btnDoneTapped(sender: UIButton)
    {
        
        if noOfComponant == 2
        {
            var dics = [JSON]()
            dics.append(arrayPicker[pickerVw.selectedRow(inComponent: 0)])
            dics.append(arrayTwoPicker[pickerVw.selectedRow(inComponent: 1)])
            self.dismiss(animated: true, completion: nil)
            pickerDelegate?.setMultiValuePicker(selectedDict: dics)
            
            
        }else{
            let dict = arrayPicker[self.pickerVw.selectedRow(inComponent: 0)]
            self.dismiss(animated: true, completion: nil)
            pickerDelegate?.setValuePicker(selectedDict: dict, Type: objSelectedType)
        }
       
        
        
    }
    
}

extension CommonPickerVC: UIPickerViewDelegate, UIPickerViewDataSource
{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return noOfComponant
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if noOfComponant == 2
        {
            if component == 0 {
                return arrayPicker.count
            }
            else{
                return arrayTwoPicker.count
            }
        }
        return arrayPicker.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if noOfComponant == 2
        {
            if component == 0 {
                return arrayPicker[row]["value"].stringValue
            }
            return arrayTwoPicker[row]["value"].stringValue
        }
        
        return arrayPicker[row]["value"].stringValue
        
    }
    
    /*
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
       
        let attributedString = NSAttributedString(string: arrayPicker[row]["value"].stringValue, attributes: [NSAttributedString.Key.font : themeFont(size: 14, fontname: .regular)])
        
        return attributedString
    }*/
    
    /*
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let label: UILabel? = (view as? UILabel)
        label?.font = themeFont(size: 14, fontname: .regular)
        if noOfComponant == 2
        {
            if component == 0 {
                
                label?.text = arrayPicker[row]["value"].stringValue
                return label!
            }
            label?.text = arrayPicker[row]["value"].stringValue
            return label!
        }
        label?.text = arrayPicker[row]["value"].stringValue
        return label!
    }
    */
}






