//
//  CustomImagePicker.swift
//  Rent A SKI
//
//  Created by Haresh on 01/06/19.
//  Copyright © 2019 Haresh. All rights reserved.
//

import Foundation
import Photos
import MobileCoreServices
import UIKit

enum PickerType : Int {
    case onlyPhoto = 0
    case onlyVideo = 1
    case both      = 2
}

class CustomImagePicker: NSObject {
    
    var typeOfPicker : PickerType = PickerType.onlyPhoto
    
    fileprivate var theNavigationColor: UIColor? = nil
    fileprivate var theImagePicked: (([UIImagePickerController.InfoKey : Any]) -> Void)? = nil
    fileprivate var theImageCanceled: (() -> Void)? = nil
    fileprivate var theImageRemoved: (() -> Void)? = nil
    fileprivate var theDelegate: UIViewController? = nil
    
    func showImagePicker(fromViewController: UIViewController, navigationColor: UIColor?, imagePicked: @escaping ([UIImagePickerController.InfoKey : Any]) -> Void, imageCanceled: @escaping () -> Void, imageRemoved: (() -> Void)?) {
        
        theDelegate         = fromViewController
        theNavigationColor  = navigationColor
        theImagePicked      = imagePicked
        theImageCanceled    = imageCanceled
        theImageRemoved     = imageRemoved
        
        var strTitle = "Choose photo"
     
        if typeOfPicker == PickerType.both {
            strTitle = "Choose photo video"
        } else if typeOfPicker == PickerType.onlyVideo {
            strTitle = "Choose capture video"
        }
        
        let alertController = UIAlertController(title: strTitle, message: "", preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Photo album".capitalized, style: .default, handler: { (alertAction) in
            self.showGallery()
            })
        
        alertController.addAction(photoAction)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let cameraAction = UIAlertAction(title: "Camera", style: .default, handler: { (alertAction) in
                self.showCamera()
                })
            
            alertController.addAction(cameraAction)
        }
        
        let cancelAction = UIAlertAction(title: "Cancel_key".localized, style: .cancel, handler: { (alertAction) in
            if self.theImageCanceled != nil {
                self.theImageCanceled!()
            }
        })
        alertController.addAction(cancelAction)
        
        fromViewController.present(alertController, animated: true, completion: nil)
    }
    
    private func showGallery() {
        obtainPermissionForMediaSourceType(sourceType: .photoLibrary, success: {
            self.prepareForImagePicker(isForGallery: true)
        }, failed: {
            self.showNotAuthorizedAlert(isForGallery: true)
        })
    }
    
    private func showCamera() {
        obtainPermissionForMediaSourceType(sourceType: .camera, success: {
            self.prepareForImagePicker(isForGallery: false)
        }, failed: {
            self.showNotAuthorizedAlert(isForGallery: false)
        })
    }
    
    private func obtainPermissionForMediaSourceType(sourceType: UIImagePickerController.SourceType, success: @escaping () -> Void, failed: @escaping () -> Void) {
        
        if sourceType == .photoLibrary || sourceType == .savedPhotosAlbum {
            PHPhotoLibrary.requestAuthorization({ (authorizationStatus) in
                switch authorizationStatus {
                case .restricted:
                    failed()
                    
                case .denied:
                    failed()
                    
                case .authorized:
                    success()
                    
                default:
                    break
                }
            })
        }
        else if sourceType == .camera {
            let status = AVCaptureDevice.authorizationStatus(for: AVMediaType.video)
            
            switch status {
            case .authorized:
                success()
                
            case .notDetermined:
                AVCaptureDevice .requestAccess(for: AVMediaType.video, completionHandler: { (isGranted) in
                    if isGranted {
                        success()
                    }
                    else {
                        failed()
                    }
                })
                
            default:
                failed()
            }
        }
        else {
            assert(false, "Permisson not found")
        }
    }
    
    private func prepareForImagePicker(isForGallery: Bool) {
        let mediaUI = UIImagePickerController()
        mediaUI.sourceType = (isForGallery ? .photoLibrary : .camera)
        
        switch self.typeOfPicker {
        case .both:
            mediaUI.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            
        case .onlyVideo:
            mediaUI.mediaTypes = [kUTTypeMovie as String]
        default:
            mediaUI.mediaTypes = [kUTTypeImage as String]
        }
        
        mediaUI.delegate = self
        
        OperationQueue.main.addOperation {
            self.theDelegate?.present(mediaUI, animated: true, completion: nil)
        }
    }
    
    private func showNotAuthorizedAlert(isForGallery: Bool)
    {
        //        let msg = "You have disabled X access?"
        
        //        let theMessage = NSString(format: msg as NSString, "\(isForGallery ? "Photos" : "Camera")")
        //TODO:- Change message to language support
        let theMessage = "You have disabled \(isForGallery ? "Photos" : "Camera") access"
        
        let theAlertController = UIAlertController(title: "", message: theMessage as String, preferredStyle: .actionSheet)
        
        let openSettingsAction = UIAlertAction(title: "Open setting".capitalized,
                                               style: .default,
                                               handler: { (alertAction) in
                                                let theURL = URL(string: UIApplication.openSettingsURLString)
                                                UIApplication.shared.open(theURL!, options: [:], completionHandler: { (isOpened) in })
        })
        
        theAlertController.addAction(openSettingsAction)
        
        let cancelAction = UIAlertAction(title: "Cancel_key".localized,
                                         style: .default,
                                         handler: { (alertAction) in })
        
        theAlertController.addAction(cancelAction)
        
        self.theDelegate?.present(theAlertController, animated: true, completion: nil)
    }
    
}

extension CustomImagePicker : UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        theDelegate?.dismiss(animated: true, completion: nil)
        
        if info[UIImagePickerController.InfoKey.mediaURL] != nil, theImagePicked != nil {
            theImagePicked!(info)
        }
        if info[UIImagePickerController.InfoKey.originalImage] != nil, theImagePicked != nil {
            theImagePicked!(info)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
        theDelegate?.dismiss(animated: true, completion: nil)
        
        if theImageCanceled != nil {
            theImageCanceled!()
        }
    }
}

extension CustomImagePicker: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        
        if theNavigationColor != nil {
            navigationController.navigationBar.isTranslucent = false
            navigationController.navigationBar.barStyle      = .blackOpaque
            navigationController.navigationBar.barTintColor  = theNavigationColor!
            navigationController.navigationBar.tintColor     = UIColor.white
        }
    }
}
