//
//  LanguageHelper.swift
//  TaalSP
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

enum LanguageType: Int {
    case english
}

class Language {
    
    static let shared = Language()
    var isEnglish = true
    
    init() {
        isEnglish = true
    }
    
    func set(lang : LanguageType) {
        switch lang {
            
        case .english:
            isEnglish = true
            UserDefaults.standard.set(lang.rawValue, forKey: "")
            break
        }
    }
}

func localizedString(_ key: String) -> String {
    let language = "Base"

    let path = Bundle.main.path(forResource: language, ofType: "lproj")
    let bundle = Bundle(path: path!)
    
    return (bundle?.localizedString(forKey: key, value: "", table: nil))!
}

extension String {
    var localized: String {
         return NSLocalizedString(self, comment: "")
//        return localizedString(self)
    }
}

extension UILabel {
    @IBInspectable var LanguageKey: String {
        get {
            return self.text!
        }
        set {
            self.text = newValue.localized
        }
    }
}


extension UIButton {
    @IBInspectable var LanguageKey: String {
        get {
            return self.titleLabel?.text ?? ""
        }
        set {
            self.setTitle(newValue.localized, for: .normal)
        }
    }
}

extension UITextField {
    @IBInspectable var LanguageKey: String {
        get {
            return self.text!
        }
        set {
            self.text = newValue.localized
        }
    }
    
    @IBInspectable var LanguagePlaceholdeKey: String {
        get {
            return self.placeholder!
        }
        set {
            self.placeholder = newValue.localized
        }
    }
}
