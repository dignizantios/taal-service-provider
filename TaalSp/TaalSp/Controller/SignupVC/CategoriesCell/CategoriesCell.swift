//
//  CategoriesCell.swift
//  TaalSp
//
//  Created by Jaydeep on 29/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class CategoriesCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblCategoriesName: UILabel!
    @IBOutlet weak var imgSelection: UIImageView!
    @IBOutlet var btnSelected: UIButton!
    //MARK:- ViewLifeCycle

    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblCategoriesName].forEach { (lbl) in
            lbl?.textColor  = .appThemeBlueColor
            lbl?.font = themeFont(size: 17, fontname: .regular)
        }
        
        self.btnSelected.isSelected = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
