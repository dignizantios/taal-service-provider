//
//  SignUpView.swift
//  TaalSp
//
//  Created by Jaydeep on 22/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class SignUpView: UIView {
    
    //MARK:- Outlet
    
    @IBOutlet weak var lblWelcome: UILabel!
    @IBOutlet weak var lblCreateAccount: UILabel!
    @IBOutlet weak var txtUsername: CustomTextField!
    @IBOutlet weak var txtEmailAddress: CustomTextField!
    @IBOutlet weak var btnSelectCountryOutlet: UIButton!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var txtCategories: CustomTextField!
    @IBOutlet weak var lblTermsService: UILabel!
    @IBOutlet weak var btnSignUpOutlet: CustomButton!    
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var btnTermsOutlet: UIButton!
    
    @IBOutlet var vwCategories: UIView!
    @IBOutlet var btnCategoryShow: UIButton!
    @IBOutlet var tblCategory: UITableView!
    @IBOutlet var tblCategoryHeightConstraint: NSLayoutConstraint!
    
    
    //MARK:- Setup
    
    func setupUI(theDelegate:SignupVC){
        
        theDelegate.navigationController?.isNavigationBarHidden = false
        
        [txtUsername,txtEmailAddress,txtMobileNumber].forEach { (textField) in
            textField?.tintColor = .white
            textField?.delegate = theDelegate
        }
        theDelegate.addDoneButtonOnKeyboard(textfield: txtMobileNumber)
        
        lblWelcome.text = "Welcome_key".localized
        lblWelcome.textColor = .white
        lblWelcome.font = themeFont(size: 30, fontname: .bold)
        
        lblCreateAccount.text = "Create_new_account_key".localized.capitalized
        lblCreateAccount.textColor = .white
        lblCreateAccount.font = themeFont(size: 22, fontname: .regular)
        
        txtUsername.font = themeFont(size: 16, fontname: .regular)
        txtUsername.placeholder = "Username_key".localized.capitalized
        txtUsername.textColor = .white
        
        txtEmailAddress.font = themeFont(size: 16, fontname: .regular)
        txtEmailAddress.placeholder = "Your_email_address_key".localized.capitalized
        txtEmailAddress.textColor = .white
        
        txtCategories.font = themeFont(size: 16, fontname: .regular)
        txtCategories.placeholder = "Categories_key".localized
        txtCategories.textColor = .white
        
        txtMobileNumber.font = themeFont(size: 16, fontname: .regular)
        txtMobileNumber.textColor = .white
        
        btnSelectCountryOutlet.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        btnSelectCountryOutlet.setTitleColor(.white, for: .normal)
        
        btnSignUpOutlet.setTitle("Sign_up_key".localized, for: .normal)
        btnSignUpOutlet.setupThemeButtonUI()
        
        lblTermsService.font = themeFont(size: 15, fontname: .regular)
        lblTermsService.textColor = .white
        lblTermsService.text = "I_accept_the_terms_of_Service_key".localized
        
        vwOuter.roundCorners([.topLeft], radius:100)
        
        txtCategories.delegate = theDelegate
//        self.tblCategoryHeightConstraint.constant = 60*3 //(60-vell height)
    }
    
    func registerXib(theDelegate: SignupVC) {
        
        self.vwCategories.isHidden = true
        tblCategory.layer.cornerRadius = 10
        tblCategory.layer.borderColor = UIColor.black.cgColor
        tblCategory.layer.borderWidth = 1
        self.tblCategory.register(UINib(nibName: "CategoriesCell", bundle: nil), forCellReuseIdentifier: "CategoriesCell")
    }
    
    func btnSelectAccept(_ sender:UIButton){
        btnTermsOutlet.isSelected = !btnTermsOutlet.isSelected
    }

}
