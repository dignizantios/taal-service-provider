//
//  SignupVC_UITableviewDelegate.swift
//  TaalSp
//
//  Created by Vishal on 12/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit


//MARK: Tableview Delegate/Datasource

extension SignupVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.theCurrentModel.arrayCategoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell", for: indexPath) as! CategoriesCell
        
        let dict = self.theCurrentModel.arrayCategoryData[indexPath.row]
        
        cell.lblCategoriesName.text = dict.name
        
        if dict.isSelected == 0 {
            cell.btnSelected.isSelected = false
        }
        else {
            cell.btnSelected.isSelected = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let dict = self.theCurrentModel.arrayCategoryData[indexPath.row]
        
        if dict.isSelected == 1 {
            dict.isSelected = 0
        }
        else {
            
            let checkTotalCategory = self.theCurrentModel.arrayCategoryData.filter { (data) -> Bool in
                let categorySelected = data.isSelected == 1 ? true : false
                return categorySelected
            }
            
            if checkTotalCategory.count > 1 {
                makeToast(strMessage: "You can not add more then 2 categories!")
                return
            }
            else {
                dict.isSelected = 1
            }
            
        }
        
        self.theCurrentModel.arrayCategoryData[indexPath.row] = dict
        self.theCurrentModel.categoryText = ""
        self.theCurrentModel.arrayCategoryID = NSMutableArray()
        for data in self.theCurrentModel.arrayCategoryData {
            if data.isSelected == 1 {
                self.theCurrentModel.arrayCategoryID.add(data.id!)
                self.theCurrentModel.categoryText = self.theCurrentModel.categoryText + " \(data.name ?? "")" + ","
            }
        }
        if self.theCurrentModel.categoryText != "" {
            self.theCurrentModel.categoryText.remove(at: self.theCurrentModel.categoryText.index(before: self.theCurrentModel.categoryText.endIndex))
        }
        print("CategoryID:- \(self.theCurrentModel.arrayCategoryID)")
        self.theCurrentView.txtCategories.text = self.theCurrentModel.categoryText
        let indexPosition = IndexPath(row: indexPath.row, section: 0)
        tableView.reloadRows(at: [indexPosition], with: .none)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == self.theCurrentView.tblCategory {
            if tableView.contentSize.height >= (screenHeight/2) {
                self.theCurrentView.tblCategoryHeightConstraint.constant = (screenHeight/1.5)
                self.theCurrentView.tblCategory.isScrollEnabled = true
            }
            else {
                self.theCurrentView.tblCategory.isScrollEnabled = false
                self.theCurrentView.tblCategoryHeightConstraint.constant = CGFloat(self.theCurrentModel.arrayCategoryData.count*60)
            }
        }
    }
    
//    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//
//        if tableView.contentSize.height > 400 {
//            tableView.contentSize.height = 400
//        }
//        else {
//            self.theCurrentView.tblCategoryHeightConstraint.constant = CGFloat(self.theCurrentModel.arrayCategoryData.count*60)
//        }
//    }
}
