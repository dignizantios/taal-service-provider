//
//  SignupViewModel.swift
//  TaalSp
//
//  Created by Vishal on 11/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class SignupViewModel {
    
    //MARK: Variables
    fileprivate weak var theController: SignupVC!
    var arrayCategoryData : [TourDataModel] = []
    var categoryText = ""
    var arrayCategoryID = NSMutableArray()
    var signupDataDict = JSON()
    var countryCode = "+965"
    
    
    //MARK: Initializer
    init(theController: SignupVC) {
        self.theController = theController
    }
    
    
    func categoryList(completionHandler: @escaping()-> Void) {
        
        let url = categoryListURL
        let parameters : [String:Any] = [:]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            if let data = result as? NSArray {
                let jsonData = JSON(data)
                print("Result:-", jsonData)
                for value in jsonData.arrayValue {
                    let category = TourDataModel(JSON: value.dictionaryObject!)
                    self.arrayCategoryData.append(category!)
                }
            }
            var array : [JSON] = []
            for i in self.arrayCategoryData {
                if let data = i as? TourDataModel {
                    array.append(JSON(data.name ?? ""))
                }
            }
            completionHandler()
            print("Array:-\(array)")
            print("Error:-", error ?? "")
            print("statusCode:-", statusCode ?? 500)
        }
    }
    
    func checkIsMobileEmailExist(param : [String:Any], completionHandlor: @escaping()->Void) {
        
        let url = mobileEmailExist
        
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().MakePostAPI(name: url, params: param, header: header) { (result, error, statusCode) in
            self.theController.hideLoader()
            if let data = result {
                let jsonData = JSON(data)
                
                if jsonData["success"].intValue == 1 {
                    makeToast(strMessage: jsonData["message"].stringValue)
                    return
                }
                else {
                    completionHandlor()
                }
            }
            print("Error:-\(error ?? "")")
            print("StatusCode:-\(statusCode ?? 500)")
        }
    }
}
