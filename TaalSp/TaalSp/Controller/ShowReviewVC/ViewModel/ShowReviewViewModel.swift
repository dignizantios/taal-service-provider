//
//  ShowReviewViewModel.swift
//  Taal
//
//  Created by Vishal on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ShowReviewViewModel {
    
    fileprivate weak var theController: ShowReviewVC!
    
    //MARK: Initialized
    init(theController:ShowReviewVC) {
        self.theController = theController
    }
    var userID = ""
    var arrayReviewData : [ReviewDataModel] = []
    
    
    //MARK: API Setup
    func getRatingReviewsAPI(userID:String, completionHandlor:@escaping()->Void) {
        
        let url = getRatingReviewsUserURL+userID
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                self.arrayReviewData = []
                if let array = response as? NSArray {
                    let jsonArray = JSON(array).arrayValue
                    print("jsonArray: ", jsonArray)
                    for review in jsonArray {
                        let reviewData = ReviewDataModel(JSON: review.dictionaryObject!)
                        self.arrayReviewData.append(reviewData!)
                    }
                }
                else if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict).dictionaryValue
                    print("jsonDict: ", jsonDict)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
