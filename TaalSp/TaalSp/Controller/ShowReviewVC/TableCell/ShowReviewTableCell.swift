//
//  ShowReviewTableCell.swift
//  Taal
//
//  Created by Vishal on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import HCSStarRatingView

class ShowReviewTableCell: UITableViewCell {
    
    //MARK: Outlets
    @IBOutlet var imgUserPic: CustomImageView!
    @IBOutlet var lblUsername: UILabel!
    @IBOutlet var vwStarrating: HCSStarRatingView!
    @IBOutlet var lblReview: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblUsername].forEach { (lbl) in
            lbl?.font = themeFont(size: 18, fontname: .semi)
            lbl?.textColor = UIColor.black
        }
        
        [lblReview].forEach { (lbl) in
            lbl?.font = themeFont(size: 16, fontname: .regular)
            lbl?.textColor = UIColor.lightGray
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func setUpData(data: ReviewDataModel) {
        self.lblUsername.text = data.userName
        self.lblReview.text = data.comment
        self.vwStarrating.value = CGFloat(truncating: data.rating)
    }
}
