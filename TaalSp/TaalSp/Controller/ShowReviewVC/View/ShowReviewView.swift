//
//  ShowReviewView.swift
//  Taal
//
//  Created by Vishal on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class ShowReviewView: UIView {
    
    //MARK: Outlets
    @IBOutlet var btnDismiss: UIButton!
    @IBOutlet var tblReview: UITableView!
    @IBOutlet var vwPopUp: UIView!
    @IBOutlet var tblReviewHeightonstraint: NSLayoutConstraint!
    
    //MARK: SetUp
    func setUpUI(theController: ShowReviewVC) {
        
        self.registerXIb()
        tblReview.tableFooterView = UIView()
        
        vwPopUp.clipsToBounds = true
        vwPopUp.layer.cornerRadius = 35
        vwPopUp.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner] // Top right corner, Top left corner respectively

    }
    
    func registerXIb() {
        
        self.tblReviewHeightonstraint.constant = UIScreen.main.bounds.size.height/2
        self.tblReview.register(UINib(nibName: "ShowReviewTableCell", bundle: nil), forCellReuseIdentifier: "ShowReviewTableCell")
    }
    
}
