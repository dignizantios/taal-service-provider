//
//  ShowReviewVC.swift
//  Taal
//
//  Created by Vishal on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class ShowReviewVC: UIViewController {
    
    //MARK: Variables
    lazy var mainView: ShowReviewView = { [unowned self] in
        return self.view as! ShowReviewView
        }()
    
    lazy var mainModelView: ShowReviewViewModel = {
        return ShowReviewViewModel(theController: self)
    }()

    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.mainView.setUpUI(theController: self)
        self.mainView.tblReview.contentSize.height = 25
        
        self.mainModelView.getRatingReviewsAPI(userID: self.mainModelView.userID) {
            self.mainView.tblReview.reloadData()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.mainView.tblReview.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
        
    override func viewDidDisappear(_ animated: Bool) {
        
        self.mainView.tblReview.removeObserver(self, forKeyPath: "contentSize")
    }
        
    
    //MARK: Button action
    @IBAction func btnDissmisAction(_ sender: Any) {
        
        self.dismiss(animated: false, completion: nil)
    }
    
}
