//
//  TrackingVC.swift
//  TaalSp
//
//  Created by Jaydeep on 30/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps
import Polyline

class TrackingVC: UIViewController {
    
    //MARK:- Variable Declaration
    
    var strConversationId = String()
    var userLocation = CLLocationCoordinate2D()
    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwGoogleMaps: GMSMapView!
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        setupTrackingData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Tracking_key".localized, barColor: .appThemeBlueColor)
    }
    
    
    func setupTrackingData() {
        userDefault.setValue("1", forKey: "isTripStarted")
        userDefault.setValue(strConversationId, forKey: "convesationId")
        userDefault.synchronize()
        appdelegate.setUpQuickLocationUpdate()
        
        appdelegate.handlerLocationUpdate = {[weak self] lattitude,longitude,polyLine in
            guard self != nil else {
                return
            }
            self?.drawPath(strPolyLine: polyLine)
        }
    }

}

//MARK:- Create Path

extension TrackingVC {
    
    func setUpPinOnMap(lat:Double,long:Double,type:Int)  {
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        if type == 1 {
            marker.icon = UIImage.init(named: "ic_map_pin_one")
        } else if type == 2 {
            let cameraCoord = CLLocationCoordinate2D(latitude: lat, longitude: long)
//            self.mainView.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: cameraCoord, zoom: 18)
            let updateCamera = GMSCameraUpdate.setTarget(cameraCoord, zoom: Float(googleMapZoomLevel))
            self.vwGoogleMaps.animate(with: updateCamera)
            marker.icon = UIImage.init(named: "ic_map_pin")
        }
        else if type == 3{
            marker.icon = UIImage.init(named: "ic_map_pin_one")
        }
        marker.map = vwGoogleMaps
    }
    
    func drawPath(strPolyLine:String) {
        self.vwGoogleMaps.clear()
        let polyline = Polyline(encodedPolyline:strPolyLine)
        let decodedCoordinates: [CLLocationCoordinate2D] = polyline.coordinates!

        let path = GMSMutablePath()
        for i in 0..<decodedCoordinates.count {
            let dictFirst = decodedCoordinates[i]
            path.add(CLLocationCoordinate2D(latitude: dictFirst.latitude, longitude: dictFirst.longitude))
            if i == 0 {
                 self.setUpPinOnMap(lat: Double(dictFirst.latitude), long: Double(dictFirst.longitude), type: 1)
            } else if i == ((decodedCoordinates.count) - 1) {
                self.setUpPinOnMap(lat: Double(dictFirst.latitude), long: Double(dictFirst.longitude), type: 2)
                /*if self.isRideStarted == false
                {
                    let target = CLLocationCoordinate2D(latitude: dictFirst.latitude, longitude: dictFirst.longitude)
                    self.mainView.vwGoogleMaps.camera = GMSCameraPosition.camera(withTarget: target, zoom: 18)
                }*/
            }
        }
        
        setUpPinOnMap(lat: userLocation.latitude, long: userLocation.longitude, type: 3)
        let rectangle = GMSPolyline(path: path)
        rectangle.strokeWidth = 2
        rectangle.strokeColor = .appThemeRedColor
        rectangle.map = self.vwGoogleMaps
    }
    
    func fitAllMarkers(_path: GMSPath) {
        var bounds = GMSCoordinateBounds()
        for index in 1..._path.count() {
            bounds = bounds.includingCoordinate(_path.coordinate(at: index))
        }
        vwGoogleMaps.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 20))
    }
}

