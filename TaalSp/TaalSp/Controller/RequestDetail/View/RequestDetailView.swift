//
//  RequestDetailView.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class RequestDetailView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var lblRequestedTitle: UILabel!
    @IBOutlet weak var lblRequestNumber: UILabel!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var pageControlRequest: UIPageControl!
    @IBOutlet weak var lblVoiceNotes: UILabel!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDescriptionDetail: UILabel!
    @IBOutlet weak var lblUserDetailTitle: UILabel!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var btnWriteQuatationOutlet: CustomButton!
    @IBOutlet weak var vwCategoryImage: UIView!
    @IBOutlet weak var collectionRequestImage: UICollectionView!
    
    @IBOutlet var tblDescription: UITableView!
    @IBOutlet var tblDescriptionHeightConstraint: NSLayoutConstraint!
    @IBOutlet var vwAudio: UIView!
    @IBOutlet weak var btnRecord: UIButton!
    @IBOutlet weak var btnPlay: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var audioSlider: UISlider!
    @IBOutlet weak var timeLabel: UILabel!
    
    //MARK:- Setup
    
    
    func setupUI(){
        [lblRequestedTitle,lblRequestNumber,lblCategoryTitle,lblDescriptionTitle,lblDescriptionTitle,lblUserDetailTitle,lblVoiceNotes].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 17, fontname:.semi)
        }
        
        [lblDescriptionDetail,lblUserName,lblNameTitle,lblAddressTitle,lblUserAddress, timeLabel].forEach({ (lbl) in
             lbl?.textColor = .appThemeBlueColor
//            lbl?.font = themeFont(size: 12, fontname:.regular)
            lbl?.font = themeFont(size: 12, fontname:.semi)
            lbl?.alpha = 0.5
        })
        
        audioSlider.minimumValue = 0
        timeLabel.text = "0:0/0:0"
        
        lblVoiceNotes.text = "Voice_notes_key".localized
        
        lblUserDetailTitle.text = "User_details_key".localized.capitalized
        
        lblRequestedTitle.text = "Request_key".localized.uppercased()
        
        [lblCategoryName].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname:.regular)
            lbl?.alpha = 0.5
        }
        
        lblCategoryTitle.text = "Category_key".localized
        
        lblDescriptionTitle.text = "Description_key".localized
        
        [btnWriteQuatationOutlet].forEach { (btn) in
            btn?.backgroundColor = .appThemeLightOrangeColor
            btn?.setTitleColor(.white, for: .normal)
            btn?.setTitle("\("Write_your_key".localized.uppercased()) \("Quatation_key".localized.uppercased())", for: .normal)
            btn?.titleLabel?.font = themeFont(size: 18, fontname: .bold)
        }
      
        pageControlRequest.currentPage = 0
        pageControlRequest.isHidden = true
        
        tblDescription.register(UINib(nibName: "CategoryDescriptionTableCell", bundle: nil), forCellReuseIdentifier: "CategoryDescriptionTableCell")
        tblDescription.isUserInteractionEnabled = false
        collectionRequestImage.register(UINib(nibName: "RequestImageCell", bundle: nil), forCellWithReuseIdentifier: "RequestImageCell")
        collectionRequestImage.reloadData()
        
        audioSlider.isContinuous = false
        
        for state: UIControl.State in [.normal, .selected, .application, .reserved,.highlighted] {
            audioSlider.setThumbImage(UIImage(named: "ic_map_pin_tracking"), for: state)
        }
        
        self.vwMain.isHidden = true
        self.vwCategoryImage.isHidden = false
    }
    
    func setUpData(data: CategoryDataModel) {
        
        print("Data: ")
        print("Data: ", data)
        print("Data: ")
        
        self.vwMain.isHidden = false
        pageControlRequest.isHidden = false
        
        if data.hasAudio == 0 {
            self.vwAudio.isHidden = true
        }
        else {
            self.vwAudio.isHidden = false
        }
        
        if data.caseDescription.count == 0 {
            self.tblDescriptionHeightConstraint.constant = 0
        }
        
        self.lblRequestNumber.text = "\(data.caseId ?? 0)"
        self.lblCategoryName.text = data.categoryName
        self.lblUserName.text = data.userDetails?.name
        lblUserAddress.text = data.userDetails?.address
        
    }

    //Timer value set
    func timerAction(max: Float, min: Float){
        
        audioSlider.maximumValue = max
        audioSlider.minimumValue = 0
        audioSlider.layoutIfNeeded()
        
        audioSlider.setValue(min, animated: false)
    }

    
    func updatePageControl(index:Int){
        pageControlRequest.currentPage = index
    }
    
    func setupPageControl(totalPages:Int){
        pageControlRequest.hidesForSinglePage = true
        pageControlRequest.pageIndicatorTintColor = UIColor.lightGray
        pageControlRequest.currentPageIndicatorTintColor = UIColor.white
        pageControlRequest.numberOfPages = totalPages
    }

}
