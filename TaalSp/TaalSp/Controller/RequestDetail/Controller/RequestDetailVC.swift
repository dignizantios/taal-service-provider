//
//  RequestDetailVC.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import SDWebImage

class RequestDetailVC: UIViewController {
    
    //MARK:- Variable
    lazy var theCurrentView:RequestDetailView = { [unowned self] in
        return self.view as! RequestDetailView
        }()
    
    lazy var theCurrentModel: RequestDetailModel = {
        return RequestDetailModel(theController: self)
    }()
    
    var player: AVPlayer!
    var activitySound = NSURL()
    var updater : CADisplayLink! = nil // tracks the time into the track
    var updater_running : Bool = false // did the updater start?
    var playing : Bool = false //indicates if track was started playing
    var isPause : Bool = false
    var audioURL = ""
    
    //MARK:- ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.theCurrentModel.checkVCCome == .home {
            self.theCurrentModel.userCategoryDetailApi(id: self.theCurrentModel.caseID) {
                
                let data = self.theCurrentModel.categoryData?.caseDescription.filter({ (value) -> Bool in
                    return value.type == "image"
                })
                self.theCurrentModel.arrayImage = data?.first?.value ?? []
                if self.theCurrentModel.arrayImage.count == 0 {
                    self.theCurrentView.vwCategoryImage.isHidden = true
                }
                else {
                    self.theCurrentView.vwCategoryImage.isHidden = false
                }
                self.theCurrentView.setupPageControl(totalPages: self.theCurrentModel.arrayImage.count)
                self.theCurrentView.collectionRequestImage.reloadData()
                self.theCurrentView.tblDescription.reloadData {
                    self.theCurrentView.tblDescriptionHeightConstraint.constant = self.theCurrentView.tblDescription.contentSize.height
                }
                self.theCurrentView.setUpData(data: self.theCurrentModel.categoryData!)
            }
        }
        else {
            
        }
                
        theCurrentModel.setupArray()
        theCurrentView.setupUI()
//        theCurrentModel.setupAudioFile()
//        theCurrentView.setupPageControl(totalPages: theCurrentModel.arrRequestImage.count)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Request_details_key".localized, barColor: .appThemeBlueColor)
    }
    
    // method makes sure updater gets stopped when leaving view controller, because otherwise it will run indefinitely in background
    override func viewWillDisappear(_ animated: Bool) {
        if playing == true {
            self.theCurrentModel.audioRecording?.stop()
        }
        if updater != nil{
            updater.invalidate()
        }
        updater_running = false
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        if isPause {
            self.player.pause()
        }
    }

}

extension RequestDetailVC:AVAudioPlayerDelegate {
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        self.theCurrentView.btnRecord.isEnabled = true
        self.theCurrentView.btnStop.isEnabled = false
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Audio Play Decode Error")
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Audio Record Encode Error")
    }
}

//MARK:- Collection View Delegate

extension RequestDetailVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.theCurrentModel.arrayImage.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RequestImageCell", for: indexPath) as! RequestImageCell
        
        let img = self.theCurrentModel.arrayImage[indexPath.row]
        cell.imgRequest.sd_setShowActivityIndicatorView(true)
        cell.imgRequest.sd_setIndicatorStyle(.gray)
        
        cell.imgRequest.sd_setImage(with: img.toURL(), placeholderImage: UIImage(named: "ic_request_details_big_splash_holder"), options: .lowPriority, completed: nil)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        theCurrentModel.currentIndex = indexPath.row
        //        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
        theCurrentView.updatePageControl(index: theCurrentModel.currentIndex)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func redirectToCell(indexPath:IndexPath){
        theCurrentView.collectionRequestImage.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}

//MARK: UITableView Delegate/DataSource
extension RequestDetailVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let array = self.theCurrentModel.categoryData?.caseDescription.filter({ (data) -> Bool in
            
            let voice = data.type?.lowercased() != "Voice".lowercased()
            let img = data.type != "image"
            return voice && img
        })
        
        return array?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryDescriptionTableCell", for: indexPath) as! CategoryDescriptionTableCell
        
        
        let filter = self.theCurrentModel.categoryData?.caseDescription.filter { (dict) -> Bool in
            
            let voice = dict.type?.lowercased() != "Voice".lowercased()
            let img = dict.type != "image"
            return voice && img
        }
        
        let dict = filter?[indexPath.row]
        
        cell.setUpDetailsData(data: dict!)
//        cell.lblDescriptionTitle.text = "\(dict?.label ?? "") : "
//        cell.lblDescriptionValue.text = dict?.value?.first
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.theCurrentModel.categoryData?.caseDescription.count ?? 0 > 0 {
            self.theCurrentView.tblDescriptionHeightConstraint.constant = tableView.contentSize.height
        }
    }
}

//MARK:- Scrollview Delegate
extension RequestDetailVC : UIScrollViewDelegate {
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        theCurrentModel.currentIndex = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
//        theCurrentView.updatePageControl(index: theCurrentModel.currentIndex)
//    }
}

//MARK:- Action Zone

extension RequestDetailVC {
    
    @IBAction func btnWriteQuatationAction(_ sender:UIButton){
        
        if self.theCurrentModel.categoryData?.isAdd == "Yes" {
            makeToast(strMessage: "You can not add more then 2 quotation on same day")
        }
        else {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "WriteQuatationVC") as! WriteQuatationVC
            obj.theCurrentModel.addEditQuatation = .add
            self.navigationController?.pushViewController(obj, animated: true)
        }
    }
    
    @IBAction func btnStartSaveAudioAction(_ sender: Any) {

        if audioURL == "" {
            var data : CaseDescription?
            if self.theCurrentModel.categoryData?.caseDescription.count ?? 0 > 0 {
                if self.theCurrentModel.categoryData!.caseDescription.contains(where: { (json) -> Bool in
                    if (json.label != nil && json.label == "Voice_key".localized) || json.type?.capitalized == "Voice_key".localized {
                        data = json
                        return true
                    }
                    return false
                }) {
                    audioURL = data?.value?.first ?? ""
                }
            }
            print("AudioURL: ", audioURL)

            if audioURL != "" {
                self.playSound(urlAudio: audioURL)
            }
        }
        else {
            if isPause {
                isPause = false
                self.player.pause()
                self.theCurrentView.btnRecord.setImage(UIImage(named: "ic_voice_white_push"), for: .normal)
            }
            else {
                isPause = true
                self.theCurrentView.btnRecord.setImage(UIImage(named: "ic_voice_white"), for: .normal)
                self.player.play()
            }
        }
    }
    
    func setTimerObserver(_ player: AVPlayer?) {
        
        player?.addPeriodicTimeObserver(forInterval: CMTime.init(value: 1, timescale: 1), queue: .main, using: { (time) in
            if let duration = player?.currentItem?.duration {
                let duration = CMTimeGetSeconds(duration), time = CMTimeGetSeconds(time)
                if !duration.isNaN {
                    
                    self.theCurrentView.timerAction(max: Float(duration), min: Float(time))
                    
                    let data = self.secondsToHoursMinutesSeconds(seconds: Int(time))
                    let durationTime = self.secondsToHoursMinutesSeconds(seconds: Int(duration))
//                    print("Hours: ", data.0)
//                    print("Minutes: ", data.1)
//                    print("Seconds: ", data.2)
                    
                    self.theCurrentView.timeLabel.text = "\(data.1):"+"\(data.2)" + "/\(durationTime.1):\(durationTime.2)"
                    
                }
            }
        })
    }
    
    func playSound(urlAudio:String) {
        guard  let url = URL(string: urlAudio) else {
            self.audioURL = ""
//            makeToast(strMessage: "Invalid URL")
            print("Invalid URL: ")
            return
        }
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [])
            try AVAudioSession.sharedInstance().setActive(true)
            player = try AVPlayer(url: url as URL)
            guard let player = player
                else {
                return
            }

            self.isPause = true
            player.play()
            self.theCurrentView.btnRecord.setImage(UIImage(named: "ic_voice_white_push"), for: .normal)
            setTimerObserver(player)
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    ///--- ABHAY setup
    @IBAction func btnPlayStopAudioAction(_ sender: Any) {
        /* if self.mainModelView.audioRecorder?.isRecording == false {
         self.mainView.btnStop.isEnabled = true
         self.mainView.btnRecord.isEnabled = false
         
         do {
         try self.mainModelView.audioPlayer = AVAudioPlayer(contentsOf:
         (self.mainModelView.audioRecorder?.url)!)
         self.mainModelView.audioPlayer!.delegate = self
         self.mainModelView.audioPlayer!.prepareToPlay()
         self.mainModelView.audioPlayer!.play()
         } catch let error as NSError {
         print("audioPlayer error: \(error.localizedDescription)")
         }
         }*/
        if (playing == false) {
            updater = CADisplayLink(target: self, selector: #selector(updateProgress))//CADisplayLink(target: self, selector: Selector("updateProgress"))
            updater.frameInterval = 1
            updater.add(to: RunLoop.current, forMode: RunLoop.Mode.default)
            updater_running = true
            if self.theCurrentModel.audioRecording?.isRecording == false {
                self.theCurrentView.btnStop.isEnabled = true
                self.theCurrentView.btnRecord.isEnabled = false
                
                do {
                    try self.theCurrentModel.audioPlayer = AVAudioPlayer(contentsOf:
                        (self.theCurrentModel.audioRecording?.url)!)
                    self.theCurrentModel.audioPlayer!.delegate = self
                    self.theCurrentModel.audioPlayer!.prepareToPlay()
                    self.theCurrentModel.audioPlayer!.play()
                    self.theCurrentView.audioSlider.minimumValue = 0.0
                    self.theCurrentView.audioSlider.maximumValue = Float(self.theCurrentModel.audioPlayer?.duration ?? 100)
                    self.theCurrentView.timeLabel.text = "\(self.theCurrentModel.audioPlayer!.currentTime)"
                } catch let error as NSError {
                    print("audioPlayer error: \(error.localizedDescription)")
                    self.theCurrentModel.audioPlayer!.stop()
                }
            }
            //playButton.selected = true // pause image is assigned to "selected"
            playing = true
            updateProgress()
        } else {
            updateProgress()  // update track time
            self.theCurrentModel.audioPlayer?.pause()  // then pause
            //playButton.selected = false  // show play image (unselected button)
            playing = false // note track has stopped playing
        }
    }
    
    @IBAction func btnStopAudioAction(_ sender: Any) {
        self.theCurrentView.btnStop.isEnabled = false
        self.theCurrentView.btnPlay.isEnabled = true
        self.theCurrentView.btnRecord.isEnabled = true
        
        if self.theCurrentModel.audioRecording?.isRecording == true {
            self.theCurrentModel.audioRecording?.stop()
        } else {
            self.theCurrentModel.audioPlayer?.stop()
        }
    }
    // action for when the slider is moved: set as 'valuechanged' for UISlider object
    @IBAction func sliderMoved(sender: UISlider) {
        // if the track was playing store true, so we can restart playing after changing the track position
        var wasPlaying : Bool = false
        if playing == true {
            self.theCurrentModel.audioPlayer?.pause()
            wasPlaying = true
        }
        self.theCurrentModel.audioPlayer?.currentTime = TimeInterval(round(self.theCurrentView.audioSlider.value))
        updateProgress()
        // starts playing track again it it had been playing
        if (wasPlaying == true) {
            self.theCurrentModel.audioPlayer?.play()
            wasPlaying = false
        }
    }
    
    
    // Timer delegate method that updates current time display in minutes
    @objc func updateProgress() {
        print("time",self.theCurrentModel.audioPlayer?.duration)
        let total = Float(self.theCurrentModel.audioPlayer?.duration ?? 0/60)
        let current_time = Float(self.theCurrentModel.audioPlayer?.currentTime ?? 0/60)
        
        self.theCurrentView.audioSlider.setValue(Float(self.theCurrentModel.audioPlayer?.currentTime ?? 0), animated: true)
        //self.mainView.timeLabel.text = NSString(format: "%.2f/%.2f", current_time, total) as String
        //let normalizedTime = Float(self.mainModelView.audioPlayer!.currentTime * 40.0  / self.mainModelView.audioPlayer!.duration)
        //self.mainView.audioSlider.value = normalizedTime
        self.theCurrentView.timeLabel.text = NSString(format: "%.2f", total) as String
        
    }
    
    //- AVAudioPlayer delegate method - resets things when track finishe playing
    func PlayerDidFinishPlaying(player: AVAudioPlayer!, successfully flag: Bool) {
        if flag {
            self.theCurrentView.btnPlay.isEnabled = true
            playing = false
            self.theCurrentModel.audioPlayer?.currentTime = 0.0
            updateProgress()
            updater.invalidate()
        }
    }
}

