//
//  ChatDetailsViewModel.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ChatDetailsViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:ChatDetailsVC!
    
    //MARK: Initialized
    init(theController:ChatDetailsVC) {
        self.theController = theController
    }
    
    //MARK: Variables
    var arrayChat : [String] = []
    var isKeyboardOpen = false
    var tableviewTxtviewisClicked = false
    var arrayChatData : [ChatDataModel] = []
    var userID = ""
    var userFullName = ""
    var conversationId = ""
    var dictChatModel : ChatDataModel?
 
    
//        ChatDataModel(JSON: JSON().dictionaryObject!)
    
    func setupKeyboard() {
        NotificationCenter.default.addObserver(self, selector:#selector(self.keyboardWillShow(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
}

//MARK: API Setup
extension ChatDetailsViewModel {
    
    ///-- ChatHistory Message Data
    func chatHistory(userID: String, completionHandlor: @escaping()->Void) {
        
        let url = providerChatHistoryURl+userID
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: [:], header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let data = response as? NSArray {
                    let jsonArray = JSON(data).arrayValue
                    print("JsonArray: ", jsonArray)                  
                        
                    
                    self.arrayChatData = []
                    for value in jsonArray {
                        let dataValue = ChatDataModel(JSON: value.dictionaryObject!)
                        self.arrayChatData.append(dataValue!)
                    }
                }
                else if let data = response as? NSDictionary {
                    let jsonDict = JSON(data).dictionaryValue
                    print("JsonDict: ", jsonDict)
                }
                completionHandlor()
            }
            else if error != nil {
//                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                
            }
        }
    }
    
    func sendMessageToUserAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = sendMessageURL
        print("PARAMS: ", param)
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
//        self.theController.showLoader()
        
        WebServices().makePostApiWithBody(name: url, params: param, headers: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            
            if statusCode == success {
                if response != nil {
                    let data = JSON(response!).dictionaryObject
                    print("Dta: ", data!)
                }
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                makeToast(strMessage: "Server_not_responding_please_try_again_key".localized)
            }
//            else if statusCode == 500 {
//                makeToast(strMessage: "Server_not_responding_please_try_again_key".localized)
//                self.theController.mainView.activityIndicater.isHidden = true
//            }
        }
    }
}


//MARK: KeyBoard hide show in chatting
extension ChatDetailsViewModel {
    
    @objc func keyboardWillShow(_ notification: NSNotification) {
        
        let view = (self.theController.view as? ChatDetailsView)
        // Do something here
        if let keyboardRectValue = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            let keyboardHeight = keyboardRectValue.height
            isKeyboardOpen = true
            if tableviewTxtviewisClicked {
                view!.tblMessageChat.contentInset.bottom = keyboardHeight
                return
            }
            UIView.animate(withDuration: 1.5, animations: {
                var bottomPadding: CGFloat = 0.0
                if #available(iOS 11.0, *) {
                    let window = UIApplication.shared.keyWindow
                    bottomPadding = window?.safeAreaInsets.bottom ?? 0.0
                }
                view?.constantBottomofView.constant = keyboardHeight - bottomPadding
            }, completion: { (status) in
                //                self.scrollToBottom()
            })
        }
    }
    
    @objc func keyboardWillHide(_ notification: NSNotification) {
        let view = (self.theController.view as? ChatDetailsView)
        isKeyboardOpen = false
        if tableviewTxtviewisClicked {
            tableviewTxtviewisClicked = false
            view!.tblMessageChat.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            return
        }
        UIView.animate(withDuration: 1.5, animations: {
            view?.constantBottomofView.constant = 0.0
            view!.layoutIfNeeded()
        }, completion: nil)
    }
}
