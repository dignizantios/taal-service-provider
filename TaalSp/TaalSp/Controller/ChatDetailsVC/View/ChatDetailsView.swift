//
//  ChatDetailsView.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ChatDetailsView: UIView {
    
    
    //MARK: Outlets
    @IBOutlet weak var tblMessageChat: UITableView!
    @IBOutlet weak var vwBottom: UIView!
    @IBOutlet weak var btnSendMessage: UIButton!
    @IBOutlet weak var vwMessageBG: UIView!
    @IBOutlet weak var txtVwMessage: CustomTextview!
    @IBOutlet weak var txtVwHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTypeMessage: UILabel!
    @IBOutlet weak var constantBottomofView: NSLayoutConstraint!
    @IBOutlet weak var constrainViewTextViewBottom: NSLayoutConstraint!
    @IBOutlet weak var activityIndicater: UIActivityIndicatorView!
    
    //MARK: SetUpUI
    func setUpUI(theDelegate: ChatDetailsVC) {        
        
        activityIndicater.isHidden = true
        [lblTypeMessage].forEach { (lbl) in
            lbl?.textColor = UIColor.lightGray
            lbl?.font = themeFont(size: 13, fontname: .semi)
            lbl?.text = "YourMsg_key".localized
        }
        
        [vwMessageBG].forEach { (vw) in
            vw?.layer.cornerRadius = 6
            vw?.layer.borderColor = UIColor.lightGray.cgColor
            vw?.layer.borderWidth = 0.8
        }
        
        vwBottom.backgroundColor = .appThemeBlueColor
        
        [txtVwMessage].forEach { (txtVw) in
            txtVw?.tintColor = UIColor.appThemeBlueColor
            txtVw?.font = themeFont(size: 14, fontname: .regular)
            txtVw?.textColor = UIColor.black
        }
        
        self.btnSendMessage.setImage(UIImage(named: "ic_send_massage")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        self.btnSendMessage.alpha = 0.5
        self.btnSendMessage.isUserInteractionEnabled = false
        theDelegate.mainModelView.arrayChat.append("Dear, Where is your location? ")
        theDelegate.mainModelView.arrayChat.append(" Marinamall Beside Salmiya ")
        
        
        /*
         theDelegate.mainModelView.refreshControl = UIRefreshControl()
         theDelegate.mainModelView.refreshControl.backgroundColor = UIColor.clear
         theDelegate.mainModelView.refreshControl.tintColor = UIColor.appOrangeBtnColor
         tblMessageChat.addSubview(theDelegate.mainModelView.refreshControl)*/
    }
    
    func afterMessageSend(){
        activityIndicater.isHidden = true
        self.lblTypeMessage.text = "YourMsg_key".localized
        self.txtVwMessage.text = ""
        self.btnSendMessage.isUserInteractionEnabled = true
        self.btnSendMessage.alpha = 0.5
    }
    
    //MARK: RegisterXIB
    func registertXIb(theDelegate: ChatDetailsVC) {
        
        tblMessageChat.register(UINib(nibName: "LeftChatCell", bundle: nil), forCellReuseIdentifier: "LeftChatCell")
        
        tblMessageChat.register(UINib(nibName: "RightChatCell", bundle: nil), forCellReuseIdentifier: "RightChatCell")
    }
    
    //MARK: Table Last data show
    func scrollToBottom(theDelegate: ChatDetailsVC) {
        
        DispatchQueue.main.async {
            /*if theDelegate.mainModelView.arrayChatMsg.count > 0{
                let indexPath = IndexPath(row: theDelegate.mainModelView.arrayChatMsg.count-1, section: 0)
                self.tblMessageChat.scrollToRow(at: indexPath, at: .bottom, animated: false)
            }*/
        }
        
    }
}
