//
//  ChatDetailsVC_UITextViewDelegate.swift
//  Taal
//
//  Created by Abhay on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

extension ChatDetailsVC : UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        
        if textView == mainView.txtVwMessage {
            var txtHeight = mainView.txtVwHeightConstraint.constant
            let fixedWidth = textView.frame.size.width
            
            let newSize = textView.sizeThatFits(CGSize.init(width: fixedWidth, height: CGFloat(MAXFLOAT)))
            var newFrame = textView.frame
            
            newFrame.size = CGSize.init(width: CGFloat(fmaxf(Float(newSize.width), Float(fixedWidth))), height: newSize.height)
            
            if (newSize.height <= 81) {
                mainView.txtVwMessage.isScrollEnabled = false
                mainView.txtVwHeightConstraint.constant = newSize.height
                txtHeight = newSize.height
            }
            else {
                mainView.txtVwMessage.isScrollEnabled = true
                mainView.txtVwHeightConstraint.constant = txtHeight
            }
            
            mainView.lblTypeMessage.isHidden = textView.text == "" ? false : true
        }
        
        if mainView.lblTypeMessage.isHidden == false {
            self.mainView.btnSendMessage.alpha = 0.5
            self.mainView.btnSendMessage.isUserInteractionEnabled = false
        }
        else {
            self.mainView.btnSendMessage.alpha = 1.0
            self.mainView.btnSendMessage.isUserInteractionEnabled = true
        }
        
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        if (text == "\n") {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }    
    
}
