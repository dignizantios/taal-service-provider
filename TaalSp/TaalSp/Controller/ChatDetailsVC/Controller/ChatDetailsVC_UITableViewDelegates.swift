//
//  ChatDetailsVC_UITableViewDelegates.swift
//  Taal
//
//  Created by Abhay on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import SDWebImage

extension ChatDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.mainModelView.arrayChatData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.mainModelView.arrayChatData.count == 0 {
            let lbl = UILabel()
            lbl.text = "Not chat yet..!"
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlueColor
            lbl.font = themeFont(size: 16.0, fontname: .regular)
            lbl.center = tableView.center
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.arrayChatData[section].chat?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let dict = self.mainModelView.arrayChatData[indexPath.section].chat?[indexPath.row]
        
        let leftCell = tableView.dequeueReusableCell(withIdentifier: "LeftChatCell", for: indexPath) as! LeftChatCell
        let rightCell = tableView.dequeueReusableCell(withIdentifier: "RightChatCell", for: indexPath) as! RightChatCell
        
        if dict?.reply == true {
            leftCell.lblMsg.text = dict?.message
            leftCell.lblTime.text = dict?.time
            leftCell.selectionStyle = .none
            return leftCell
        }
        else {
            rightCell.lblMsg.text = dict?.message
            rightCell.lblTime.text = dict?.time
            rightCell.selectionStyle = .none
            return rightCell
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*let dict = self.mainModelView.arrayChatMsg[indexPath.row]
        if dict.msgType == "img" && dict.imgMSG != ""{
            let ImgOpenVC = AppStoryboard.User.instance.instantiateViewController(withIdentifier: "ImgOpenVC") as! ImgOpenVC
            ImgOpenVC.mainModelView.selectedImg = dict.imgMSG!
            self.mainModelView.arrayChatMsg.filter { (chat) -> Bool in
                if chat.msgType == "img" {
                    chat.toName = self.mainModelView.fromName
                    ImgOpenVC.mainModelView.imgChatArray.append(chat)
                    return true
                }
                return false
            }
            ImgOpenVC.hidesBottomBarWhenPushed = true
            self.navigationController?.present(ImgOpenVC, animated: false, completion: nil)
            
        }*/
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section < self.mainModelView.arrayChatData.count {
            return self.mainModelView.arrayChatData[section].date
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 50))

        let label = UILabel()
        label.frame = CGRect.init(x: 5, y: 5, width: headerView.frame.width-10, height: headerView.frame.height-10)
        label.text = self.mainModelView.arrayChatData[section].date
        label.textAlignment = .center
        label.font = themeFont(size: 13, fontname: .regular)
        label.textColor = UIColor.lightGray
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
      /*  let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        //print("offsetY: \(offsetY) | contHeight-scrollViewHeight: \(contentHeight-scrollView.frame.height)")
        //if offsetY > contentHeight - scrollView.frame.height - 50 {
        if offsetY <= 0
        {
            // Bottom of the screen is reached
            print("fetchingMore:-->",self.mainModelView.fetchingMore)
            if !self.mainModelView.fetchingMore {
                self.getMSGData()
                self.previousKey = self.lastMessageKey
            }
        }*/
    }
}
