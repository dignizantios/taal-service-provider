//
//  CountryCodeVC_UITextFieldDelegate.swift
//  Liber
//
//  Created by Khushbu on 13/07/19.
//  Copyright © 2019 Abhay. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension CountryCodeVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let text = textField.text,
            let textRange = Range(range, in: text) {
            let updatedText = text.replacingCharacters(in: textRange, with: string)
            print(updatedText)
            if updatedText == "" {
                mainView.isFiltered = false
                self.mainView.tblCountry.tableFooterView = UIView()
                self.mainView.tblCountry.reloadData()
            }
            else {
                mainView.isFiltered = true
                let jobj = mainView.countryList.arrayValue
                if !jobj.isEmpty {
                    let j = jobj.filter({ (json) -> Bool in
                        return json["name"].stringValue.lowercased().contains(updatedText.lowercased()) || json["dial_code"].stringValue.lowercased().contains(updatedText.lowercased()) ||
                            json["code"].stringValue.lowercased().contains(updatedText.lowercased())
                    })
                    self.mainView.filteredCountryList.arrayObject = j
                    self.mainView.tblCountry.tableFooterView = UIView()
                    self.mainView.tblCountry.reloadData()
                }
            }
        }
        return true
    }
    
}
