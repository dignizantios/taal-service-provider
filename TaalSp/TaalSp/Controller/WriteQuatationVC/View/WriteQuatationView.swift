//
//  WriteQuatationView.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class WriteQuatationView: UIView {

    //MARK:- Setup UI
    
    @IBOutlet weak var lblVoiceNoteTitle: UILabel!
    @IBOutlet weak var btnRecord: RecordButton!
    @IBOutlet weak var vwAudioRecording: CustomView!
    @IBOutlet weak var lblOfferedTitle: UILabel!
    @IBOutlet weak var txtOfferPrice: CustomTextField!
    @IBOutlet weak var lblExtraNotesTitle: UILabel!
    @IBOutlet weak var txtExtraNotes: CustomTextview!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    @IBOutlet weak var lblAudioName: UILabel!
    
    //MARK:- Setup UI
    
    func setupUI(theController: WriteQuatationVC){
        
        [lblVoiceNoteTitle,lblOfferedTitle,lblExtraNotesTitle].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 17, fontname:.semi)
        }
        
        [btnRecord].forEach { (btn) in
            btn?.layer.cornerRadius = 4
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        
        lblVoiceNoteTitle.text = "Send_voice_note_key".localized.capitalized
        lblOfferedTitle.text = "Offered_price_key".localized.capitalized + "*"
        lblExtraNotesTitle.text = "Extra_notes_key".localized.capitalized
        
        [btnContinueOutlet].forEach { (btn) in
            btn?.backgroundColor = .appThemeLightOrangeColor
            btn?.setTitleColor(.white, for: .normal)
            btn?.setTitle("\("Continue_key".localized.uppercased())", for: .normal)
            btn?.titleLabel?.font = themeFont(size: 20, fontname: .bold)
        }
        
        [txtOfferPrice].forEach { (txtField) in
            txtField?.textColor = .black
            txtField?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [txtExtraNotes].forEach { (txtView) in
            txtView?.textColor = .black
            txtView?.font = themeFont(size: 15, fontname: .regular)
        }
        
        [lblAudioName].forEach { (lbl) in
            lbl?.textColor = .black
            lbl?.font = themeFont(size: 15, fontname:.regular)
        }
        self.lblAudioName.text = ""
        
        var arrPrize: [String] = []
        for number in (5...200) {
            guard number % 5 == 0 else {
                continue
            }
            arrPrize.append("\(number) KD")
        }
        
        theController.theCurrentModel.prizeDeropDown.dataSource = arrPrize
        theController.theCurrentModel.prizeDeropDown.selectionAction = { (index, item) in
            self.txtOfferPrice.text = item
            theController.view.endEditing(true)
            theController.theCurrentModel.prizeDeropDown.hide()
        }
        
    }
    
    func setUpData(checkAddEdit: enumAddEditQuatation) {
        
        if checkAddEdit == .add {
            print("Add")
        }
        else if checkAddEdit == .editPending {
            print("Edit Pending")
        }
        else if checkAddEdit == .editAccepted {
            print("Edit Accepted")
        }
    }
    
    func setUpValue(data: CategoryDataModel) {
        self.lblAudioName.text = data.voiceNotes
        self.txtOfferPrice.text = data.price
        self.txtExtraNotes.text = data.extraNotes
    }
}
