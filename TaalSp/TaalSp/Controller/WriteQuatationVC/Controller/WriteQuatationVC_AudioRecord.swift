//
//  WriteQuatationVC_AudioRecord.swift
//  TaalSp
//
//  Created by Vishal on 27/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import CoreMIDI


//MARK: Audio record Animation setup
extension WriteQuatationVC : RecordViewDelegate {
 
    func setupAudioRecord() {
        
        self.theCurrentView.btnRecord.translatesAutoresizingMaskIntoConstraints = false
        
        self.theCurrentModel.recordView.translatesAutoresizingMaskIntoConstraints = false
        self.theCurrentView.vwAudioRecording.addSubview(self.theCurrentModel.recordView)
                
        self.theCurrentModel.recordView.centerYAnchor.constraint(equalTo: self.theCurrentView.vwAudioRecording.centerYAnchor, constant: 8).isActive = true

        self.theCurrentModel.recordView.trailingAnchor.constraint(equalTo: self.theCurrentView.vwAudioRecording.trailingAnchor, constant: -30).isActive = true
        self.theCurrentModel.recordView.leadingAnchor.constraint(equalTo: self.theCurrentView.vwAudioRecording.leadingAnchor, constant: 0).isActive = true
        
        self.theCurrentView.btnRecord.recordView = self.theCurrentModel.recordView
        
        self.theCurrentModel.recordView.delegate = self
        
    }
    
    func onStart() {
        
        print("onStart")
        [self.theCurrentView.btnRecord].forEach { (btn) in
            btn?.layer.cornerRadius = 25
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.red
        }
        
        self.theCurrentView.lblAudioName.text = ""
        self.setUpRecorder()
    }
    
    func onCancel() {
        
        print("onCancel")
        [self.theCurrentView.btnRecord].forEach { (btn) in
            btn?.layer.cornerRadius = 4
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        self.theCurrentView.lblAudioName.text = "Voice"
        
        self.theCurrentModel.audioRecorder.deleteRecording()
    }
    
    func onFinished(duration: CGFloat) {
        
        print("onFinished \(duration)")
        [self.theCurrentView.btnRecord].forEach { (btn) in
            btn?.layer.cornerRadius = 4
            btn?.clipsToBounds = true
            btn?.backgroundColor = UIColor.appThemeLightOrangeColor
        }
        
        self.theCurrentView.lblAudioName.text = self.theCurrentModel.fileName
        
        self.theCurrentModel.audioRecorder.stop()
        let url = self.theCurrentModel.audioRecorder.url
        print("URL: ", url)
        
        self.showLoader()
        DispatchQueue.main.async {
            do {
                let base64 = try Data(contentsOf: url).base64EncodedString()
                dataCategoriData?.audioBase64 = base64
                print("Base64: ", base64)
                self.hideLoader()
            }
            catch {
                print("Error")
            }
        }
    }
    
    func onAnimationEnd() {
        print("onAnimationEnd")
    }
}


//MARK: Audio Setup
extension WriteQuatationVC : AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    
    func getDocumentDirectory() -> URL {
        
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentDirectory = paths[0]
        return documentDirectory
    }
    
    func getFileURL() -> URL {
        
        let fileName = self.theCurrentModel.fileName
        let filePath = getDocumentDirectory().appendingPathComponent(fileName)
        return filePath
    }
        
    func setUpRecorder() {
        
        self.theCurrentModel.recordingSession = AVAudioSession.sharedInstance()
        do {
            try self.theCurrentModel.recordingSession?.setCategory(AVAudioSession.Category.playAndRecord)
            try self.theCurrentModel.recordingSession.setActive(true)
            
            let settings = [AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
            AVSampleRateKey: 12000,
            AVNumberOfChannelsKey: 1,
            AVEncoderAudioQualityKey: AVAudioQuality.high.rawValue
            ]
            
            self.removeData()
            
            self.theCurrentModel.audioRecorder = try AVAudioRecorder(url: getFileURL(), settings: settings)
            self.theCurrentModel.audioRecorder.delegate = self
            self.theCurrentModel.audioRecorder.isMeteringEnabled  = true
            self.theCurrentModel.audioRecorder.prepareToRecord()
            self.theCurrentModel.audioRecorder.record()
            
        }
        catch let error {
            print("ERROR: ", error.localizedDescription)
        }
    }
    
    ///--- In directory remove last all record file
    func removeData() {
        
        let documentsUrl =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!

        do {
            let fileURLs = try FileManager.default.contentsOfDirectory(at: documentsUrl,
                                                                       includingPropertiesForKeys: nil,
                                                                       options: [.skipsHiddenFiles, .skipsSubdirectoryDescendants])
            for fileURL in fileURLs {
                if fileURL.pathExtension == "m4a" {
                    try FileManager.default.removeItem(at: fileURL)
                }
            }
        } catch  { print(error) }
    }
}
