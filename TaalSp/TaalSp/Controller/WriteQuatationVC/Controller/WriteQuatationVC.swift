//
//  WriteQuatationVC.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class WriteQuatationVC: UIViewController {

    //MARK:- Variable
    lazy var theCurrentView:WriteQuatationView = { [unowned self] in
        return self.view as! WriteQuatationView
        }()
    
    lazy var theCurrentModel: WriteQuatationModel = {
        return WriteQuatationModel(theController: self)
    }()
    
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI(theController: self)
        self.setupAudioRecord()
        
        if self.theCurrentModel.addEditQuatation == .editPending {
            self.theCurrentModel.getQuotationData(quotationID: "\(self.theCurrentModel.dictQuotationData?.quotationId ?? 0)") {
                
                if self.theCurrentModel.dictQuotationDetailData != nil {
                    
                    let str = self.theCurrentModel.dictQuotationDetailData?.voiceUrl
                    let url = URL(string: str!)
                    DispatchQueue.main.async {
                        do {
                            let base64 = try Data(contentsOf: url!).base64EncodedString()
                            dataCategoriData?.audioBase64 = base64
                            print("Base64: ", base64)
                        }
                        catch {
                            print("Error")
                        }
                    }
                    self.theCurrentView.setUpValue(data: self.theCurrentModel.dictQuotationDetailData!)
                }
                print("Response get")
            }
        }
        else {
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "\("Write_your_key".localized) \("Quatation_key".localized.localizedLowercase)", barColor: .appThemeBlueColor)
    }
    
    override func viewDidLayoutSubviews() {
        configuDropDown(dropDown: self.theCurrentModel.prizeDeropDown, sender: self.theCurrentView.txtOfferPrice)
    }

}

//MARK: TextField Delegate
extension WriteQuatationVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == theCurrentView.txtOfferPrice {
            self.view.endEditing(true)
            self.theCurrentModel.prizeDeropDown.show()
            return false
        }
        return true
    }
    
}

//MARK:- Action Zone

extension WriteQuatationVC {

    @IBAction func btnContinueAction(_ sender:UIButton) {
        
        if self.theCurrentModel.addEditQuatation == .add {
            
            if dataCategoriData?.audioBase64 == "" || ((self.theCurrentView.txtOfferPrice.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) || self.theCurrentView.txtExtraNotes.text == "" || self.theCurrentView.txtExtraNotes.text == nil {
                makeToast(strMessage: "enter value")
            }
            else {
                dataCategoriData?.price = self.theCurrentView.txtOfferPrice.text ?? ""
                dataCategoriData?.quotationExtraNotes = self.theCurrentView.txtExtraNotes.text ?? ""
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
                obj.setupData(selectedController: .addQuotation)
                self.navigationController?.pushViewController(obj, animated: true)
            }
        }
        else if self.theCurrentModel.addEditQuatation == .editPending {
            
            let param = ["price": self.theCurrentView.txtOfferPrice.text ?? "",
                         "extra_notes": self.theCurrentView.txtExtraNotes.text ?? "",
                         "voice_notes": dataCategoriData?.audioBase64 ?? ""]

            print("PARAM: ", param)
            self.theCurrentModel.editQuatationAPI(quotationID: "\(self.theCurrentModel.dictQuotationDetailData?.quotationId ?? 0)", params: param) {
                
                isCategoryDelete = true
                guard let homeVC = self.navigationController?.getReferenceVC(to: RequestHistoryVC.self) else { return }
                self.navigationController?.popToViewController(homeVC, animated: true)
            }
        }
        else if self.theCurrentModel.addEditQuatation == .editAccepted {
            
        }
//        let obj = self.storyboard?.instantiateViewController(withIdentifier: "InvoiceVC") as! InvoiceVC
//        self.navigationController?.pushViewController(obj, animated: true)
    }
}
