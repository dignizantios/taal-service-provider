//
//  WriteQuatationModel.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMIDI
import DropDown
import SwiftyJSON

class WriteQuatationModel: NSObject {

    //MARK:- Variable
    fileprivate weak var theController:WriteQuatationVC!
    let recordView = RecordView()
    var addEditQuatation = enumAddEditQuatation.add
    var dictQuotationData: CategoryDataModel?
    var dictQuotationDetailData: CategoryDataModel?
    var audioRecorder: AVAudioRecorder!
    var audioPlayer: AVAudioPlayer!
    var recordingSession: AVAudioSession!
    var fileName : String = "audio.m4a"
    var prizeDeropDown = DropDown()
    
    //MARK:- LifeCycle
    init(theController:WriteQuatationVC) {
        self.theController = theController
    }
    
    //MARK: AddQuotation API
    func addQuotationURL(parameters: [String:Any], completionHandlor: @escaping() -> Void) {
        
        let url = addQuotationsURL
        print("URL :", url)
        print("param: ", parameters)
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        print("header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().makePostApiWithBody(name: url, params: parameters, headers: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
            }
        }
    }
    
    
    //MARK: Edit Quotation get data
    func getQuotationData(quotationID:String, completionHandlor: @escaping() -> Void) {
        
        let url = editQuatationGetDataURL+quotationID
        print("URL: ", url)
        let parameters: [String:Any] = [:]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        print("header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            
            if statusCode == success {
                
                if let data = response as? NSDictionary {
                    let jsonData = JSON(data).dictionaryObject
                    print("JSON: ", jsonData!)
                    self.dictQuotationDetailData = CategoryDataModel(JSON: jsonData!)
                }
                completionHandlor()
            } else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                
            }
        }
    }
    
    //MARK: Edit Quotation
    func editQuatationAPI(quotationID:String, params: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = editQuatationURL+quotationID
        print("URL: ", url)
        print("PARAM: ", params)
        var header: [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        print("header: ", header)
        
        self.theController.showLoader()
        
        WebServices().MakePutAPI(name: url, params: params, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            
            if statusCode == success {
                if let data = response {
                    let jsonData = JSON(data).dictionaryObject
                    print("JSON: ", jsonData!)
                }
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else if statusCode == unprocessableEntity {
                if let data = response as? NSDictionary {
                    makeToast(strMessage: data["error"] as! String)
                }
            }
        }
    }
}
