//
//  OTPVC.swift
//  TaalSp
//
//  Created by Jaydeep on 23/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import FirebaseAuth
import FirebaseMessaging

class OTPVC: UIViewController {
    
    //MARK:- Variable
    lazy var theCurrentView:OTPView = { [unowned self] in
        return self.view as! OTPView
        }()
    
    lazy var theControllerModel: OTPViewModel = {
        return OTPViewModel(theController: self)
    }()
    var enteredOTP : String = ""
    
    
    //MARK:- ViewLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        theCurrentView.setupUI(theDelegate: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        setupTransparentNavigationBar()
        
//        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back") , style: .plain, target: self, action: #selector(backButtonTapped))
//        leftButton.tintColor = .black
//        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        let leftButton = UIBarButtonItem(image: UIImage(named: "ic_back")?.imageFlippedForRightToLeftLayoutDirection() , style: .plain, target: self, action: #selector(backButtonTapped))
        leftButton.tintColor = .black
        self.navigationItem.leftBarButtonItem = leftButton
        //        theCurrentView.setupUI()
    }
    
    func verifyOTPCode() {
        
        self.showLoader()
        
        let verificationID = userDefault.value(forKey: "verificationId")
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID as! String, verificationCode: enteredOTP)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            
            self.hideLoader()
            
            if error == nil {
                
                if user != nil {
                    
                    user?.user.getIDToken(completion: { (data, error) in
                        if error == nil {
                            
//                            if self.theControllerModel.selectController != .update {
//
//                            }
                            
                            let token : String = data ?? ""
                            userDefault.set(token, forKey: "idToken")
                            idToken = userDefault.value(forKey: "idToken") as! String
                            print("IdToken:", token)
                            print("")
                            
                            if self.theControllerModel.selectController == .login {
                                self.loginApi()
                            }
                            else if self.theControllerModel.selectController == .register {
                                self.registerApi()
                            }
                            else if self.theControllerModel.selectController == .update {
                                self.updateMobileNumberApi()
                            }
                        }
                    })
                    
//                    let uID : String = user?.user.uid ?? ""
//                    userDefault.set(uID, forKey: "idToken")
//                    print("uID:-", uID)
//
//                    let userToken = user?.user.refreshToken
//                    userDefault.set(userToken, forKey: "idToken")
//                    print("userToken:-",userToken!)
                    
                    print("Verify Successfully")
                    
                    let firebaseAuth = Auth.auth()
                    do {
                        try firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                }
            }
            else if error != nil {
                makeToast(strMessage: error?.localizedDescription ?? "")
                print("Error:-\(error?.localizedDescription as Any)")
                return
            }
        }
        
    }
    
    func loginApi() {
        
        let param = ["idToken":idToken,
                     "mobile":self.theControllerModel.signupDataDict["mobile"].stringValue,
                     "country_code":self.theControllerModel.signupDataDict["country_code"].stringValue,
                     "player_id":Messaging.messaging().fcmToken ?? "",
                     "device_type":deviceType] as [String : Any]
        print("Param:-", param)
        
        self.theControllerModel.loginDataAPI(parameters: param) {
             self.hideLoader()
            appdelegate.tabBarVC = TaalTabBarVC()
            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
            appdelegate.window?.rootViewController = navigation
        }
        
    }
    
    func registerApi() {
        
        let param = ["idToken":idToken,
                     "fullname":self.theControllerModel.signupDataDict["fullname"].stringValue,
                     "email":self.theControllerModel.signupDataDict["email"].stringValue,
                     "mobile":self.theControllerModel.signupDataDict["mobile"].stringValue,
                     "category_ids":self.theControllerModel.arrayCategoryID as NSArray,
                     "country_code":self.theControllerModel.signupDataDict["country_code"].stringValue,
                     "player_id":Messaging.messaging().fcmToken ?? "",
                     "device_type":deviceType] as [String : Any]
        
        print("Param:-", param)
        
        self.theControllerModel.registerAPI(parameters: param, completionHandlor: {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: LoginVC.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
//            appdelegate.tabBarVC = TaalTabBarVC()
//            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
//            appdelegate.window?.rootViewController = navigation
        })
    }
    
    func updateMobileNumberApi() {
        let param = ["idToken":idToken,
                     "mobile":self.theControllerModel.signupDataDict["mobile"].stringValue,
                     "country_code":theControllerModel.signupDataDict["country_code"].stringValue] as [String : Any]
        print("Param:-", param)
        
        self.theControllerModel.updateMobileNumberAPI(param: param) {
            for controller in self.navigationController!.viewControllers as Array {
                if controller.isKind(of: ProfileVC.self) {
                    self.navigationController?.popToViewController(controller, animated: true)
                    break
                }
            }
        }
    }
}

//MARK:- Action Zone

extension OTPVC {
    
    @IBAction func btnContinueAction(_ sender:UIButton){
        
        if self.enteredOTP.count < 6 {
            makeToast(strMessage: "Please enter OTP number")
        }
        else {
            verifyOTPCode()
        }
    }
    
    @IBAction func btnResendAction(_ sender:UIButton){

        var phoneNumber = ""
//        self.theControllerModel.signupDataDict["country_code"].stringValue+self.theControllerModel.signupDataDict["mobile"].stringValue
        
        if self.theControllerModel.selectController == .update {
            phoneNumber = "\(getUserData()?.user?.countryCode ?? "")" + "\(getUserData()?.user?.mobile ?? "")"
        }
        else {
            phoneNumber = self.theControllerModel.signupDataDict["country_code"].stringValue+self.theControllerModel.signupDataDict["mobile"].stringValue
        }
        
//        let phoneNumber = self.theControllerModel.signupDataDict["country_code"].stringValue+self.theControllerModel.signupDataDict["mobile"].stringValue
        print("phoneNumber:-\(phoneNumber)")
        
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            if error == nil {
                guard let verify = verificationId else { return }
                print("verificationId: ", verify)
                userDefault.set(verify, forKey: "verificationId")
            }
            else {
                makeToast(strMessage: error?.localizedDescription ?? "")
                return
            }
        }
        
    }
        
    
}
