//
//  OTPViewModel.swift
//  TaalSp
//
//  Created by Vishal on 12/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

class OTPViewModel {
    
    //MARK: Variables
    fileprivate weak var theController: OTPVC!
    var verifyId = String()
    var arrayCategoryID = NSMutableArray()
    var signupDataDict = JSON()
    var selectController = enumForLoginRegisterOTP.login
    
    //MARK: Initializer
    init(theController: OTPVC) {
        self.theController = theController
    }
    
    //MARK: API Setup
    //Login
    func loginDataAPI(parameters: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = loginURL
        
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().MakePostAPI(name: url, params: parameters, header: header) { (response, error, statusCode) in          
           
            
            if statusCode == success {
                if let result = response {
                    //                    print("Result:-", result)
                    let data = JSON(result)
                    print("Data:-", data)
                    
                    savedUserData(user: data)
                    
                    completionHandlor()
                }
            }
            else if statusCode == notFound {
                makeToast(strMessage: "Not_found_key".localized)
            }
            else if statusCode == unprocessableEntity {
                makeToast(strMessage: "unprocessable_Entity_key".localized)
            }
            else {
                makeToast(strMessage: error?.localized ?? "")
            }
        }
    }
    
    //SignUp
    func registerAPI(parameters: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = registerURL
        
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().makePostApiWithBody(name: url, params: parameters, headers: header) { (response, error, statusCode) in

            self.theController.hideLoader()
            
            if statusCode == success {
                if let result = response {

                    let data = JSON(result)
                    print("Data:-", data)
//                    savedUserData(user: data)

                    makeToast(strMessage: data["message"].stringValue)
                    completionHandlor()
                }
            }
            else if statusCode == notFound {
                makeToast(strMessage: "Not_found_key".localized)
            }
            else {
                
            }
        }
    }
    
    
    func updateMobileNumberAPI(param : [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = updateMobileNumberURL
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        WebServices().MakePutAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if let data = response {
                
                let jsonData = JSON(data)
                print("jsonData:=",jsonData)
                
                if statusCode == success {
//                    getUserData()?.user?.email = jsonData["user"]["email"].stringValue
//                    getUserData()?.user?.fullname = jsonData["user"]["fullname"].stringValue
                    getUserData()?.user?.mobile = jsonData["user"]["mobile"].stringValue
                    makeToast(strMessage: jsonData["message"].stringValue)
                    completionHandlor()
                }
                else if error != nil {
                    makeToast(strMessage: error?.localized ?? "")
                }
                else {
                    makeToast(strMessage: jsonData["error"].stringValue)
                }
            }
        }
    }
    
}
