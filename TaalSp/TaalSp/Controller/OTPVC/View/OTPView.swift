//
//  OTPView.swift
//  TaalSp
//
//  Created by Jaydeep on 23/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Foundation

class OTPView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblTitle: UILabel!    
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var vwOTP:VPMOTPView!
    @IBOutlet weak var btnContinueOutlet: CustomButton!
    @IBOutlet weak var btnResendOutlet: CustomButton!
    
    //MARK:- Setup
    
    func setupUI(theDelegate: OTPVC){
        
        lblTitle.textColor = .black
        lblTitle.font = themeFont(size: 25, fontname: .regular)
        lblTitle.text = "Verify_phone_number_key".localized.capitalized
        
        lblSubTitle.textColor = .black
        lblSubTitle.font = themeFont(size: 15, fontname: .light)
        
        let code = "Enter_6_digit_code_sent_to_you_at_key".localized
        let mobile = "\(theDelegate.theControllerModel.signupDataDict["country_code"].stringValue)"+" \(theDelegate.theControllerModel.signupDataDict["mobile"].stringValue)"
        lblSubTitle.text = "\(code) \( mobile)"
        
//        lblSubTitle.text = "Enter_6_digit_code_sent_to_you_at_key".localized+""
        
        vwOTP.otpFieldsCount = 6
        vwOTP.otpFieldDefaultBorderColor = UIColor.lightGray
        vwOTP.otpFieldEnteredBorderColor = UIColor.lightGray
        vwOTP.otpFieldErrorBorderColor = UIColor.red
        vwOTP.otpFieldBorderWidth = 0.5
        vwOTP.delegate = theDelegate.self
        vwOTP.otpFieldDisplayType = .square
        vwOTP.cursorColor = UIColor.lightGray
        vwOTP.shouldAllowIntermediateEditing = false
        vwOTP.otpFieldFont = themeFont(size: 15, fontname: .regular)
        if UI_USER_INTERFACE_IDIOM() == .phone {
            let result = UIScreen.main.bounds.size
            if result.height == 480 || result.height == 568 || result.height == 844 {
                vwOTP.otpFieldSize = 33
            }else{
                vwOTP.otpFieldSize = 45
            }
            
            if result.width <= 375 {
                vwOTP.otpFieldSeparatorSpace = 10
            } else {
                vwOTP.otpFieldSeparatorSpace = 20
            }
        }
        
        btnContinueOutlet.setTitle("Continue_key".localized, for: .normal)
        btnContinueOutlet.setupThemeButtonUI(backColor: UIColor.appThemeLightOrangeColor)
        
        btnResendOutlet.setTitle("Resend_code_key".localized.capitalized, for: .normal)
        btnResendOutlet.setupThemeButtonUI(backColor: .clear,fontColor:.black)
        btnResendOutlet.borderWidth = 0.5
        btnResendOutlet.alpha = 0.5
        
        DispatchQueue.main.async {
            self.vwOTP.initializeUI()
        }
        
    }
}
