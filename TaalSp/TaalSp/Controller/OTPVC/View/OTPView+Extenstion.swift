//
//  OTPView+Extenstion.swift
//  Taal
//
//  Created by Vishal on 23/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation

extension OTPVC : VPMOTPViewDelegate {
    
    func shouldBecomeFirstResponderForOTP(otpFieldIndex index: Int) -> Bool {
        return true
    }
    
    func enteredOTP(otpString: String) {
        self.enteredOTP = otpString
        print("OTPString:\(otpString)")
    }
    
    func hasEnteredAllOTP(hasEntered: Bool) -> Bool {
        print("Has entered all OTP? \(hasEntered)")
        return true
    }
    
}
