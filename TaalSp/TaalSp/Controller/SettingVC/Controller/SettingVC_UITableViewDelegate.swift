//
//  SettingVC_UITableViewDelegate.swift
//  Taal
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

extension SettingVC : UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dic = self.mainViewModel.arraySetting[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath) as! SettingCell
        
        cell.imgLeft.image = UIImage(named: "\(dic["img"].stringValue)")
        cell.lblTitile.text = dic["title"].stringValue
        
        cell.btnNotification.isHidden = false
        cell.btnNotification.tag = indexPath.row
        cell.btnNotification.addTarget(self, action: #selector(notificationOnOff(sender:)), for: UIControl.Event.touchUpInside)
        if getUserData()?.user?.notificationSetting ?? "" == "1" {
            cell.btnNotification.setImage(UIImage(named: "ic_notification_switch_on"), for: .normal)
        }
        else {
            cell.btnNotification.setImage(UIImage(named: "ic_notification_switch_off"), for: .normal)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if getUserData()?.user?.notificationSetting ?? "" == "1" {
            let param = ["notification":"0"]
            self.mainViewModel.updateSettingsAPI(param: param) {
                print("Change")
                self.mainView.tblSetting.reloadData()
            }
        }
        else {
            let param = ["notification":"1"]
            self.mainViewModel.updateSettingsAPI(param: param) {
                print("Change")
                self.mainView.tblSetting.reloadData()
            }
        }
        self.mainView.tblSetting.reloadData()
    }
    
    @objc func notificationOnOff(sender:UIButton){
        if getUserData()?.user?.notificationSetting ?? "" == "1" {
            let param = ["notification":"0"]
            self.mainViewModel.updateSettingsAPI(param: param) {
                print("Change")
                self.mainView.tblSetting.reloadData()
            }
        }
        else {
            let param = ["notification":"1"]
            self.mainViewModel.updateSettingsAPI(param: param) {
                print("Change")
                self.mainView.tblSetting.reloadData()
            }
        }
    }
}
