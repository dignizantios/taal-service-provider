//
//  SettingVC.swift
//  Taal
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class SettingVC: UIViewController {

    //MARK: Variables
    lazy var mainView: SettingView = { [unowned self] in
        return self.view as! SettingView
        }()
    
    lazy var mainViewModel: SettingViewModel = {
        return SettingViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainView.setUpUI(theController: self)
        self.setupNavigationbarwithBackButton(titleText: "Settings_key".localized, barColor: .appThemeBlueColor)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
