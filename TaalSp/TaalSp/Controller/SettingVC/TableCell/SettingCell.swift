//
//  SettingCell.swift
//  Taal
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class SettingCell: UITableViewCell {

    @IBOutlet weak var imgLeft: UIImageView!
    @IBOutlet weak var lblTitile: UILabel!
    @IBOutlet weak var btnNotification: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        lblTitile.font = themeFont(size: 18, fontname: .regular)
    }
    
}
