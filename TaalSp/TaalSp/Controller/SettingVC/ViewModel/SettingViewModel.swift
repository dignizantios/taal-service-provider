//
//  SettingViewModel.swift
//  Taal
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class SettingViewModel {
    
    fileprivate weak var theController : SettingVC!
    var arraySetting:[JSON] = []
    
    //MARK: Initializer
    init(theController: SettingVC) {
        self.theController = theController
    }
    
    //MARK: GetHistoryData
        func updateSettingsAPI(param: [String:Any], completionHandlor:@escaping()->Void) {
            
            let url = updateSettingsURL
            let header = ["Accept-Language":"language".localized,
                          "Version":iosVersion,
                          "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
            print("URL: ", url)
            print("Header: ", header)
            print("Param: ", param)
            
            guard ReachabilityTest.isConnectedToNetwork() else {
                makeToast(strMessage: "No_internet_connection_available_key".localized)
                return
            }
            self.theController.showLoader()
            
            WebServices().MakePutAPI(name: url, params: param, header: header) { (response, error, statusCode) in
                self.theController.hideLoader()
                if statusCode == success {
                    if let array = response as? NSArray {
                        let jsonArray = JSON(array).arrayValue
                        print("jsonArray: ", jsonArray)
                    }
                    else if let dict = response as? NSDictionary {
                        let jsonDict = JSON(dict.value(forKey: "user") as! NSDictionary)
                        print("jsonDict: ", jsonDict)
                        print("Notification_setting: ", jsonDict["notification_setting"])
                                            
                        setValueUserDetail(forKey: "notification_setting", forValue: jsonDict["notification_setting"].stringValue)
    //                    savedUserData(user: JSON(dict))
                    }
                    completionHandlor()
                }
                else if statusCode == notFound {
                    completionHandlor()
                }
                else if error != nil {
                    print("Error: ", error?.localized ?? "")
                }
                else {
                    print("StatusCode: ", statusCode ?? 0)
                }
            }
        }
}
