//
//  ChooseLocationView.swift
//  Taal
//
//  Created by Vishal on 23/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps

class ChooseLocationView: UIView {
    
    //MARK: Outlets
    @IBOutlet var vwMain: UIView!
    @IBOutlet var lbltitle: UILabel!
    @IBOutlet var vwMap: GMSMapView!
    @IBOutlet var btnConfirmLocation: UIButton!
    @IBOutlet var vwBottom: CustomView!
    @IBOutlet var txtLocation: UITextField!
    @IBOutlet var txtAddress: UITextField!
    
    //MARK: SetUp
    
    func setUpUi(theDelegate: ChooseLocationVC) {
        self.txtLocation.isUserInteractionEnabled = false
    }
    
    func setUpData(theDelegate:ChooseLocationVC) {
        self.vwBottom.roundCorners([.topLeft, .topRight], radius: 35)
//        btnConfirmLocation.roundCorners(corners: [.topLeft], radius: 35)
        btnConfirmLocation.setTitle("Confirm_location_key".localized, for: .normal)
    }
}

