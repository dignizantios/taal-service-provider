//
//  ReviewVC.swift
//  TaalSp
//
//  Created by Vishal on 16/06/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import UIKit

class ReviewVC: UIViewController {

    //MARK: Variables
    lazy var mainView: ReviewView = { [unowned self] in
        return self.view as! ReviewView
        }()
    
    lazy var mainModelView: ReviewViewModel = {
        return ReviewViewModel(theController: self)
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        mainView.setUpUI(theController: self)
        self.setupNavigationbarwithBackButton(titleText: "Write_a_review_key".localized, barColor: .appThemeBlueColor)
    }
    /*let vwRate = self.mainView.vwStarRating.value
    if (self.mainView.txtReviewHeadline.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
        makeToast(strMessage: "Please_fill_required_fields_key".localized)
    }
    else if (self.mainView.txtVwReviewDescription.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
        makeToast(strMessage: "Please_fill_required_fields_key".localized)
    }
    else if vwRate == 0 {
        makeToast(strMessage: "Please_fill_required_fields_key".localized)
    }
    else {
        
        let param = ["provideID": self.mainModelView.dictSelectCategoryData?.providerId ?? 0,
                     "rating": vwRate,
                     "comment": self.mainView.txtVwReviewDescription.text ?? "",
                     "title": self.mainView.txtReviewHeadline.text ?? ""] as [String : Any]
        print("Param: ", param)
        self.mainModelView.rateProviderAPI(param: param) {
            print("Review sucessfully add")
            self.goBack()
        }
    }*/
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        let rateValue = self.mainView.vwStarRating.value
        
        if (self.mainView.txtReviewHeadline.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_fill_required_fields_key".localized)
        }
        else if (self.mainView.txtVwReviewDescription.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_fill_required_fields_key".localized)
        }
        else if rateValue == 0 {
            makeToast(strMessage: "Please_fill_required_fields_key".localized)
        }
        else {
            
            let param = ["case_id": "\(self.mainModelView.caseID)",
                         "rating": rateValue,
                         "comment": self.mainView.txtVwReviewDescription.text ?? "",
                         "title": self.mainView.txtReviewHeadline.text ?? ""] as [String : Any]
            print("Param: ", param)
            self.mainModelView.rateProviderAPI(param: param) {
                print("Review sucessfully add")
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}

