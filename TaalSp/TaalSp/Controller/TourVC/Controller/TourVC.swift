//
//  TourVC.swift
//  TaalSp
//
//  Created by Jaydeep on 22/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SDWebImage

class TourVC: UIViewController {

    //MARK:- Variable
    lazy var theCurrentView:TourView = { [unowned self] in
        return self.view as! TourView
        }()
    
    lazy var theCurrentModel: TourModel = {
        return TourModel(theController: self)
     }()
    
    //MARK:- ViewLifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        theCurrentView.setupUI()
        theCurrentView.setupPageControl(totalPages: theCurrentModel.arrTour.count)
        
        //MARK:- remove when get data
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(obj, animated: false)
        
        //MARK:- Live when get data
//        if userDefault.bool(forKey: isFirstTime) == false {
//            theCurrentModel.tourDataApi {
//                self.theCurrentView.collectionTour.reloadData {
//                    self.theCurrentView.collectionTour.scrollToItem(at: IndexPath(item: 0, section: 0), at: .left, animated: false)
//                }
//            }
//        }
//        else {
//            let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
//            self.navigationController?.pushViewController(obj, animated: false)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    

}

//MARK:- Collection View Delegate & Datasouecw

extension TourVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return theCurrentModel.arrTour.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TourCell", for: indexPath) as! TourCell
        let dict = theCurrentModel.arrTour[indexPath.row]
        
        cell.lblTitle.text = dict.name
        cell.lblSubTitle.text = dict.description
        
        cell.imgTour.sd_setShowActivityIndicatorView(true)
        cell.imgTour.sd_setIndicatorStyle(.gray)
        cell.imgTour.sd_setImage(with: dict.imageUrl?.toURL(), placeholderImage: nil, options: .lowPriority, completed: nil)

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        theCurrentModel.currentIndex = indexPath.row
//        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
        theCurrentView.updatePageControl(index: theCurrentModel.currentIndex)
        if theCurrentModel.currentIndex == 0 {
            theCurrentView.btnShowGetStartedButton()
        } else if (theCurrentModel.currentIndex + 1) == theCurrentModel.arrTour.count{
            theCurrentView.btnShowFinishButton()
        } else {
            theCurrentView.btnShowNextButton()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func redirectToCell(indexPath:IndexPath){
        theCurrentView.collectionTour.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    func redirectToHome(){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(obj, animated: false)
    }
}

//MARK:-Action Zone

extension TourVC {
    
    @IBAction func btnSkipAction(_ sender:UIButton){
         redirectToHome()
    }
    
    @IBAction func btnGetStartedAction(_ sender:UIButton){
        if theCurrentModel.arrTour.count < theCurrentModel.currentIndex{
            redirectToHome()
            return
        }
        theCurrentModel.currentIndex += 1
        theCurrentView.updatePageControl(index: theCurrentModel.currentIndex)
        theCurrentView.btnShowNextButton()
        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
    }
    
    @IBAction func btnNextAction(_ sender:UIButton){
        if theCurrentModel.arrTour.count-1 == theCurrentModel.currentIndex{
            redirectToHome()
            return
        }
        theCurrentModel.currentIndex += 1        
        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
        theCurrentView.updatePageControl(index: theCurrentModel.currentIndex)
        if theCurrentModel.currentIndex == 0 {
            theCurrentView.btnShowGetStartedButton()
        } else if (theCurrentModel.currentIndex + 1) == theCurrentModel.arrTour.count{
            theCurrentView.btnShowFinishButton()
        } else {
            theCurrentView.btnShowNextButton()
        }
    }
}

//MARK:- Scrollview Delegate
extension TourVC : UIScrollViewDelegate {
    
//    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        theCurrentModel.currentIndex = Int(scrollView.contentOffset.x) / Int(scrollView.frame.width)
//        redirectToCell(indexPath: IndexPath(item: theCurrentModel.currentIndex, section: 0))
//        theCurrentView.updatePageControl(index: theCurrentModel.currentIndex)
//        if theCurrentModel.currentIndex == 0 {
//            theCurrentView.btnShowGetStartedButton()
//        } else if (theCurrentModel.currentIndex + 1) == theCurrentModel.arrTour.count{
//            theCurrentView.btnShowFinishButton()
//        } else {
//            theCurrentView.btnShowNextButton()
//        }
//    }
    
}
