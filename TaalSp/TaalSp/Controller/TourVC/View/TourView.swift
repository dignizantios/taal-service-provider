//
//  TourView.swift
//  TaalSp
//
//  Created by Jaydeep on 22/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class TourView: UIView {
 
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnSkipOutlet: UIButton!
    @IBOutlet weak var btnGetStartedOutlet: CustomButton!
    @IBOutlet weak var btnNextOutlet: UIButton!
    @IBOutlet weak var btnFinishOutlet: UIButton!
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var collectionTour: UICollectionView!
    
    //MARK:- Setup UI
    
    func setupUI() {
        
        btnGetStartedOutlet.setTitle("Get_started_key".localized.capitalized, for: .normal)
        btnGetStartedOutlet.setupThemeButtonUI()
        
        btnFinishOutlet.setTitle("Finish_key".localized, for: .normal)
        btnFinishOutlet.setupThemeButtonUI()
        
        btnSkipOutlet.setTitle("skip_key".localized, for: .normal)
        btnSkipOutlet.titleLabel?.font = themeFont(size: 18, fontname: .regular)
        btnSkipOutlet.setTitleColor(UIColor.appThemeBlueColor, for: .normal)
        
        btnNextOutlet.setupThemeButtonUI()
        btnNextOutlet.setTitle("", for: .normal)
        btnNextOutlet.tintColor = .white
        btnNextOutlet.setImage(UIImage(named: "ic_left_arrow_white")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        
        [btnSkipOutlet,btnFinishOutlet,btnNextOutlet].forEach { (btn) in
            btn?.isHidden = true
        }
        
        collectionTour.register(UINib(nibName: "TourCell", bundle: nil), forCellWithReuseIdentifier: "TourCell")
        collectionTour.reloadData()
        
    }
    
    func updatePageControl(index:Int){
       pageControl.currentPage = index
    }
    
    func setupPageControl(totalPages:Int){
        pageControl.hidesForSinglePage = true
        pageControl.pageIndicatorTintColor = UIColor.lightGray
        pageControl.currentPageIndicatorTintColor = UIColor.appThemeBlueColor
        pageControl.numberOfPages = totalPages
    }
    
    func btnClickOnGetStarted(){
        btnGetStartedOutlet.isHidden = true
        [btnSkipOutlet,btnFinishOutlet,btnNextOutlet].forEach { (btn) in
            btn?.isHidden = false
        }
    }
    
    func btnShowGetStartedButton() {
        btnGetStartedOutlet.isHidden = false
        [btnSkipOutlet,btnFinishOutlet,btnNextOutlet].forEach { (btn) in
            btn?.isHidden = true
        }
    }
    
    func btnShowFinishButton(){
        btnFinishOutlet.isHidden = false
        [btnSkipOutlet,btnGetStartedOutlet,btnNextOutlet].forEach { (btn) in
            btn?.isHidden = true
        }
    }
    
    func btnShowNextButton(){
        
        [btnFinishOutlet,btnGetStartedOutlet].forEach { (btn) in
            btn?.isHidden = true
        }
        [btnNextOutlet,btnSkipOutlet].forEach { (btn) in
            btn?.isHidden = false
        }
    }

}
