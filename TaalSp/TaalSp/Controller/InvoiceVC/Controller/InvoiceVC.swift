//
//  InvoiceVC.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import CoreLocation

class InvoiceVC: UIViewController {
    
    //MARK:- Variable
    lazy var theCurrentView:InvoiceView = { [unowned self] in
        return self.view as! InvoiceView
        }()
    
    lazy var theCurrentModel: InvoiceModel = {
        return InvoiceModel(theController: self)
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI(theCurrentModel.selectedParentController)
        
        self.theCurrentView.tblCategory.delegate = self
        self.theCurrentView.tblCategory.dataSource = self
        if self.theCurrentModel.categoryID != "" {
            self.theCurrentView.vwData.isHidden = true
            self.theCurrentModel.providerQuotationDetailAPI(id: self.theCurrentModel.categoryID) {
//                self.theCurrentView.tblCategory.reloadData()
                self.theCurrentView.vwData.isHidden = false
                self.theCurrentView.setUpData(data: self.theCurrentModel.dictQuotationData!)
                print("Data success")                
            }
        }
    }
    
    func setupData(selectedController:enumForInvoiceDetail){
        theCurrentModel.selectedParentController = selectedController
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if theCurrentModel.selectedParentController == .invoice{
            setupNavigationbarwithBackButton(titleText: "Invoice_key".localized, barColor: .appThemeBlueColor)
        } else if theCurrentModel.selectedParentController == .accepted || theCurrentModel.selectedParentController == .pending {
            setupNavigationbarwithBackButton(titleText: "Quatation_detail_key".localized, barColor: .appThemeBlueColor)
        }
        else if theCurrentModel.selectedParentController == .addQuotation {
            setupNavigationbarwithBackButton(titleText: "Invoice_key".localized, barColor: .appThemeBlueColor)
        }
    }
}

//MARK: Button Action
extension InvoiceVC {
    
    @IBAction func btnSendAction(_ sender: Any) {
        
        addQuotationAPI()
    }
    
    @IBAction func btnArrivedAtLocationAction(_ sender: Any) {
        
        let param = ["case_id":self.theCurrentModel.dictQuotationData?.caseId ?? 0,
                     "status":6] as [String : Any]
        
        self.theCurrentModel.caseChangeStatusAPI(params: param) {
            print("Case changed")
            self.theCurrentModel.providerQuotationDetailAPI(id: self.theCurrentModel.categoryID) {
                self.theCurrentView.setUpData(data: self.theCurrentModel.dictQuotationData!)
            }
        }
    }

    
    @IBAction func btnStartJobAction(_ sender: Any) {
        
//        {"case_id":72,"status":4}

        let param = ["case_id":self.theCurrentModel.dictQuotationData?.caseId ?? 0,
                     "status":4] as [String : Any]
        
        self.theCurrentModel.caseChangeStatusAPI(params: param) {
            print("Case changed")
            self.theCurrentModel.providerQuotationDetailAPI(id: self.theCurrentModel.categoryID) {
                self.theCurrentView.setUpData(data: self.theCurrentModel.dictQuotationData!)
                
                let obj = self.storyboard?.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
                obj.strConversationId = (self.theCurrentModel.dictQuotationData?.userDetails!.conversationId)!
                obj.userLocation = CLLocationCoordinate2DMake(self.theCurrentModel.dictQuotationData?.latitude ?? 0.0, self.theCurrentModel.dictQuotationData?.longitude ?? 0.0)
                self.navigationController?.pushViewController(obj, animated: false)
                
            }
//            self.theCurrentView.setUpData(data: self.theCurrentModel.dictQuotationData!)
        }
    }
    
    
    @IBAction func btnCompletedJobAction(_ sender: Any) {
        appdelegate.stopUpdateLocation()
        
        appdelegate.arrWayPoint = []
        userDefault.removeObject(forKey: "isTripStarted")
        userDefault.synchronize()
        appdelegate.ref.child("tracking/\(self.theCurrentModel.dictQuotationData?.userDetails?.conversationId ?? "0")").removeValue()
        
        self.theCurrentModel.caseCompletedAPI(caseId: "\(self.theCurrentModel.dictQuotationData?.caseId ?? 0)") {
            isJobCompleted = true
            isCompletedJob = true
            self.theCurrentModel.deleteQuotationHandlor()
            self.navigationController?.popViewController(animated: true)
        }
    }

    
    @IBAction func btnDeleteAction(_ sender: Any) {
        
        let alertController = UIAlertController(title: "Taal_key".localized, message: "Are_you_sure_want_to_delete_?_key".localized, preferredStyle: UIAlertController.Style.alert)

        let okAction = UIAlertAction(title:"Delete_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            print("Delete: ")
            if self.theCurrentModel.dictQuotationData?.quotationId != nil {
                self.theCurrentModel.deleteQuotationAPI(quotationId: "\(self.theCurrentModel.dictQuotationData?.quotationId ?? 0)") {
                    self.theCurrentModel.deleteQuotationHandlor()
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
        let cancelAction = UIAlertAction(title:"Cancel_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel: ")
        }
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnEditAction(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "WriteQuatationVC") as! WriteQuatationVC
        obj.theCurrentModel.addEditQuatation = .editPending
        obj.theCurrentModel.dictQuotationData = self.theCurrentModel.dictQuotationData
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnTrackingAction(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "TrackingVC") as! TrackingVC
        obj.strConversationId = (theCurrentModel.dictQuotationData?.userDetails!.conversationId)!
        obj.userLocation = CLLocationCoordinate2DMake(theCurrentModel.dictQuotationData?.latitude ?? 0.0, theCurrentModel.dictQuotationData?.longitude ?? 0.0)
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @IBAction func btnCallAction(_ sender: Any) {
        
        let mobileNumber = "\(self.theCurrentModel.dictQuotationData?.userDetails?.countryCode ?? "")\(self.theCurrentModel.dictQuotationData?.userDetails?.mobile ?? "")"

        dialNumber(number: mobileNumber)
    }
    
    func dialNumber(number : String) {
        
        if let url = URL(string: "tel://\(number)"),
            UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler:nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        else {
            print("Not calling: ")
        }
    }

    
    @IBAction func btnChatAction(_ sender: Any) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChatDetailsVC") as! ChatDetailsVC
        
        obj.mainModelView.conversationId = theCurrentModel.dictQuotationData?.userDetails?.conversationId ?? ""
        obj.mainModelView.userFullName = theCurrentModel.dictQuotationData?.userDetails?.name ?? ""
//        obj.mainModelView.dictChatModel = dict
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tappedBackToHomeButton() {
        guard let homeVC = navigationController?.getReferenceVC(to: HomeVC.self) else { return }
        navigationController?.popToViewController(homeVC, animated: true)
    }
    
}

//MARK: API Setup
extension InvoiceVC {
        
    func addQuotationAPI() {
        let param : [String : Any] = ["category_id": "\(dataCategoriData?.categoryId ?? 0)",
                     "case_id": "\(dataCategoriData?.caseId ?? 0)",
                     "price": dataCategoriData?.price ?? "",
                     "extra_notes": dataCategoriData?.quotationExtraNotes ?? "",
                     "voice_notes": dataCategoriData?.audioBase64 ?? ""]
        
        print("param : ", param)
        self.theCurrentModel.addQuotationURL(parameters: param) {
            isCategoryDelete = true
            self.tappedBackToHomeButton()
        }
    }
}
