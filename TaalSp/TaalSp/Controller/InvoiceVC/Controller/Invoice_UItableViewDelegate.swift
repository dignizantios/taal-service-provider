//
//  Invoice_UItableViewDelegate.swift
//  TaalSp
//
//  Created by Vishal on 24/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

extension InvoiceVC : UITableViewDelegate, UITableViewDataSource {    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        print("Count: \(self.theCurrentModel.dictQuotationData?.caseDescription.count ?? 0)")
        return self.theCurrentModel.dictQuotationData?.caseDescription.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryDescriptionTableCell") as! CategoryDescriptionTableCell
        
        let dict = self.theCurrentModel.dictQuotationData?.caseDescription[indexPath.row]
        
        cell.lblDescriptionTitle.text = "\(dict?.label ?? "") : "
        cell.lblDescriptionValue.text = dict?.value?.first
                
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

//        self.theCurrentView.tblCategoryHeightContstraint.constant = CGFloat((self.theCurrentModel.dictQuotationData?.caseDescription?.count ?? 0)*20)
    }
    
}
