//
//  InvoiceView.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class InvoiceView: UIView {

    //MARK:-Outlet Zone
    
    @IBOutlet weak var vwData: CustomView!
    @IBOutlet weak var lblRequestedTitle: UILabel!
    @IBOutlet weak var lblRequest: UILabel!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var imgCategory: UIImageView!
    @IBOutlet weak var tblCategory: UITableView!
    @IBOutlet weak var tblCategoryHeightContstraint: NSLayoutConstraint!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDescriptionDetail: UILabel!
    @IBOutlet weak var lblProvideTitle: UILabel!
    @IBOutlet weak var lblProvideName: UILabel!
    @IBOutlet weak var lblQuatationDetailTitle: UILabel!
    @IBOutlet weak var lblQuatationName: UILabel!
    @IBOutlet weak var lblQuatationDescription: UILabel!
    @IBOutlet weak var lblOfferedPriceTitle: UILabel!
    @IBOutlet weak var lblOffedPrice: UILabel!
    @IBOutlet weak var lblVoiceNotesTitle: UILabel!
    @IBOutlet weak var lblUserDetailTitle: UILabel!
    @IBOutlet weak var lblNameTitle: UILabel!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblAddressTitle: UILabel!
    @IBOutlet weak var lblUserAddress: UILabel!
    @IBOutlet weak var btnSendOutlet: CustomButton!
    @IBOutlet weak var vwSend: UIView!
    @IBOutlet weak var vwEditDelete: UIView!
    @IBOutlet weak var btnDeleteOutlet: CustomButton!
    @IBOutlet weak var btnEditOutlet: CustomButton!
    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var vwStatus: CustomView!
    @IBOutlet weak var vwStatusOuter: UIView!
    @IBOutlet weak var vwAccepted: UIView!
    @IBOutlet weak var vwCallChat: UIView!
    @IBOutlet weak var btnRequestStatusOutlet: CustomButton!
    @IBOutlet weak var btnCallOutlet: CustomButton!
    @IBOutlet weak var btnChatOutlet: CustomButton!
    @IBOutlet weak var vwTracking: UIView!
    @IBOutlet weak var btnTracking: CustomButton!
    @IBOutlet weak var vwStartJob: UIView!
    @IBOutlet weak var btnStartJob: CustomButton!
    @IBOutlet weak var vwArrivedAtLocation: UIView!
    @IBOutlet weak var btnArrivedAtLocation: CustomButton!
    
    
    //MARK:- Setup UI
    
    func setupUI(_ selectedControlller:enumForInvoiceDetail){
        
        self.tblCategory.register(UINib(nibName: "CategoryDescriptionTableCell", bundle: nil), forCellReuseIdentifier: "CategoryDescriptionTableCell")
        //self.tblCategoryHeightContstraint.constant = 20
        [lblRequestedTitle,lblRequest,lblCategoryTitle,lblDescriptionTitle,lblProvideTitle,lblQuatationName,lblOffedPrice,lblUserDetailTitle,lblVoiceNotesTitle,lblStatus].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 17, fontname:.semi)
        }
        
        [lblOffedPrice].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 17, fontname:.bold)
        }
        
        lblRequestedTitle.text = "Request_key".localized.uppercased()
        lblCategoryTitle.text = "Category_key".localized
        lblDescriptionTitle.text = "Description_key".localized
        lblProvideTitle.text = "Provider_key".localized
        lblQuatationDetailTitle.text = "Quatation_details_key".localized
        lblUserDetailTitle.text = "User_details_key".localized
        lblOfferedPriceTitle.text = "Offred_Price_key".localized
        [lblCategoryName,lblProvideName,lblQuatationDetailTitle,lblOfferedPriceTitle].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname:.regular)
            lbl?.alpha = 0.5
        }
        
        [lblDescriptionDetail,lblUserName,lblNameTitle,lblAddressTitle,lblUserAddress, lblQuatationDescription].forEach({ (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname:.regular)
        })
        
        [btnSendOutlet,btnEditOutlet,btnDeleteOutlet,btnChatOutlet,btnCallOutlet,btnRequestStatusOutlet,btnTracking, btnStartJob, btnArrivedAtLocation].forEach { (btn) in
            btn?.setTitleColor(.white, for: .normal)
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .bold)
        }
        
        btnSendOutlet.backgroundColor = .appThemeLightOrangeColor
        btnSendOutlet.setTitle("Send_key".localized.localizedUppercase, for: .normal)
        
        btnEditOutlet.backgroundColor = .appThemeBlueColor
        btnEditOutlet.setTitle("Edit_key".localized.localizedUppercase, for: .normal)
        
        btnDeleteOutlet.backgroundColor = .appThemeRedColor
        btnDeleteOutlet.setTitle("Delete_key".localized.localizedUppercase, for: .normal)
        
        btnCallOutlet.backgroundColor = .appThemeLightBlueColor
        btnCallOutlet.setTitle("Call_key".localized.localizedUppercase, for: .normal)
        
        btnChatOutlet.backgroundColor = .appThemeBlueColor
        btnChatOutlet.setTitle("Chat_key".localized.localizedUppercase, for: .normal)
        
        btnRequestStatusOutlet.setTitle("COMPLETE_JOB_key".localized, for: .normal)
        
        btnTracking.backgroundColor = .appThemeBlueColor
        btnTracking.setTitle("Tracking_key".localized.localizedUppercase, for: .normal)
        
        btnStartJob.backgroundColor = .appThemeOrangeColor
        btnStartJob.setTitle("START_JOB_key".localized.localizedUppercase, for: .normal)
        
        btnArrivedAtLocation.backgroundColor = .appThemeLightOrangeColor
        btnArrivedAtLocation.setTitle("Arrived_at_location_key".localized.localizedUppercase, for: .normal)
        
        
        [vwSend, vwStartJob, vwTracking, vwCallChat, vwEditDelete, vwAccepted, vwArrivedAtLocation].forEach { (vw) in
            vw?.isHidden = true
        }
        
        if selectedControlller == .invoice {
            [vwEditDelete,vwStatusOuter,vwAccepted,vwCallChat].forEach { (vw) in
                vw?.isHidden = true
            }
        }
        else if selectedControlller == .accepted{
            [vwSend,vwEditDelete, vwStatusOuter].forEach { (vw) in
               vw?.isHidden = true
            }
//            self.vwTracking.isHidden = false
            vwStatus.backgroundColor = .appThemeGreenColor
            btnRequestStatusOutlet.backgroundColor = .appThemeGreenColor
        }
        else if selectedControlller == .pending{
            [vwSend,vwCallChat,vwAccepted, vwStatusOuter].forEach { (vw) in
                vw?.isHidden = true
            }
            vwStatus.backgroundColor = .appThemeLightOrangeColor
            btnRequestStatusOutlet.backgroundColor = .appThemeLightOrangeColor
        }
        else if selectedControlller == .addQuotation {
            [vwEditDelete,vwStatusOuter,vwAccepted,vwCallChat].forEach { (vw) in
                vw?.isHidden = true
            }
            vwSend.isHidden = false
            
            self.tblCategory.isHidden = true
            self.lblProvideName.text = getUserData()?.user?.fullname
            self.lblRequest.text = "\(dataCategoriData?.quotationId ?? 0 != nil ? dataCategoriData?.caseId ?? 0 : dataCategoriData?.quotationId ?? 0)" //dataCategoriData?.caseId
            self.lblDescriptionDetail.text = dataCategoriData?.extraNotes
            self.lblOffedPrice.text = "\(dataCategoriData?.price ?? "")"
            self.lblStatus.text = dataCategoriData?.status
            self.lblQuatationName.text = dataCategoriData?.categoryName
            self.lblUserName.text = dataCategoriData?.userDetails?.name
            self.lblUserAddress.text = dataCategoriData?.userDetails?.address
            self.lblCategoryName.text = dataCategoriData?.categoryName
            self.lblQuatationDescription.text = dataCategoriData?.quotationExtraNotes
        }
        
//        [lblQuatationDescription].forEach({ (lbl) in
//            lbl?.textColor = .appThemeBlueColor
//            lbl?.font = themeFont(size: 15, fontname:.light)
//        })
        
        [lblStatusTitle].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname:.semi)
            lbl?.alpha = 0.5
        }
    
        lblStatusTitle.text = "Status_key".localized
        lblNameTitle.text = "Name_key".localized
        lblAddressTitle.text = "Address_key".localized
        
//        self.lblDescriptionDetail.isHidden = true
    }
    
    func setUpData(data: CategoryDataModel) {
        self.lblQuatationDescription.isHidden = true
        self.tblCategory.isHidden = true
        self.lblProvideName.text = getUserData()?.user?.fullname
        self.lblRequest.text = "\(data.caseId ?? 0)"
        self.lblDescriptionDetail.text = data.extraNotes
        self.lblOffedPrice.text = "\(data.offeredPrice) KD"
        self.lblStatus.text = data.status
        self.lblQuatationName.text = ""
//        self.lblQuatationName.text = data.categoryName
        self.lblUserName.text = data.userDetails?.name
        self.lblUserAddress.text = data.userDetails?.address
        self.lblCategoryName.text = data.categoryName
        
        vwStatusOuter.isHidden = false
        vwStartJob.isHidden = true
      
        if data.statusId == 1 {
            vwCallChat.isHidden = false
            vwStartJob.isHidden = false
        }
        else if data.statusId == 0 {
//            if data.status != "Pending" {
//                vwCallChat.isHidden = false
//            }
            vwEditDelete.isHidden = false
//            vwTracking.isHidden = false
//            vwArrivedAtLocation.isHidden = false
        }
        else if data.statusId == 4 || data.statusId == 5 || data.statusId == 6 {
            vwTracking.isHidden = false
            vwArrivedAtLocation.isHidden = false
            vwCallChat.isHidden = false
        }
        else if data.statusId == 7 {
            vwAccepted.isHidden = false
            vwCallChat.isHidden = false
        }
        
        ///--New
        [vwSend, vwStartJob, vwTracking, vwEditDelete, vwAccepted, vwArrivedAtLocation].forEach { (vw) in
            vw?.isHidden = true
        }
        
        
        /*
        if data.status.lowercased() == acceptedCase {
            vwStartJob.isHidden = false
        }
        else if data.status.lowercased() == startJob {
            vwTracking.isHidden = false
            vwArrivedAtLocation.isHidden = false
        }
        else if data.status.lowercased() == startTracking || data.status.lowercased() == arrivedAtLocation {
            vwTracking.isHidden = false
            vwArrivedAtLocation.isHidden = false
        }
        else if data.status.lowercased() == userConfirm {
            vwAccepted.isHidden = false
        }
         */
    }
}
