//
//  InvoiceModel.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Foundation
import SwiftyJSON
import Alamofire


class InvoiceModel: NSObject {
    
    //MARK:- Variable
    fileprivate weak var theController:InvoiceVC!
    var selectedParentController = enumForInvoiceDetail.invoice
    var categoryID = String()
    var deleteQuotationHandlor:()->Void = {}
    var dictQuotationData: CategoryDataModel?
    
    //MARK:- LifeCycle
    init(theController:InvoiceVC) {
        self.theController = theController
    }
    
    
    //MARK: API SetUP
    func providerQuotationDetailAPI(id:String, completionHandlor:@escaping()->Void) {
        
        let url = providerQuotationDetailsURL+id
        print("URL: ", url)
        
        let param = NSDictionary() as! [String:Any]
        var header : [String:String] = [:]
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakeGetAPI(name: url, params: param, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            
            if statusCode == success {
                
                if let arrayData = result as? NSArray {
                    
                    let jsonData = JSON(arrayData).arrayValue
                    print("DATA: ", jsonData)
                    completionHandlor()
                }
                else if let dicData = result as? NSDictionary {
                    
                    let jsonData = JSON(dicData).dictionaryObject
                    print("DATA: ", JSON(dicData))
                    self.dictQuotationData = CategoryDataModel(JSON: jsonData!)
                    completionHandlor()
                }
            }
            else if error != nil {
                makeToast(strMessage: error?.localized.description ?? "")
//                completionHandlor()
            }
            else {
//                completionHandlor()
            }
        }
    }
    
    //MARK: Delete API
    func caseCompletedAPI(caseId: String, completionHandlor:@escaping()->Void) {
        
        let url = caseCompletedURL+caseId
        print("URL: ", url)
        let param = NSDictionary() as! [String:Any]
        var header : [String:String] = [:]
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePatchAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                if let data = response as? NSDictionary {
                    let jsonData = JSON(data)
                    print("JSON: ", JSON(data).dictionaryValue)
                    makeToast(strMessage: jsonData["message"].stringValue )
                }
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                
            }
        }
    }
    
    //MARK: Delete API
    func deleteQuotationAPI(quotationId: String, completionHandlor:@escaping()->Void) {
        
        let url = deleteQuotationURL+quotationId
        print("URL: ", url)
        let param = NSDictionary() as! [String:Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().makeDeleteApiWithBody(name: url, params: param, headers: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                if let data = response {
                    let jsonData = JSON(data).dictionaryObject
                    print("JSON: ", jsonData!)
                    makeToast(strMessage: data["message"] as! String )
                }
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                
            }
        }
    }
    
    
    //MARK: AddQuotation API
    func addQuotationURL(parameters: [String:Any], completionHandlor: @escaping() -> Void) {
        
        let url = addQuotationsURL
        print("URL :", url)
//        print("param: ", parameters)
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        print("header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().makePostApiWithBody(name: url, params: parameters, headers: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if statusCode == success {
                if let data = response {
                    
                    let jsonData = JSON(data)
                    makeToast(strMessage: jsonData["message"].stringValue)
                }
                completionHandlor()
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            else {
                if let data = response {
                    let jsonData = JSON(data)
                    makeToast(strMessage: jsonData["message"].stringValue)
                }
            }
        }
    }
    
    //MARK: API Setup
    func caseChangeStatusAPI(params: [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = caseChangeStatusURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Authorization":"\(getUserData()?.tokenType ?? "") "+"\(getUserData()?.accessToken ?? "")"]
        
        print("URL: ", url)
        print("PARAM: ", params)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        self.theController.showLoader()
        WebServices().MakePatchAPI(name: url, params: params, header: header) { (response, error, statusCode) in
            self.theController.hideLoader()
            if statusCode == success {
                if let dict = response as? NSDictionary {
                    let jsonDict = JSON(dict)
//                    self.dictQuotationData?.status = jsonDict["job_status"].stringValue
                    makeToast(strMessage: jsonDict["message"].stringValue)
//                    self.dictQuotationData?.status = ""
                    print("jsonDict: ", jsonDict)
                }
                else if let array = response as? NSArray {
                    let jsonArray = JSON(array)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                makeToast(strMessage: error ?? "")
//                completionHandlor()
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
