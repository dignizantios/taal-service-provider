//
//  CategoriesVC.swift
//  TaalSp
//
//  Created by Jaydeep on 29/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import RSSelectionMenu

class AddCategoriesVC: UIViewController {
    
    //MARK:- Variable
    lazy var theCurrentView:AddCategoriesView = { [unowned self] in
        return self.view as! AddCategoriesView
        }()
    
    lazy var theCurrentModel: AddCategoriesModel = {
        return AddCategoriesModel(theController: self)
    }()
    
    let dataArray = ["Trainer", "Developer", "Mechanic", "Painter", "Plumber", "Technician"]
    var selectedDataArray = [String]()
    var cellSelectionStyle: CellSelectionStyle = .tickmark
    
    //MARK:- ViewLifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI(theDelegate: self)
        allProviderCategoriesListApi {
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.theCurrentView.tblCategories.reloadData()
        setupNavigationbarwithBackButton(titleText: "Categories_key".localized, barColor: .appThemeBlueColor)
    }
    
}

//MARK:- Action Zone

extension AddCategoriesVC {
    
    @IBAction func btnEditAction(_ sender:UIButton){
        
        if self.theCurrentView.lblEditStatus.text == "Edit_key".localized {
            self.theCurrentView.lblEditStatus.text = "Btn_Done_key".localized
        }
        else {
            if self.theCurrentModel.arrayProviderCategory.count == 0 {
                self.theCurrentView.btnEdit.isHidden = true
                self.theCurrentView.lblEditStatus.isHidden = true
                self.theCurrentView.vwAddCategory.isHidden = true
            }
            else {
                self.theCurrentModel.arrayProviderCategory.forEach { (data) in
                    self.theCurrentModel.arrayCategoryID = []
                    if data.isSelected == 1 {
                        guard let id = data.id as? Int else { return }
                        self.theCurrentModel.arrayCategoryID.add(id)
                        self.theCurrentModel.addCategoriesApi(param: ["category_ids":self.theCurrentModel.arrayCategoryID], completionHandler: {
                            self.theCurrentView.btnEdit.isHidden = true
                            self.theCurrentView.lblEditStatus.isHidden = true
                            self.theCurrentView.vwAddCategory.isHidden = true
                            
                            self.allProviderCategoriesListApi {
                                self.theCurrentView.tblCategories.reloadData()
                            }
                        })
                    }
                    else {
                        self.theCurrentView.btnEdit.isHidden = true
                        self.theCurrentView.lblEditStatus.isHidden = true
                        self.theCurrentView.vwAddCategory.isHidden = true
                    }
                }
            }
//            self.theCurrentView.lblEditStatus.text = "Edit_key".localized
        }
    }
    
    @IBAction func btnAddToCategoryAction(_ sender:UIButton){
        
        self.theCurrentModel.providerCategoriesListApi {
            
            DispatchQueue.main.async {
                self.theCurrentView.tblAddCategory.reloadData()
            }
            if self.theCurrentModel.arrayProviderCategory.count == 0 {
                DispatchQueue.main.async {
                    self.theCurrentView.tblAddCategory.reloadData()
                }
                self.theCurrentView.btnEdit.isHidden = true
                self.theCurrentView.lblEditStatus.isHidden = true
                self.theCurrentView.vwAddCategory.isHidden = true
            }
            else {
                self.theCurrentView.btnEdit.isHidden = false
                self.theCurrentView.lblEditStatus.isHidden = false
                self.theCurrentView.vwAddCategory.isHidden = false
            }
        }
        
        if self.theCurrentView.vwAddCategory.isHidden == true {
            self.theCurrentView.btnEdit.isHidden = false
            self.theCurrentView.lblEditStatus.isHidden = false
            self.theCurrentView.lblEditStatus.text = "Btn_Done_key".localized
            self.theCurrentView.vwAddCategory.isHidden = false
            self.theCurrentView.tblAddCategory.reloadData()
        }
        else {
            self.theCurrentView.vwAddCategory.isHidden = true
            self.theCurrentView.tblCategories.reloadData()
        }
        
        if self.theCurrentModel.arrayProviderCategory.count == 0 {
            DispatchQueue.main.async {
                self.theCurrentView.tblAddCategory.reloadData()
            }
            self.theCurrentView.btnEdit.isHidden = true
            self.theCurrentView.lblEditStatus.isHidden = true
            self.theCurrentView.vwAddCategory.isHidden = true
        }
        
//        self.showAsMultiSelectPopover(sender: theCurrentView.btnAddCategory)
    }
    
    @IBAction func btnHideAddCategoryVCAction(_ sender: Any) {
//        self.theCurrentView.vwAddCategory.isHidden = true
//        self.theCurrentView.tblCategories.reloadData()
    }
}

//MARK:- Tableview Delegate & Datasource

extension AddCategoriesVC :UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == self.theCurrentView.tblCategories {
            if arrayCategoryList.count == 0 {
                setTableView("No_categories_available_key".localized, tableView: self.theCurrentView.tblCategories)
                return arrayCategoryList.count
            }
            self.theCurrentView.tblCategories.backgroundView = nil
            return arrayCategoryList.count
        }
        else {
            return self.theCurrentModel.arrayProviderCategory.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoriesCell") as! CategoriesCell
        
        if tableView == self.theCurrentView.tblCategories {
            let dict = arrayCategoryList[indexPath.row]
            cell.lblCategoriesName.text = dict.name
        }
        else {
            let dict = self.theCurrentModel.arrayProviderCategory[indexPath.row]
            cell.lblCategoriesName.text = dict.name
            
            if dict.isSelected == 0 {
                cell.btnSelected.isSelected = false
            }
            else {
                cell.btnSelected.isSelected = true
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if tableView == self.theCurrentView.tblAddCategory {
            
            let dict = self.theCurrentModel.arrayProviderCategory[indexPath.row]
            if dict.isSelected == 1 {
                dict.isSelected = 0
            }
            else {
                dict.isSelected = 1
            }
            let index = IndexPath(row: indexPath.row, section: 0)
            tableView.reloadRows(at: [index], with: .none)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if tableView == self.theCurrentView.tblAddCategory {
            if tableView.contentSize.height >= (screenHeight/2) {
                self.theCurrentView.tblAddCategoryHeightConstraint.constant = (screenHeight/1.5)
                self.theCurrentView.tblAddCategory.isScrollEnabled = true
            }
            else {
                self.theCurrentView.tblAddCategory.isScrollEnabled = false
                self.theCurrentView.tblAddCategoryHeightConstraint.constant = CGFloat(self.theCurrentModel.arrayProviderCategory.count*60)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if tableView == self.theCurrentView.tblCategories {
            let delete = UIContextualAction(style: .normal, title:  "", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                print("Update action ...")
                let alertController = UIAlertController(title: "Taal_key".localized, message: "Are_you_sure_want_to_delete_?_key".localized, preferredStyle: UIAlertController.Style.alert)
                let okAction = UIAlertAction(title:"Delete_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                    let data = arrayCategoryList[indexPath.row]
                    self.theCurrentModel.deleteCategoryApi(categoryID: Int(exactly: data.id ?? 0) ?? 0, completionHandlor: {
                        print("Remove Data for row: ", indexPath.row)
                        arrayCategoryList.remove(at: indexPath.row)
                        isCategoryDelete = true
                        self.theCurrentView.tblCategories.reloadData()
                    })
                }
                let cancelAction = UIAlertAction(title:"Cancel_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                    print("Cancel: ")
                }
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
                success(true)
            })
            
            delete.image = UIImage(named: "ic_delete")
            delete.backgroundColor = .red
            return UISwipeActionsConfiguration(actions: [delete])
        }
        else {
            return nil
        }
    }


    /*
    func tableView(_ tableView: UITableView, editActionsForRowAt: IndexPath) -> [UITableViewRowAction]? {
        
        if tableView == self.theCurrentView.tblCategories {
            let delete = UITableViewRowAction(style: .normal, title: "Delete_key".localized) { action, index in
                print("copy button tapped")
                
                let alertController = UIAlertController(title: "Taal_key".localized, message: "Are_you_sure_want_to_delete_?_key".localized, preferredStyle: UIAlertController.Style.alert)
                
                let okAction = UIAlertAction(title:"Delete_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
                    let data = arrayCategoryList[editActionsForRowAt.row]
                    
                    self.theCurrentModel.deleteCategoryApi(categoryID: Int(exactly: data.id ?? 0) ?? 0, completionHandlor: {
                        print("Remove Data for row: ", editActionsForRowAt.row)
                        arrayCategoryList.remove(at: editActionsForRowAt.row)
                        self.theCurrentView.tblCategories.reloadData()
                    })
                }
                
                let cancelAction = UIAlertAction(title:"Cancel_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
                    print("Cancel: ")
                }
                
                alertController.addAction(okAction)
                alertController.addAction(cancelAction)
                self.present(alertController, animated: true, completion: nil)
            }
            delete.backgroundColor = UIColor.appThemeRedColor
            //            UIColor(patternImage: UIImage(named: "ic_delete")!)
            
            return [delete]
        }
        else {
            return nil
        }
    }*/
    
}

/*
 extension AddCategoriesVC {
 
 func showAsMultiSelectPopover(sender: UIView) {
 
 let cellNibName = "CategoriesCell"
 let cellIdentifier = "CategoriesCell"
 
 let selectionMenu = RSSelectionMenu(selectionStyle: .multiple, dataSource: dataArray, cellType: .custom(nibName: cellNibName, cellIdentifier: cellIdentifier)) { (cell, name, indexPath) in
 
 let customCell = cell as! CategoriesCell
 
 customCell.lblCategoriesName.text = name.components(separatedBy: " ").first
 customCell.separatorInset = .zero
 customCell.selectionStyle = .none
 cell.tintColor = #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
 }
 
 selectionMenu.tableView?.layer.borderColor = UIColor.black.cgColor
 selectionMenu.tableView?.layer.borderWidth = 1
 selectionMenu.tableView?.layer.cornerRadius = 4
 
 //        selectionMenu.view.layer.borderColor = UIColor.black.cgColor
 //        selectionMenu.view.layer.borderWidth = 1
 
 selectionMenu.tableView?.separatorColor = .lightGray
 
 selectionMenu.searchBar?.isHidden = true
 
 selectionMenu.setSelectedItems(items: selectedDataArray) { [weak self] (text, index, selected, selectedList) in
 
 
 self?.selectedDataArray = selectedList
 
 //                self?.theCurrentView.txtCategories.text = selectedList.joined(separator: ", ")
 }
 
 selectionMenu.showEmptyDataLabel(text: "")
 
 selectionMenu.cellSelectionStyle = self.cellSelectionStyle
 
 selectionMenu.show(style: .popover(sourceView: sender, size: nil), from: self)
 }
 }
 */
