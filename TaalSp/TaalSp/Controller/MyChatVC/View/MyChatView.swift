//
//  MyChatView.swift
//  Taal
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class MyChatView: UIView {
    
    //MARK: Outlets
    @IBOutlet weak var tblMyChat: UITableView!
    
    func setupUI(theDelegate: MyChatVC) {
        
        
        tblMyChat.register(UINib(nibName: "MyChatCell", bundle: nil), forCellReuseIdentifier: "MyChatCell")
        
        
        theDelegate.mainModelView.arrayHeader = ["Abdul Aziz", "Sayed Ibrahim","Aban elyasi","Abdul khureshi"]
        tblMyChat.dataSource = theDelegate
        tblMyChat.delegate = theDelegate
    }
}

