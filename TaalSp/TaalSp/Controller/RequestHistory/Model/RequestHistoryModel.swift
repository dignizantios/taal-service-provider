//
//  RequestHistoryModel.swift
//  TaalSp
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation
import SwiftyJSON
import Alamofire

class RequestHistoryViewModel {

    fileprivate weak var theController: RequestHistoryVC!
    var isProfileVC = Bool()
    var arrayCategoryData: [CategoryDataModel] = []
    var refreshController = UIRefreshControl()
    var CategoryID = ""
    var isshowLoader = true
    
    
    var audioPlayer: AVAudioPlayer?
    var audioRecording: AVAudioRecorder?
    
    init(theController: RequestHistoryVC) {
        self.theController = theController
    }
    
}

//MARK: API Setup
extension RequestHistoryViewModel {
    
    func allProviderCategoriesListAPI(categoryID: String, completionHandlor:@escaping()->Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        let url = providerPendingAndAcceptedQuotationsURL+categoryID
        let param = NSDictionary() as! [String:Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        if isshowLoader {
            self.theController.showLoader()
        }
        WebServices().MakeGetAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            if self.isshowLoader {
                self.theController.hideLoader()
            }
            self.refreshController.endRefreshing()
            if statusCode == success {
                
                if let data = response as? NSDictionary {
                    let dicData = JSON(data).dictionaryValue
                    print("DicData: ", dicData)
                }
                else if let data = response as? NSArray {
                    let arrayData = JSON(data).arrayValue

                    self.arrayCategoryData = []
                    for quatation in arrayData {
                        let data = CategoryDataModel(JSON: quatation.dictionaryObject!)
                        self.arrayCategoryData.append(data!)
//                        self.arrayCategoryData.reverse()
                    }
                    print("ArrayData: ", arrayData)
                }
                
                completionHandlor()
            }
            else if statusCode == notFound {
                self.arrayCategoryData = []
                completionHandlor()
//                makeToast(strMessage: error ?? "")
            }
            else if error != nil {
//                makeToast(strMessage: error?.localized ?? "")
                completionHandlor()
            }
            else {
                completionHandlor()
            }
        }
    }
}
