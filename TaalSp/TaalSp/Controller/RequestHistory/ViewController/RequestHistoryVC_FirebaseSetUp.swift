//
//  RequestHistoryVC_FirebaseSetUp.swift
//  TaalSp
//
//  Created by Vishal on 10/01/20.
//  Copyright © 2020 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Firebase
import FirebaseAnalytics
import FirebaseDatabase


////MARK: Pass provider location lat long setup
//extension RequestHistoryVC {
//    
//    func createDatabaseFirebase() {
//
//        if self.theCurrentModel.CategoryID == "1" {
//
//            var ref: DatabaseReference!
//            ref = Database.database().reference(withPath: "Data")
//            let data = ref.queryOrdered(byChild: "ID").queryEqual(toValue: "5")
//
//            data.observe(.value) { (snapshot) in
//                if let value = snapshot.value as? NSDictionary {
//                    print("Not Nill")
//                    print("Value: ", value)
//                }
//                else {
//                    print("Nill")
//                    print("Data Not Available")
//                    let value : [String:String] = ["ID":"5",
//                                                   "my_lat":"10",
//                                                   "my_long":"20",
//                                                   "user_lat":"",
//                                                   "user_long":""]
//
//                    ref.childByAutoId().setValue(value) { (error, value) in
//                        if error != nil {
//                            print("Error: ", error?.localizedDescription ?? "")
//                        }
//                        else {
//                            print("Value: ", value)
//                            print("Msg saved successfully")
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
