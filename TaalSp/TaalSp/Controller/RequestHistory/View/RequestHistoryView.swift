//
//  RequestHistoryView.swift
//  TaalSp
//
//  Created by Jaydeep on 30/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class RequestHistoryView: UIView {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnPendingOutlet: CustomButton!
    @IBOutlet weak var btnAcceptedOutlet: CustomButton!
    @IBOutlet weak var tblRequestHistory: UITableView!
    @IBOutlet weak var heightOfUpperView: NSLayoutConstraint!
    @IBOutlet weak var vwUppar: UIView!
    
    //MARK:- Setup UI
    
    func setupUI(){
        
        [btnPendingOutlet,btnAcceptedOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        }
        
        btnPendingOutlet.setTitleColor(.white, for: .selected)
        btnPendingOutlet.setTitleColor(.appThemeBlueColor, for: .normal)
//        btnPendingOutlet.setTitle("Pending_key".localized.uppercased(), for: .normal)
        btnPendingOutlet.setTitle("Pending_key".localized.uppercased(), for: .normal)
        
        btnAcceptedOutlet.setTitleColor(.white, for: .selected)
        btnAcceptedOutlet.setTitleColor(.appThemeBlueColor, for: .normal)
        btnAcceptedOutlet.setTitle("Accepted_key".localized.uppercased(), for: .normal)
//        btnAcceptedOutlet.setTitle("Accepted_key".localized.uppercased(), for: .normal)
        
        btnSelectPending()
        
        tblRequestHistory.register(UINib(nibName: "RequestTableCell", bundle: nil), forCellReuseIdentifier: "RequestTableCell")
        tblRequestHistory.tableFooterView = UIView()
        tblRequestHistory.reloadData()
    }
    
    func btnSelectPending(){
        btnPendingOutlet.backgroundColor = .appThemeLightOrangeColor
        btnPendingOutlet.isSelected = true
        btnPendingOutlet.tintColor = .clear
        
        [btnAcceptedOutlet].forEach { (btn) in
            btn?.isSelected = false
            btn?.backgroundColor = .clear
        }
        tblRequestHistory.reloadData()
    }
    
    func btnSelectAccepted(){
        btnAcceptedOutlet.backgroundColor = .appThemeLightOrangeColor
        btnAcceptedOutlet.isSelected = true
        btnAcceptedOutlet.tintColor = .clear
        
        [btnPendingOutlet].forEach { (btn) in
            btn?.isSelected = false
            btn?.backgroundColor = .clear
        }
        tblRequestHistory.reloadData()
    }
}
