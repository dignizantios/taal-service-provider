//
//  AddFeedbackView.swift
//  Taal
//
//  Created by Vishal on 16/06/20.
//  Copyright © 2020 Dignizant. All rights reserved.
//

import Foundation
import UIKit

class AddFeedbackView: UIView {
    
    //MARK: Outlets
    
    @IBOutlet weak var vwMain: CustomView!
    @IBOutlet weak var lblTitleText: UILabel!
    @IBOutlet weak var txtVwFeedback: UITextView!
    @IBOutlet weak var btnSubmit: CustomButton!
    
    
    func setUpUI(theDelegate: AddFeedbackVC) {
        
        self.txtVwFeedback.becomeFirstResponder()
        lblTitleText.font = themeFont(size: 18, fontname: .bold)
        txtVwFeedback.font = themeFont(size: 14, fontname: .regular)
        btnSubmit.titleLabel?.font = themeFont(size: 17, fontname: .semi)
        btnSubmit.setTitle("Submit".localized, for: .normal)
    }
    
}
