//
//  HomeTableCell.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//
import Foundation
import UIKit
import HCSStarRatingView

class HomeTableCell: UITableViewCell {
    
    //MARK:- Outlet Zone
    
    @IBOutlet weak var lblRequestTitle: UILabel!
    @IBOutlet weak var lblRequestNumber: UILabel!
    @IBOutlet weak var lblCategoryTitle: UILabel!
    @IBOutlet weak var lblCategoryName: UILabel!
    @IBOutlet weak var lblDescriptionTitle: UILabel!
    @IBOutlet weak var lblDesriptionDetail: UILabel!
    @IBOutlet var tblDescription: UITableView!
    @IBOutlet var tblDescriptionHeightConstant: NSLayoutConstraint!
    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var btnWriteYourQuatationOutlet: UIButton!
    @IBOutlet weak var lblWriteYourTitle: UILabel!
    @IBOutlet weak var lblQuotationTitle: UILabel!
    @IBOutlet weak var btnAudio: UIButton!
    @IBOutlet weak var vwStarRatingView: HCSStarRatingView!
    @IBOutlet weak var btnViewReview: UIButton!
    
    //MARK:- ViewLifeCycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        [lblRequestTitle,lblRequestNumber,lblCategoryTitle,lblDescriptionTitle,lblStatus,lblDesriptionDetail].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 17, fontname:.semi)
        }
        
        lblDesriptionDetail.font = themeFont(size: 12, fontname:.light)
        
        lblRequestTitle.text = "Request_key".localized
        
        [lblCategoryName,lblStatusTitle].forEach { (lbl) in
            lbl?.textColor = .appThemeBlueColor
            lbl?.font = themeFont(size: 12, fontname:.semi)
            lbl?.alpha = 0.5
        }
        
        lblCategoryTitle.text = "Category_key".localized
        
        lblDescriptionTitle.text = "Description_key".localized
        
        lblStatusTitle.text = "Status_key".localized
        
        [lblWriteYourTitle,lblQuotationTitle].forEach { (lbl) in
            lbl?.textColor = .white
        }
        
        lblWriteYourTitle.font = themeFont(size: 12, fontname: .semiBold)
        lblQuotationTitle.font = themeFont(size: 15, fontname: .semiBold)
        
        lblWriteYourTitle.text = "Write_your_key".localized.uppercased()
        lblQuotationTitle.text = "Quatation_key".localized.uppercased()
        
        btnWriteYourQuatationOutlet.backgroundColor = .appThemeLightOrangeColor
        
        tblDescription.isScrollEnabled = true
        
        [btnViewReview].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 12, fontname: .semi)
            btn?.setTitleColor(UIColor.appThemeBlueColor, for: .normal)
        }
        btnViewReview.setTitle("view_user_reviews".localized, for: .normal)
        
        // Create observer for inner table height
        tblDescription.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setUpData(data: CategoryDataModel) {
        
        btnAudio.isHidden = data.hasAudio == 1 ? true : false
        
        self.lblRequestNumber.text = "\(data.caseId ?? 0)"
        self.lblCategoryName.text = data.categoryName
        self.lblStatus.text = data.status
        
        if data.caseDescription.count ?? 0 > 0 {
            if data.caseDescription.contains(where: { (json) -> Bool in
                if json.label != nil && json.label == "Voice_key".localized {
                    return true
                }
                return false
            }) {
                btnAudio.isHidden = true
            }
            else {
                btnAudio.isHidden = true
            }
        }
        
        btnAudio.isHidden = true
        if data.hasAudio == 1 {
            btnAudio.isHidden = false
        }
        
        self.vwStarRatingView.value = CGFloat(exactly: data.userDetails?.rating ?? 0) ?? 0.0
    }
    
    // Set observer for inner table height
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if(keyPath == "contentSize"){
            if object is UITableView {
                // print("contentSize:= \(tblSubData.contentSize.height)")
                self.contentView.layoutIfNeeded()
                self.contentView.layoutSubviews()
                self.tblDescriptionHeightConstant.constant = tblDescription.contentSize.height
            }
        }
    }
    
    deinit {
        self.tblDescription.removeObserver(self, forKeyPath: "contentSize")
    }
}
