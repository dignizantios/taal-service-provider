//
//  HomeModel.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class HomeModel: NSObject {

    //MARK:- Variable
    fileprivate weak var theController:HomeVC!
    var arrHomeRequestList:[JSON] = []
    var arrayAllData : [CategoryDataModel] = []
    var arrayPendingCategory : [CategoryDataModel] = []
    var categoryID = ""
    
    var refreshController : UIRefreshControl!
    
    
    //MARK:- LifeCycle
    init(theController:HomeVC) {
        self.theController = theController
    }
}

//MARK: API Setup
extension HomeModel {
    
    func userPendingRequestsAPI(_ completionHandlor: @escaping() -> Void) {
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        var url = ""
        if categoryID == "" {
            url = userPendingRequestsURL
        }
        else {
            url = userPendingRequestsURL+"/\(categoryID)"
        }
        print("URL:- ", url)
//        let url = userPendingRequestsURL
        let params = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
//        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
//        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: params, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            self.refreshController.endRefreshing()
            
            self.arrayAllData = []
            self.arrayPendingCategory = []
            if statusCode == success {
                
                if let array = result as? NSArray {
                    let data = JSON(array).arrayValue
                    print("Data: ", data)
                    
                    self.arrayPendingCategory = []
                    for category in data {
                        let data = CategoryDataModel(JSON: category.dictionaryObject!)
                        self.arrayPendingCategory.append(data!)
                        self.arrayAllData.append(data!)
//                        self.arrayPendingCategory.reverse()
                    }
                    completionHandlor()
                }
            }
            else if statusCode == notFound {
                self.arrayPendingCategory = []
                completionHandlor()
//                makeToast(strMessage: "No_record_found_key".localized)
            }
            else {
                completionHandlor()
            }
        }
    }
    
    func changeLanguageAPI(param:[String:Any], completionHandlor:@escaping()->Void) {
        
        let url = changeLangURL
        let header = ["Accept-Language":"language".localized,
                      "Version":iosVersion,
                      "Content-Type":"application/json"]
        
        print("URL: ", url)
        print("Param: ", param)
        print("Header: ", header)
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            if statusCode == success {
                if let data = response {
                    let dictJosn = JSON(data)
                    print("Data: ", dictJosn)
                }
                completionHandlor()
            }
            else if statusCode == notFound {
                
            }
            else if error != nil {
                print("Error: ", error?.localized ?? "")
            }
            else {
                print("StatusCode: ", statusCode ?? 0)
            }
        }
    }
}
