//
//  RequestVC.swift
//  TaalSp
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class RequestVC: UIViewController {

    //MARK:- Variable
    fileprivate lazy var theCurrentView:RequestView = { [unowned self] in
        return self.view as! RequestView
        }()
    
    private lazy var theCurrentModel: RequestModel = {
        return RequestModel(theController: self)
    }()
    
    //MARK:- ViewLife cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

       theCurrentView.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
         setupNavigationbarWithoutBackButton(titleText: "My_request_key".localized, barColor: .appThemeBlueColor)
    }

}


//MARK:- TableView Delegate

extension RequestVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableCell") as! HomeTableCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "RequestDetailVC") as! RequestDetailVC
        obj.theCurrentModel.checkVCCome = .MyRequest
        obj.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

