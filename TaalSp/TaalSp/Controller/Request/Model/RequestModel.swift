//
//  RequestModel.swift
//  TaalSp
//
//  Created by Jaydeep on 02/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class RequestModel: NSObject {
    //MARK:- Variable
    fileprivate weak var theController:RequestVC!
 
    
    //MARK:- LifeCycle
    init(theController:RequestVC) {
        self.theController = theController
    }
}
