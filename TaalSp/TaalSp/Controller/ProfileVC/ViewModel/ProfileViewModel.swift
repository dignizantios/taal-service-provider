//
//  ProfileViewModel.swift
//  Taal
//
//  Created by Jaydeep on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class ProfileViewModel {
    
    //MARK:- Variables
    fileprivate weak var theController:ProfileVC!
    var imagePicker = UIImagePickerController()
    var customImagePicker   = CustomImagePicker()
    var imageData = ""
    
    
    //MARK: Initialized
    init(theController:ProfileVC) {
        self.theController = theController
        setupArray()
    }
    
    //MARK: Variables
    /*var arrayHeader : [String] = []
    var arrayImgHeader : [String] = []*/
    
    var arrProfile:[JSON] = []
    /*theDelegate.mainModelView.arrayHeader = ["My_chats_key".localized.capitalized, "Addresses_key".localized,"Request_history_key".localized,"Change_mobile_key".localized,"Settings_key".localized,"Terms_Of_use_key".localized,"Privacy_policy_key".localized,"Logout_key".localized]
     theDelegate.mainModelView.arrayImgHeader = ["ic_my_chats", "ic_address","ic_request_history","ic_change_mobile","ic_settings","ic_terms_of_use","ic_privacy_policy","ic_logout"]*/

}

//MARK:- Setup Array

extension ProfileViewModel{
    func setupArray(){
        var dict = JSON()
        dict["name"].stringValue = "My_chats_key".localized.capitalized
        dict["image"] = "ic_my_chats"
        arrProfile.append(dict)
        
//        dict = JSON()
//        dict["name"].stringValue = "Addresses_key".localized
//        dict["image"] = "ic_address"
//        arrProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Request_history_key".localized
        dict["image"] = "ic_request_history"
        arrProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "My_categories_key".localized.capitalized
        dict["image"] = "ic_request_history"
        arrProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Change_mobile_key".localized
        dict["image"] = "ic_change_mobile"
        arrProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Settings_key".localized
        dict["image"] = "ic_settings"
        arrProfile.append(dict)
        
//        dict = JSON()
//        dict["name"].stringValue = "Terms_Of_use_key".localized
//        dict["image"] = "ic_terms_of_use"
//        arrProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Privacy_policy_key".localized
        dict["image"] = "ic_privacy_policy"
        arrProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "App_feedback".localized
        dict["image"] = "ic_feedback"
        arrProfile.append(dict)
        
        dict = JSON()
        dict["name"].stringValue = "Logout_key".localized
        dict["image"] = "ic_logout"
        arrProfile.append(dict)
        
    }
}


extension ProfileViewModel {
    
    //MARK: GetProfile
    func getProfileDataApi(completionHandler: @escaping ()-> Void) {
        
        let url = getProfileURL
        let parameters = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            
            if let data = result as? NSDictionary {
                print("Result:-\(result!)")
                let modelData = JSON(data)
                print("Data:-\(modelData)")
                
                updateUserDicData(user: modelData)
                completionHandler()
            }
            else if error != nil {
                makeToast(strMessage: error!)
            }
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 500)")
        }
    }
    
    
    //MARK: Update profile
    func updateProfileAPI(param : [String:Any], completionHandlor:@escaping()->Void) {
        
        let url = updateProfileURL
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        WebServices().MakePutAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.theController.hideLoader()
            if let data = response {
               
                let jsonData = JSON(data)
                print("jsonData:=",jsonData)
               
                if statusCode == success {
//                    setValueUserDetail(forKey: "user", forValue: jsonData["user"])
//                    getUserData()?.user?.email = jsonData["user"]["email"].stringValue
//                    getUserData()?.user?.fullname = jsonData["user"]["fullname"].stringValue
                    makeToast(strMessage: jsonData["message"].stringValue)
                    completionHandlor()
                    
                }
                else if error != nil {
                    makeToast(strMessage: error?.localized ?? "")
                }
                else {
                    makeToast(strMessage: jsonData["error"].stringValue)
                }
            }
        }
    }
}
