//
//  ProfileVC.swift
//  Taal
//
//  Created by Jaydeep on 25/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import TOCropViewController

class ProfileVC: UIViewController {

    //MARK:- Variables
    lazy var mainView: ProfileView = { [unowned self] in
        return self.view as! ProfileView
        }()
    
    lazy var mainModelView: ProfileViewModel = {
        return ProfileViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
 
        mainView.setupUI(theDelegate: self)
        
        mainView.txtName.text = getUserData()?.user?.fullname ?? ""
        mainView.txtEmail.text = getUserData()?.user?.email ?? ""
        
        self.mainModelView.getProfileDataApi {
            self.mainView.btnProfileSelection.isUserInteractionEnabled = false
            self.mainView.txtName.text = getUserData()?.user?.fullname
            self.mainView.txtEmail.text = getUserData()?.user?.email
        }
        
        let btnshare = UIBarButtonItem(image: UIImage(named: "ic_share")?.imageFlippedForRightToLeftLayoutDirection(), style: .plain, target: self, action: #selector(btnShareAction(_:)))
        btnshare.tintColor = UIColor.white
        navigationItem.rightBarButtonItem = btnshare
    }
    override func viewWillAppear(_ animated: Bool) {
        
        isHistoryVC = true
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        setupNavigationbarWithoutBackButton(titleText: "Profile_key".localized, barColor: .appThemeBlueColor)
        isNotificationChatVC()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
    }
    
    func isNotificationChatVC() {
        if isNotificationChat {
            let MyChatVC = self.storyboard?.instantiateViewController(withIdentifier: "MyChatVC") as! MyChatVC
            MyChatVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(MyChatVC, animated: false)
        }
    }
    
    @objc func btnShareAction(_ sender: UIBarButtonItem) {
        print("Share")
        //Set the default sharing message.
        let message = ""
        //Set the link to share.
        if let link = NSURL(string: "itms-apps://itunes.apple.com/app/") {
            let objectsToShare = [message,link] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.addToReadingList]
            self.present(activityVC, animated: true, completion: nil)
        }
    }
}

//MARK: SetUp
extension ProfileVC {
    
    func setUpUI() {
        
    }
    
    func checkValidation() {
        
        if ((mainView.txtName.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            makeToast(strMessage: "Please_enter_username_key".localized)
        }
        else if ((mainView.txtEmail.text?.trimmingCharacters(in: .whitespaces).isEmpty)!) {
            makeToast(strMessage: "Please_enter_email_key".localized)
        }
        else if getUserData()?.user?.fullname == self.mainView.txtName.text && getUserData()?.user?.email == self.mainView.txtEmail.text {
            [self.mainView.txtName,self.mainView.txtEmail].forEach { (txtField) in
                txtField?.isUserInteractionEnabled = false
                txtField?.resignFirstResponder()
            }
            self.mainView.lblEditStatus.text = "Edit_key".localized
        }
        else {
            
            var param : [String:Any] = [:]
            param["fullname"] = self.mainView.txtName.text ?? ""
            param["email"] = self.mainView.txtEmail.text ?? ""
            param["mobile"] = getUserData()?.user?.mobile
            param["image"] = ""
//                self.mainModelView.imageData
            print("param:",param)
            
            self.mainModelView.updateProfileAPI(param: param) {
                self.mainModelView.getProfileDataApi {
                    self.mainView.btnProfileSelection.isUserInteractionEnabled = false
                    self.mainView.txtName.text = getUserData()?.user?.fullname
                    self.mainView.txtEmail.text = getUserData()?.user?.email
                }
            }
        }
    }
}

//MARK:- Action Zone
extension ProfileVC {
    
    @IBAction func btnProfileSelectionAction(_ sender: Any) {
        
        mainModelView.customImagePicker.typeOfPicker = .onlyPhoto
        mainModelView.customImagePicker.showImagePicker(fromViewController: self, navigationColor: UIColor.appThemeBlueColor, imagePicked: { (response) in
            let themeImage = response[UIImagePickerController.InfoKey.originalImage] as! UIImage
            self.openCropVC(Image: themeImage)
            
        }, imageCanceled: {
        }, imageRemoved: nil)
    }
    
    @IBAction func btnEditProfileAction(_ sender:UIButton){
        
        if self.mainView.lblEditStatus.text == "Edit_key".localized {
            [self.mainView.txtName,self.mainView.txtEmail].forEach { (txtField) in
                txtField?.isUserInteractionEnabled = true
            }
            self.mainView.lblEditStatus.text = "Update_key".localized
            self.mainView.txtName.becomeFirstResponder()
            self.mainView.btnProfileSelection.isUserInteractionEnabled = true
        } else {
            [self.mainView.txtName,self.mainView.txtEmail].forEach { (txtField) in
                txtField?.isUserInteractionEnabled = false
                txtField?.resignFirstResponder()
            }
            self.mainView.lblEditStatus.text = "Edit_key".localized
            self.checkValidation()
        }
    }
}

//MARK:- TOCropViewcontroller Delegate
extension ProfileVC: TOCropViewControllerDelegate {
    
    func openCropVC(Image:UIImage) {
        self.view.endEditing(true)
        //        let cropVC = TOCropViewController(image: Image)
        let cropVC = TOCropViewController(croppingStyle: TOCropViewCroppingStyle.circular, image: Image)
        cropVC.customAspectRatio = CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.width)
        cropVC.delegate = self
        //        cropVC.navigationController?.isNavigationBarHidden = true
        cropVC.aspectRatioPreset = .presetCustom
        cropVC.rotateButtonsHidden = true
        cropVC.aspectRatioPickerButtonHidden = true
        cropVC.rotateClockwiseButtonHidden = true
        cropVC.resetAspectRatioEnabled = false
        cropVC.toolbar.backgroundColor = UIColor.white
        cropVC.toolbarPosition = .top
        cropVC.toolbar.cancelTextButton.setTitleColor(UIColor.white, for: .normal)
        cropVC.toolbar.doneTextButton.setTitleColor(UIColor.white, for: .normal)
        self.present(cropVC, animated: false, completion: nil)
    }
    
    /*func cropViewController(_ cropViewController: TOCropViewController, didCropTo image: UIImage, with cropRect: CGRect, angle: Int) {
     self.mainView.imgProfile.image = image
     cropViewController.dismiss(animated: false, completion: nil)
     }*/
    func cropViewController(_ cropViewController: TOCropViewController, didCropToCircularImage image: UIImage, with cropRect: CGRect, angle: Int) {
        self.mainView.imgProfile.image = image
        
        self.mainModelView.imageData = image.pngData()?.base64EncodedString(options: .lineLength64Characters) ?? ""
        cropViewController.dismiss(animated: false, completion: nil)
    }
    
    func cropViewController(_ cropViewController: TOCropViewController, didFinishCancelled cancelled: Bool) {
        
        dismiss(animated: true, completion: nil)
    }
}
