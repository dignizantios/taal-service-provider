//
//  ProfileVC_UiTableViewDelegates.swift
//  Taal
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import FirebaseMessaging

extension ProfileVC:UITableViewDelegate,UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.mainModelView.arrProfile.count == 0
        {
            let lbl = UILabel()
            lbl.text = "List not found yet"
            lbl.numberOfLines = 5
            lbl.textAlignment = NSTextAlignment.center
            lbl.textColor = UIColor.appThemeBlueColor
            lbl.center = tableView.center
            lbl.font = themeFont(size: 16.0, fontname: .light)
            tableView.backgroundView = lbl
            return 0
        }
        tableView.backgroundView = nil
        return self.mainModelView.arrProfile.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell") as! ProfileCell
        if indexPath.row == 0{
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                cell.backView.roundCorners([.topLeft,.topRight], radius: 6)
            }
            
        }
        if indexPath.row == self.mainModelView.arrProfile.count-1{
            DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
                cell.backView.roundCorners([.bottomLeft,.bottomRight], radius: 6)
            }
        }
        let dict = self.mainModelView.arrProfile[indexPath.row]
        cell.lblTitle.text = "\(dict["name"].stringValue)"
        cell.imgProfile.image = UIImage(named: "\(dict["image"].stringValue)")
        
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50//UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        let dict = self.mainModelView.arrayHeader[indexPath.row]
        let dict = self.mainModelView.arrProfile[indexPath.row]
        
        if dict["name"].stringValue.lowercased() == "My_chats_key".localized.lowercased(){
            let MyChatVC = self.storyboard?.instantiateViewController(withIdentifier: "MyChatVC") as! MyChatVC
            MyChatVC.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(MyChatVC, animated: true)
        }
        if dict["name"].stringValue.lowercased() == "Addresses_key".localized.lowercased(){
//            let addressVC = self.storyboard?.instantiateViewController(withIdentifier: "AddressesVC") as! AddressesVC
//            addressVC.hidesBottomBarWhenPushed = true
//            self.navigationController?.pushViewController(addressVC, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "Change_mobile_key".localized.lowercased() {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "ChangeMobileNumberVC") as! ChangeMobileNumberVC
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "My_categories_key".localized.lowercased() {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddCategoriesVC") as! AddCategoriesVC
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "Request_history_key".localized.lowercased() {
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "RequestHistoryVC") as! RequestHistoryVC
            obj.hidesBottomBarWhenPushed = true
            obj.theCurrentModel.isProfileVC = true
            obj.selectParentController = .fromProfile
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "Settings_key".localized.lowercased(){
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "Terms_Of_use_key".localized.lowercased(){
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "TermsPolicyVC") as! TermsPolicyVC
            obj.mainViewModel.isTerms = true
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "Privacy_policy_key".localized.lowercased(){
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "TermsPolicyVC") as! TermsPolicyVC
            obj.mainViewModel.isTerms = false
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "App_feedback".localized.lowercased(){
            let obj = self.storyboard?.instantiateViewController(withIdentifier: "AddFeedbackVC") as! AddFeedbackVC
            obj.hidesBottomBarWhenPushed = true
            self.navigationController?.pushViewController(obj, animated: true)
        }
        else if dict["name"].stringValue.lowercased() == "Logout_key".localized.lowercased(){
            
            DispatchQueue.main.async {
                self.LogoutButtonAction()
            }
        }
        
    }
    
    @objc func LogoutButtonAction() {
        let alertController = UIAlertController(title: "Taal_key".localized, message: "Are_you_sure_want_to_logout_?_key".localized, preferredStyle: UIAlertController.Style.alert)
        
        let okAction = UIAlertAction(title:"Yes_key".localized, style: UIAlertAction.Style.default) { (result : UIAlertAction) -> Void in
            
            let param = ["token":Messaging.messaging().fcmToken ?? ""]
            self.logoutApi(param: param)
        }
        
        let cancelAction = UIAlertAction(title:"No_key".localized, style: UIAlertAction.Style.cancel) { (result : UIAlertAction) -> Void in
            print("Cancel")
        }
        
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

