//
//  ProfileCell.swift
//  Taal
//
//  Created by Jaydeep on 26/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class ProfileCell: UITableViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        lblTitle.font = themeFont(size: 16, fontname: .regular)
    }
    
}
