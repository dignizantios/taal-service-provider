//
//  TermsPolicyVC.swift
//  Taal
//
//  Created by Abhay on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import WebKit

class TermsPolicyVC: UIViewController {

    //MARK: Variables
    lazy var mainView: TermsPolicyView = { [unowned self] in
        return self.view as! TermsPolicyView
        }()
    
    lazy var mainViewModel: TermsPolicyViewModel = {
        return TermsPolicyViewModel(theController: self)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.mainView.setUpUI(theController: self)
        if mainViewModel.isTerms{
            self.mainViewModel.isTitile = "Terms_Of_use_key".localized
            let param = ["setting_type" : "terms-and-condition"]
            self.mainViewModel.termsAndPrivacyApi(param: param) { data in
                let htmlString = data["terms"].stringValue
                self.mainViewModel.webView.loadHTMLString(htmlString, baseURL: nil)
            }
        } else {
            self.mainViewModel.isTitile = "Privacy_policy_key".localized
            let param = ["setting_type" : "privacy-and-policy"]
            self.mainViewModel.termsAndPrivacyApi(param: param) { data in
                let htmlString = data["privacy"].stringValue
                self.mainViewModel.webView.loadHTMLString(htmlString, baseURL: nil)
            }
        }
        setupNavigationbarwithBackButton(titleText: self.mainViewModel.isTitile, barColor: .appThemeBlueColor)
    }
    
}

extension TermsPolicyVC: WKNavigationDelegate{
    func webViewWebContentProcessDidTerminate(_ webView: WKWebView) {
        print(#function)
       //stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didCommit navigation: WKNavigation!) {
        print(#function)
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print(#function)
       //stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print(#function)
         //showLoader()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print(#function)
        //stopAnimating()
    }
    
}
