//
//  TermsPolicyView.swift
//  Taal
//
//  Created by Abhay on 30/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

class TermsPolicyView: UIView {
    
    //MARK: Outlets
   
    
    func setUpUI(theController: TermsPolicyVC) {
        theController.mainViewModel.webView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        theController.mainViewModel.webView.backgroundColor = .clear
        theController.view.addSubview(theController.mainViewModel.webView)
    }
    
}
