//
//  ChangeMobileNumberVC.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseAuth

class ChangeMobileNumberVC: UIViewController {
    
    //MARK:- Variable
    lazy var theCurrentView:ChangeMobileNumberView = { [unowned self] in
        return self.view as! ChangeMobileNumberView
        }()
    
    lazy var theCurrentModel: ChangeMobileNumberModel = {
        return ChangeMobileNumberModel(theController: self)
    }()
    
    //MARK:- ViewLife cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        theCurrentView.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setupNavigationbarwithBackButton(titleText: "Change_mobile_key".localized.capitalized, barColor: .appThemeBlueColor)
    }
}

//MARK:-Action Zone

extension ChangeMobileNumberVC {
    
    @IBAction func btnUpdateAction(_ sender: Any) {
        
        if (self.theCurrentView.txtMobileNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_phone_number_key".localized)
        }
        else {
            
            let dict = ["mobile" : self.theCurrentView.txtMobileNumber.text ?? "",
                        "country_code" : self.theCurrentModel.countryCode ?? ""] as [String : Any]
            print("Param:- \(dict)")
            self.theCurrentModel.loginDataDict = JSON(dict)
            self.theCurrentModel.checkIsMobileEmailExist(param: dict) {
                self.setupMobileNumberOTP()
            }
        }
    }
    
    @IBAction func btnSelectCountryAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)
    }
}

extension ChangeMobileNumberVC {
    
    func setupMobileNumberOTP() {
        
        let phoneNumber = "\(self.theCurrentModel.countryCode ?? "")"+"\(self.theCurrentView.txtMobileNumber.text!)"
        print("phoneNumber:-\(phoneNumber)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            if error == nil {
                print("verificationId:-",verificationId!)
                guard let verify = verificationId else { return }
                userDefault.set(verify, forKey: "verificationId")
                
                let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                otpVC.theControllerModel.signupDataDict["mobile"].stringValue = self.theCurrentView.txtMobileNumber.text ?? ""
                otpVC.theControllerModel.signupDataDict = self.theCurrentModel.loginDataDict
                otpVC.theControllerModel.verifyId = verify
                otpVC.theControllerModel.selectController = .update
                self.navigationController?.pushViewController(otpVC, animated: true)
            }
            else {
                print("Error:-\(error?.localizedDescription ?? "")")
            }
        }
    }
    
}

// MARK:- CountryCode Delegate

extension ChangeMobileNumberVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        print("data:\(data)")
        self.theCurrentModel.countryCode = data["dial_code"].stringValue
        self.theCurrentView.btnSelectCountryCodeOutlet.setTitle(data["dial_code"].stringValue, for: .normal)
    }
}
