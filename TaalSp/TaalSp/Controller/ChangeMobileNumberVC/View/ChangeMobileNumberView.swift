//
//  ChangeMobileNumberView.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class ChangeMobileNumberView: UIView {

    //MARK:- Outlet Zone
    
    @IBOutlet weak var btnSelectCountryCodeOutlet: UIButton!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var lblMobileNumberTitle: UILabel!
    @IBOutlet weak var btnUpdateMobileNumberOutlet: CustomButton!
    
    //MARK:- Setup UI
    
    func setupUI(){
        
        [btnSelectCountryCodeOutlet].forEach { (btn) in
            btn?.titleLabel?.font = themeFont(size: 17, fontname: .semi)
            btn?.setTitleColor(.black, for: .normal)
        }
        
        txtMobileNumber.textColor = .black
        txtMobileNumber.font = themeFont(size: 17, fontname: .regular)
        
        btnUpdateMobileNumberOutlet.setupThemeButtonUI(backColor: .appThemeChatOrangeColor, font:themeFont(size: 20, fontname: .bold))
        btnUpdateMobileNumberOutlet.setTitle("Update_key".localized.localizedUppercase, for: .normal)
        
        lblMobileNumberTitle.textColor = UIColor.appThemeBlueColor
        lblMobileNumberTitle.font = themeFont(size: 10, fontname: .regular)
        lblMobileNumberTitle.text = "Change_mobile_number_key".localized
        txtMobileNumber.text = getUserData()?.user?.mobile
        btnSelectCountryCodeOutlet.setTitle(getUserData()?.user?.countryCode, for: .normal)
        setUpData()
    }
    
    func setUpData() {
        
    }

}
