//
//  ChangeMobileNumberModel.swift
//  TaalSp
//
//  Created by Jaydeep on 27/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON

class ChangeMobileNumberModel: NSObject {

    fileprivate weak var theController: ChangeMobileNumberVC!
    var countryCode = getUserData()?.user?.countryCode
    var loginDataDict = JSON()
    
    //MARK: Initialized
    init(theController: ChangeMobileNumberVC) {
        self.theController = theController
    }
    
    
    //MARK:- checkIsMobileEmailExist
    func checkIsMobileEmailExist(param:[String:Any], completionHandlor:@escaping()->Void) {
        let url = mobileEmailExist
        
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        theController.showLoader()
        WebServices().MakePostAPI(name: url, params: param, header: header) { (result, error, statusCode) in
            
            self.theController.hideLoader()
            if let data = result {
                let jsonData = JSON(data)
                if jsonData["success"].intValue == 1 {
                    makeToast(strMessage: jsonData["message"].stringValue)
                    return
                }
                else {
                    completionHandlor()
                    
                }
            }
            else if error != nil {
                makeToast(strMessage: error?.localized ?? "")
            }
            print("Error:-\(error ?? "")")
            print("StatusCode:-\(statusCode ?? 500)")
        }
    }
    
}
