//
//  AddAddressVC_UITextFeidDelegates.swift
//  Taal
//
//  Created by vishal on 02/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import Foundation
import UIKit
import DropDown

extension AddAddressVC : UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == mainView.txtCity {
            self.view.endEditing(true)
            self.mainViewModel.cityNameDropDown.show()
            return false
        }
        
        return true
    }
}
