//
//  AddAddressVC.swift
//  Taal
//
//  Created by vishal on 29/11/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit

class AddAddressVC: UIViewController {

    //MARK: Variables
    lazy var mainView: AddAddressView = { [unowned self] in
        return self.view as! AddAddressView
        }()
    
    lazy var mainViewModel: AddAddressesViewModel = {
        return AddAddressesViewModel(theController: self)
    }()
    
    //MARK: Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationRightBtnSetUP()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setupNavigationbarwithBackButton(titleText: "Add_Address_key".localized.capitalized, barColor: .appThemeBlueColor)
        self.mainView.setUpUI(theController: self)
    }
    
    func navigationRightBtnSetUP() {
        let btnRightAdd = UIBarButtonItem(image: UIImage(named: "ic_header_add"), style: .plain, target: self, action: #selector(self.backButtonTapped))
        btnRightAdd.tintColor = .white
        self.navigationItem.rightBarButtonItem = btnRightAdd
    }

}
