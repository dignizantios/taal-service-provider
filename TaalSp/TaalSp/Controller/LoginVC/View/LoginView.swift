//
//  LoginView.swift
//  TaalSp
//
//  Created by Jaydeep on 25/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

class LoginView: UIView {

    //MARK:-Outlet Zone
    
    @IBOutlet weak var vwOuter: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblSubTitle: UILabel!
    @IBOutlet weak var btnSelectCountryOutlet: UIButton!
    @IBOutlet weak var txtMobileNumber: CustomTextField!
    @IBOutlet weak var btnLoginOutlet: CustomButton!
    @IBOutlet weak var lblOR: UILabel!
    @IBOutlet weak var btnSignUpOutlet: CustomButton!
    
    //MARK:- Setup
    
    func setupUI(theDelegate: LoginVC){
        lblTitle.text = "Welcome_back_key".localized.capitalized
        lblTitle.textColor = .white
        lblTitle.font = themeFont(size: 30, fontname: .bold)
        
        lblSubTitle.text = "Sign_in_to_continue_key".localized
        lblSubTitle.textColor = .white
        lblSubTitle.font = themeFont(size: 22, fontname: .regular)
        
        txtMobileNumber.font = themeFont(size: 16, fontname: .regular)
        txtMobileNumber.textColor = .white
        
        btnSelectCountryOutlet.titleLabel?.font = themeFont(size: 15, fontname: .regular)
        btnSelectCountryOutlet.setTitleColor(.white, for: .normal)
        
//        btnSignUpOutlet.setImage(UIImage(named: "")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnSignUpOutlet.setTitle("Sign_up_key".localized, for: .normal)
        btnSignUpOutlet.setupThemeButtonUI()
        
//        btnLoginOutlet.setImage(UIImage(named: "")?.imageFlippedForRightToLeftLayoutDirection(), for: .normal)
        btnLoginOutlet.setTitle("Login_key".localized, for: .normal)
        btnLoginOutlet.setupThemeButtonUI()
        
        lblOR.textColor = .white
        lblOR.text = "OR_key".localized
        lblOR.alpha = 0.5
        lblOR.font = themeFont(size: 12, fontname: .regular)
        
        vwOuter.roundCorners([.topLeft], radius:100)
        
        [txtMobileNumber].forEach { (textField) in
            textField?.tintColor = .white
        }
        theDelegate.addDoneButtonOnKeyboard(textfield: txtMobileNumber)
        
    }

}
