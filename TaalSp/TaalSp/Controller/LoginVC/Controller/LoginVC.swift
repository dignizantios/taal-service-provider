//
//  LoginVC.swift
//  TaalSp
//
//  Created by Jaydeep on 25/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import SwiftyJSON
import FirebaseAuth

class LoginVC: UIViewController {
    
    //MARK:- Variable
    lazy var theCurrentView:LoginView = { [unowned self] in
        return self.view as! LoginView
        }()
    
    private lazy var theCurrentModel: LoginViewModel = {
        return LoginViewModel(theController: self)
    }()
    
    
    //MARK:- ViewLife cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if getUserData()?.accessToken == nil || getUserData()?.accessToken == nil {
            
        }
        else {
            appdelegate.tabBarVC = TaalTabBarVC()
            appdelegate.tabBarVC.selectedIndex = 0
            self.navigationController?.pushViewController(appdelegate.tabBarVC, animated: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLayoutSubviews() {
        theCurrentView.setupUI(theDelegate: self)
    }

}

//MARK:-Action Zone

extension LoginVC {
    @IBAction func btnLoginAction(_ sender:UIButton){
        
        if (theCurrentView.txtMobileNumber.text?.trimmingCharacters(in: .whitespaces).isEmpty)! {
            makeToast(strMessage: "Please_enter_phone_number_key".localized)
        }
        else if theCurrentView.txtMobileNumber.text?.isNumeric == false {
            makeToast(strMessage: "Please_enter_valid_phone_number_key".localized)
        }
        else {

            let dict = ["mobile" : self.theCurrentView.txtMobileNumber.text ?? "",
                        "country_code" : self.theCurrentModel.countryCode] as [String : Any]
            print("Param:- \(dict)")
            self.theCurrentModel.loginDataDict = JSON(dict)
            self.theCurrentModel.checkIsMobileEmailExist(param: dict) {
                self.setupMobileNumberOTP()
            }
        }
        
    }
    
    @IBAction func btnSignUpAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(obj, animated: false)
    }
    
    @IBAction func btnSelectCountryAction(_ sender:UIButton){
        let obj = self.storyboard?.instantiateViewController(withIdentifier: "CountryCodeVC") as! CountryCodeVC
        obj.modalPresentationStyle = .overCurrentContext
        obj.mainView.delegate = self
        self.present(obj, animated: false, completion: nil)
    }
}


//MARK: OTP Setup
extension LoginVC {
    
    func setupMobileNumberOTP() {
        let phoneNumber = "\(self.theCurrentModel.countryCode)"+"\(self.theCurrentView.txtMobileNumber.text!)"
        print("phoneNumber:-\(phoneNumber)")
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            
            if error == nil {
                print("verificationId:-",verificationId!)
                guard let verify = verificationId else { return }
                userDefault.set(verify, forKey: "verificationId")
                let otpVC = self.storyboard?.instantiateViewController(withIdentifier: "OTPVC") as! OTPVC
                otpVC.theControllerModel.verifyId = verify
                otpVC.theControllerModel.selectController = .login
                otpVC.theControllerModel.signupDataDict = self.theCurrentModel.loginDataDict
                self.navigationController?.pushViewController(otpVC, animated: false)
            }
            else {
                print("Error:-\(error?.localizedDescription ?? "")")
            }
        }
    }
}


// MARK:- CountryCode Delegate
extension LoginVC : CountryCodeDelegate {
    
    func CountryCodeDidFinish(data: JSON) {
        print("data:\(data)")
        self.theCurrentModel.countryCode = data["dial_code"].stringValue
        self.theCurrentView.btnSelectCountryOutlet.setTitle(data["dial_code"].stringValue, for: .normal)
    }
}

