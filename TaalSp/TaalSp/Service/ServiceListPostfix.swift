//
//  ServiceListPostfix.swift
//  TaalSp
//
//  Created by Jaydeep on 03/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

///--- Validate
let deviceType = 1
let basic_username = "admin"
let basic_password = "1234567"
let user_data_key = "user data"

//api iOS version
let isFirstTime = "isFirstTime"
let iosVersion = "iOS-1.1"
let appType = "2"


///-- Google Map Key
let googleMapKey = "AIzaSyDehzq0fWedJmDB-Ta9n9UwNFPZkVpVhf0"

//let googleMapKey = "AIzaSyAI8Oi8hv4wq2YbAMZho9jC86wwzXlf4W0"


///-- One Signal authentication
let oneSignalAppId = "dae26ed8-eaba-411a-b984-54a4a9341f64"


///-- Pusher Data
let pusherInstanceLocater = "v1:us1:b813103a-3a73-42c3-9bc9-5a11d281ebf3"
let pusherSecretKey = "036f22a1-25dd-405a-bc77-bd6076765dc9:L4eHu/lKeE1axLR38AOgDBTNwX/Tn7Ogbj/usE3YWF8="


//MARK: Error response
let success = 200
let notFound = 404
let unprocessableEntity = 422
let invalidApi = 400
let invalidCredential = 401
let accountNotUproved = 406


//Main URL:
//let mainURL = "http://173.231.196.229/~tvavisa/taal/"
//let mainURL = "http://taal.dignizantapps.com/"
//let mainURL = "http://3.19.240.124/taal/"   //Test url
//let mainURL = "http://52.66.128.99/taal/" //Test url
let mainURL = "http://www.taaal.com/"          //Live url


//Bace screen
let screenBase = "api/screen/"


//changeLang
let changeLangURL = "provider/changeLang" //changeLang


//SignUP
let categoryListURL = "api/category"
let mobileEmailExist = "api/provider/mobileEmailExist"
let loginURL = "api/provider/login"
let registerURL = "api/provider/register"


//Category
let providerCategoryURL = "api/provider/category" //register time Not selected categories list
let allProviderCategoriesListURL = "api/provider/categories" //All Selected categories when register time selected
let addCategoriesURL = "api/provider/addCategories"  //Add provider categories
let deleteCategoryURL = "api/provider/deleteCategory/" // Delete Category by Id


//Provider Chat
let sendMessageURL = "api/chat/sendMessageToUser"   //Send Message
let providerChatHistoryURl = "api/chat/providerChatHistory/"    //Provider ChatHistory
let providerChatListURL = "api/chat/providerChatList"   //Provider ChatList


//Provider Cases
let userPendingRequestsURL = "api/provider/cases"  //Get User Pending Requests
let userCaseDetailURL = "api/provider/case/"  //Get User Case in detail


//Provider Quotations
let providerPendingAndAcceptedQuotationsURL = "api/provider/quotes/"   // Get Provider Pending and Accepted Quotations
let providerQuotationDetailsURL = "api/provider/quote/"     //Get Provider Quotation details by Id
let addQuotationsURL = "api/provider/quote/add"      //Add Quote
let editQuatationGetDataURL = "api/provider/quote/fields/edit/"   //Edit Quotation get data
let editQuatationURL = "api/provider/quote/edit/"   //Edit Quotation by Id
let deleteQuotationURL = "api/provider/quote/delete/"     //Delete Quotation by Id
let caseCompletedURL = "api/provider/caseCompleted/"  //Case Completed


//updateSettings
let updateSettingsURL       = "api/provider/updateSettings"         //updateSettings

 
//caseChangeStatus
let caseChangeStatusURL     = "api/provider/providerCaseStatus"       //caseChangeStatus


//Profile
let getProfileURL = "api/provider/getProfile"
let updateProfileURL = "api/provider/updateProfile"
let updateMobileNumberURL = "api/provider/updateMobileNumber"
let getPages = "api/user/getPages" //For Used Terms and privacy

//Feedback
let addFeedbackURL = "api/provider/feedback" //Add feedback


let rateUserURL = "api/provider/rateUser" //Rate user
let getRatingReviewsUserURL = "api/provider/getRatingReviewsUser/"     //getRatingReviewsUser


//Logout:
let logoutURL = "api/provider/logout"

//privacy-and-policy:
let termsAndPrivacyURL      = "/api/termsAndPrivacy"            // termsAndPrivacy <For T&C param pass : "setting_type" = "setting_type", For privacy policy param: "setting_type" = "privacy-and-policy" >
