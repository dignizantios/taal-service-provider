//
//  WebServices.swift
//  Taal
//
//  Created by Vishal on 09/12/19.
//  Copyright © 2019 Dignizant. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SystemConfiguration
import NVActivityIndicatorView

struct WebServices : NVActivityIndicatorViewable {
    
    func isNetworkReachable(with flags: SCNetworkReachabilityFlags) -> Bool {
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        let canConnectAutomatically = flags.contains(.connectionOnDemand) || flags.contains(.connectionOnTraffic)
        let canConnectWithoutUserInteraction = canConnectAutomatically && !flags.contains(.interventionRequired)
        
        return isReachable && (!needsConnection || canConnectWithoutUserInteraction)
    }
    
    //MARK: - Post with body
    
    func makePostApiWithBody(name: String, params:[String:Any], headers:[String:String], completionHandler: @escaping(NSDictionary?, String?, Int?) -> Void) {
        
        print("header:-\(headers)")

        let url = mainURL + name
        print("url:-\(url)")
        print("Param:-\(JSON(params))")
        
        request(url, method: .post, parameters: params, encoding: URLEncoding.httpBody , headers: headers).responseJSON(completionHandler: { (response) in
            print("response:", response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = "Server_not_responding_please_try_again_key".localized
//                if let res = response.data {
//                    if let json = try! JSONSerialization.jsonObject(with: res, options: .mutableContainers) as? [String: Any] {
//                        error = json["error"] as! String
//                    }
//                }
//                if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
//                    error = json["error"] as! String
//                }
                completionHandler(nil, error, statusCode)
//                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
        })
    }
    
    //MARK: - Post
    func MakePostAPI(name:String, params:[String:Any], header:[String:String], completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
        print("header:-\(header)")
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Param:-\(JSON(params))")
        Alamofire.request(url, method: .post, parameters: params, encoding: JSONEncoding.default, headers: header).validate().responseJSON { (response) in
            print(response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
//                var error = ""
//                if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
//                    error = json["error"] as! String
//                }
//                completionHandler(nil, error, statusCode)
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
            }.resume()
    }
    
    
    //MARK: - Get
    func MakeGetAPI(name:String, params:[String:Any], header:[String:String], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        print("header:-\(header)")
        
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseJSON { (response) in
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    
                    if let _ = response.result.value as? Any {
                        completionHandler(response.result.value as? Any, nil, statusCode)
                    }
//                    else {
//                        completionHandler(response.result.value as? Any, nil, statusCode)
//                    }
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                
//                if !response.data!.isEmpty {
//                    if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
//                        error = json["error"] as? String ?? ""
//                    }
//                }
                                
                if error != "" {
                    completionHandler(nil, error, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
                
            }
        }.resume()
    }
    
    func MakeGetAPIWithoutAuth(name:String, params:[String:Any], progress: Bool = true, completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
        var headers:[String : String] = [:]
        print(headers)
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.request(url, method: .get, parameters: params, encoding: URLEncoding.default, headers: headers).validate().responseJSON { (response) in
            print(response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                    error = json["error"] as! String
                }
                completionHandler(nil, error, statusCode)
//                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
            }.resume()
    }
    
    //MARK: - Put
    func MakePutAPI(name:String, params:[String:Any], header:[String:String], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
        print("header:",header)
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Params:-\(JSON(params))")
        
        Alamofire.request(url, method: .put, parameters: params, encoding: URLEncoding.default, headers: header).responseJSON { (response) in
            print(response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? Any, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                    error = json["error"] as! String
                }
                completionHandler(nil, error, statusCode)
//                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
            }.resume()
    }
    
    func MakeDeleteAPI(name:String, params:[String:Any], isAuth:Bool = true, completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
        var headers:[String : String] = [:]
        print(headers)
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        
        Alamofire.request(url, method: .delete, parameters: params, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                    error = json["error"] as! String
                }
                completionHandler(nil, error, statusCode)
//                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
            }.resume()
    }
    
    //MARK: PATCH
    func MakePatchAPI(name:String, params:[String:Any], header:[String:String], completionHandler: @escaping (Any?, String?, Int?)-> Void) {
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        print("header:-\(header)")
        
        Alamofire.request(url, method: .patch, parameters: params, encoding: URLEncoding.default, headers: header).validate().responseJSON { (response) in
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            print("Response: ", response)
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    
                    if let _ = response.result.value as? Any {
                        completionHandler(response.result.value as? Any, nil, statusCode)
                    }
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                    error = json["error"] as! String
                }
                completionHandler(nil, error, statusCode)
            }
        }.resume()
    }
    
    //MARK: - Post with Image upload
    func MakePostWithImageAPI(name:String, params:[String:Any], images:[UIImage], imageName:String = "photo", isAuth:Bool = true, completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void)
    {
//        guard ReachabilityTest.isConnectedToNetwork() else {
//            makeToast(strMessage: "No internet connection available")
//            return
//        }
        
        var headers:[String : String] = [:]
        print(headers)
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for image in images {
                multipartFormData.append(image.jpeg(.medium)!, withName: imageName,fileName: "\(randomString(length: 5)).jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method:.post,
           headers:headers, encodingCompletion: { result in
            
            
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    
                    let statusCode = response.response?.statusCode
                    print("statusCode:-\(statusCode ?? 0)")
                    
                    if(response.error == nil) {
                        completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                    }
                    else {
                        completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                    }
                })
            case .failure( _):
                
                
                
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, 500)
            }
        })
    }
    
    func MakePutWithImageAPI(name:String, params:[String:Any], images:[UIImage], imageName:String = "photo", headers:[String : String], completionHandler: @escaping (NSDictionary?, String?, Int?)-> Void) {
        //        guard ReachabilityTest.isConnectedToNetwork() else {
        //            makeToast(strMessage: "No internet connection available")
        //            return
        //        }
        
        print("headers:", headers)
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Json:-\(JSON(params))")
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for image in images {
                multipartFormData.append(image.jpeg(.medium)!, withName: imageName,fileName: "\(randomString(length: 5)).jpg", mimeType: "image/jpg")
            }
            
            for (key, value) in params {
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to: url, method:.put,
           headers:headers, encodingCompletion: { result in
            
            
            
            switch result {
            case .success(let upload, _, _):
                upload.responseJSON(completionHandler: { (response) in
                    
                    let statusCode = response.response?.statusCode
                    print("statusCode:-\(statusCode ?? 0)")
                    
                    if(response.error == nil) {
                        completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                    }
                    else {
                        completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                    }
                })
            case .failure( _):
                
                
                
                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, 500)
            }
        })
    }
    
    //MARK: - Delete with body
    func makeDeleteApiWithBody(name: String, params:[String:Any], headers:[String:String], completionHandler: @escaping(NSDictionary?, String?, Int?) -> Void) {
        
        print("header:-\(headers)")
        
        let url = mainURL + name
        print("url:-\(url)")
        print("Param:-\(JSON(params))")
        
        request(url, method: .delete, parameters: params, encoding: URLEncoding.httpBody , headers: headers).responseJSON(completionHandler: { (response) in
            print("response:", response)
            
            let statusCode = response.response?.statusCode
            print("statusCode:-\(statusCode ?? 0)")
            
            switch response.result {
            case .success( _):
                if(response.error == nil) {
                    completionHandler(response.result.value as? NSDictionary, nil, statusCode)
                }
                else {
                    completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
                }
            case .failure( _):
                var error = ""
                if let json = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableContainers) as? [String: Any] {
                    error = json["error"] as! String
                }
                completionHandler(nil, error, statusCode)
//                completionHandler(nil, "Server_not_responding_please_try_again_key".localized, statusCode)
            }
        })
    }
}
