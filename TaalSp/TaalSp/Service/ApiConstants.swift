//
//  ApiConstants.swift
//  TaalSp
//
//  Created by Vishal on 17/12/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire
import FirebaseAuth

//MARK: global variables
var quatationData = JSON()
var arrayCategoryList: [TourDataModel] = []
var dataCategoriData = CategoryDataModel(JSON: JSON().dictionaryObject!)
var isCategoryDelete = false

extension UIViewController {
    
    func allProviderCategoriesListApi(completionHandler: @escaping ()-> Void) {
        
        let url = allProviderCategoriesListURL
        let parameters = NSDictionary() as! [String : Any]
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        
        showLoader()
        
        WebServices().MakeGetAPI(name: url, params: parameters, header: header) { (result, error, statusCode) in
            
            self.hideLoader()
            
            arrayCategoryList = []
            if statusCode == success {
                print("Result: ", result)
                if let data = result {
                    var modelData = JSON(data).arrayValue
                    
                    
                    for i in 0..<modelData.count {
                        var dict = modelData[i]
                        dict["isSubCat"].intValue = 0
                        modelData[i] = dict
                    }
                    
                    print("modelData: ", modelData)
                    
                    for data in modelData {
                        print("Data:-\(data)")
                        let categori = TourDataModel(JSON: data.dictionaryObject!)
                        arrayCategoryList.append(categori!)
                    }
                }
                completionHandler()
            }
            if error != nil {
                if statusCode == notFound {
                    completionHandler()
//                    makeToast(strMessage: "Category_not_found_key".localized)
                }
                else {
                    makeToast(strMessage: error!)
                }
            }
            
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 500)")
        }
    }
    
    func logoutApi(param: [String:Any]) {
        
        let url = logoutURL
        var header : [String:String] = [:]
        header["accept"] = "application/json"
        header["Accept-Language"] = "language".localized
        header["Version"] = iosVersion
        header["X-CSRF-TOKEN"] = ""
        header["Authorization"] = "\(getUserData()?.tokenType ?? "")"+" \(getUserData()?.accessToken ?? "")"
        
        guard ReachabilityTest.isConnectedToNetwork() else {
            makeToast(strMessage: "No_internet_connection_available_key".localized)
            return
        }
        self.showLoader()
        
        WebServices().MakePostAPI(name: url, params: param, header: header) { (response, error, statusCode) in
            
            self.hideLoader()
            
            if statusCode == 200 {
                removeUserData()
                userDefault.removeObject(forKey: "idToken")
                appdelegate.arrWayPoint = []
                userDefault.removeObject(forKey: "isTripStarted")
                userDefault.synchronize()
                appdelegate.stopUpdateLocation()
                if let data = response {
                    print("Data:", data)
                    let jsonData = JSON(data)
                    
                    let firebaseAuth = Auth.auth()
                    do {
                        try firebaseAuth.signOut()
                    } catch let signOutError as NSError {
                        print ("Error signing out: %@", signOutError)
                    }
                    
                    let obj = self.storyboard?.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
                    var nav = UINavigationController()
                    nav = UINavigationController(rootViewController: obj)
                    DispatchQueue.main.async {
                        makeToast(strMessage: jsonData["message"].stringValue)
                        appdelegate.window?.rootViewController = nav
                    }
                }
                appdelegate.removeTopNotification()
            }
            else if statusCode == 406 {
                makeToast(strMessage: "Account_deactivated_or_not_approved_key".localized)
            }
            print("Error:-\(error?.localized ?? "")")
            print("statuscode:-\(statusCode ?? 500)")
        }
    }
}
