//
//  AppDelegate.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseDatabase
import Polyline
import FirebaseMessaging
import SwiftyJSON
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    //MARK:- Variable Declaration

    var window: UIWindow?
    var tabBarVC = TaalTabBarVC()
    static let shared = UIApplication.shared.delegate as! AppDelegate
    var locationManager = CLLocationManager()
    var lattitude  = Double()
    var longitude = Double()
    var handlerLocationUpdate:(Double,Double,String) -> Void = {_,_,_ in}
    var arrWayPoint: [CLLocationCoordinate2D] = []
    var ref =  DatabaseReference()
    
    //MARK:- Appdelegate Method

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        FirebaseApp.configure()
        ref = Database.database().reference()
        GMSServices.provideAPIKey(googleMapKey)
        
        let fcmToken = Messaging.messaging().fcmToken
        print("fcmToken:\(fcmToken ?? "")")
        
        Messaging.messaging().delegate = self
        removeTopNotification()
        registerForRemoteNotification()
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

    }

    func applicationDidBecomeActive(_ application: UIApplication) {

    }

    func applicationWillTerminate(_ application: UIApplication) {

    }
    
    func removeTopNotification() {
        if #available(iOS 10.0, *) {
           let center = UNUserNotificationCenter.current()
           center.removeAllDeliveredNotifications()
           center.removeAllPendingNotificationRequests()
        }
        else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    /*
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        if Auth.auth().canHandleNotification(notification) {
            print("Notification: ", notification)
            completionHandler(.noData)
            return
        }
        print("Notification: ", notification)
        // This notification is not auth related, developer should handle it.
    }
      */
    
}



//MARK: Location Manager
extension AppDelegate: CLLocationManagerDelegate {
    
    func setUpQuickLocationUpdate() {
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.distanceFilter = 10
        locationManager.activityType = .fitness
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.startUpdatingLocation()
    }
    
    func stopUpdateLocation() {
//        locationManager = CLLocationManager()
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let latestLocation = locations.first {
            
            if lattitude != latestLocation.coordinate.latitude && longitude != latestLocation.coordinate.longitude {
                lattitude = latestLocation.coordinate.latitude
                longitude = latestLocation.coordinate.longitude
                //                print("lattitude:- \(lattitude), longitude:- \(longitude)")
                
                if let isTripStarted = userDefault.value(forKey: "isTripStarted") as? String,let strConvesationId = userDefault.value(forKey: "convesationId") as? String {
                    print("isTripStarted \(isTripStarted)")
                    let userID : String = strConvesationId
                    var wayPoint:[String:String] = [:]
                    wayPoint = ["way_lat": String(lattitude),
                                "way_lng": String(longitude)]
                    
                    let destination = CLLocationCoordinate2DMake(lattitude, longitude)
                    arrWayPoint.append(destination)
                    //                    print("arrWayPoint \(arrWayPoint)")
                    
                    let polyline = Polyline(coordinates: arrWayPoint)
                    let encodedPolyline: String = polyline.encodedPolyline
                    print("encodedPolyline \(encodedPolyline)")
                    
                    handlerLocationUpdate(lattitude,longitude,encodedPolyline)
                    
                    var dict = [String:Any]()
                    dict["latitude"] = lattitude
                    dict["longitude"] = longitude
                    dict["polyline"] = encodedPolyline
                    ref.child("tracking/\(userID)").updateChildValues(dict)
                    
                    /*let userDeliverLatLong = CLLocation(latitude: kDeliverLatitude, longitude: kDeliverLongitude)
                     
                     let userCurrentLatLong = CLLocation(latitude: kDeliverLatitude, longitude: kDeliverLongitude)
                     
                     let distance = userCurrentLatLong.distance(from: userDeliverLatLong)*/
                    
                    /*if distance <= 20 {
                     automaticFinishCallAPI(polyline: encodedPolyline)
                     }*/
                } else {
                    appdelegate.arrWayPoint = []
                    locationManager.stopUpdatingLocation()
                }
            }
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            // setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            //  setupUserCurrentLocation()
            //  manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            openSetting()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            openSetting()
            break
        default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error :- \(error)")
    }
    
    //MARK:- open Setting
    func openSetting() {
        let alertController = UIAlertController (title: "Taal_key".localized, message: "Go_to_settings_key".localized, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: "Setting_key", style: .default) { (_) -> Void in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            
            if UIApplication.shared.canOpenURL(settingsUrl) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                        print("Settings opened: \(success)") // Prints true
                    })
                } else {
                    // Fallback on earlier versions
                }
            }
        }
        alertController.addAction(settingsAction)
        
        window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
}

//MARK: - Register FCM

extension AppDelegate: MessagingDelegate,UNUserNotificationCenterDelegate {
    
    func registerForRemoteNotification() {
        
        UNUserNotificationCenter.current().delegate = self
        UIApplication.shared.registerForRemoteNotifications()
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                print("granted:==\(granted)")
            }
        }
        else {
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil))
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let chars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
        var token = ""
        
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", arguments: [chars[i]])
        }
        print("Device Token = ", token)
        
        let deviceId = UIDevice.current.identifierForVendor?.uuidString
        print("deviceId:->\(deviceId)")
        
        userDefault.set(token, forKey: "device_token")
        userDefault.synchronize()
        
        Messaging.messaging().apnsToken = deviceToken
        
        print("FCM Token = \(Messaging.messaging().fcmToken ?? "nill")")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Error = ",error.localizedDescription)
        
    }
  
    // While App on Foreground mode......
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo
        print("UserInfo:-.  \(userInfo)")
        print("info:=\(JSON(userInfo))")
        let dictPushData = JSON(userInfo)
        print("convert TO json:=\(JSON.init(parseJSON: dictPushData["custom_data"].stringValue))")
        let dictCustomdata = JSON.init(parseJSON: dictPushData["custom_data"].stringValue)
        
        
        if dictCustomdata["job_status"].stringValue == createCase {
//            handlorNewCreateCase()
            completionHandler([.alert, .badge, .sound])
        }
        else if dictCustomdata["job_status"].stringValue == "send_message" {
            if isOnChatScreen {
                if strOppositeUserId == dictCustomdata["conversation_id"].stringValue{
                    
                } else{
                    completionHandler([.alert, .badge, .sound])
                }
            }
            else {
                completionHandler([.alert, .badge, .sound])
            }
        }
        else if dictPushData["status_id"].stringValue == "7" {
            completionHandler([.alert, .badge, .sound])
        }
        else {
            completionHandler([.alert, .badge, .sound])
        }
    }
    
    // While Banner Tap and App in Background mode..
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = JSON(response.notification.request.content.userInfo)
        print("info:=\(JSON(userInfo))")
        
        let jsonDict = JSON.init(parseJSON: userInfo["custom_data"].stringValue)
        print("JSONDICT: ", jsonDict)
        
        dictNotificationData = jsonDict
        if jsonDict["job_status"].stringValue == createCase {
            appdelegate.tabBarVC = TaalTabBarVC()
            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
            appdelegate.tabBarVC.selectedIndex = 0
            appdelegate.window?.rootViewController = navigation
        }
        else if jsonDict["status_id"].stringValue == "1" {
            isAcceptedCaseNotification = true
            appdelegate.tabBarVC = TaalTabBarVC()
            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
            appdelegate.tabBarVC.selectedIndex = 1
            appdelegate.window?.rootViewController = navigation
        }
        else if jsonDict["status_id"].stringValue == "7" {
            isUserConfirmNotification = true
            appdelegate.tabBarVC = TaalTabBarVC()
            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
            appdelegate.tabBarVC.selectedIndex = 1
            appdelegate.window?.rootViewController = navigation
        }
        else if  jsonDict["job_status"].stringValue == "send_message" {
            if isOnChatScreen == false {
                isNotificationChat = true
                appdelegate.tabBarVC = TaalTabBarVC()
                let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
                appdelegate.tabBarVC.selectedIndex = 2
                appdelegate.window?.rootViewController = navigation
                
            }
        }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification notification: [AnyHashable : Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        if Auth.auth().canHandleNotification(notification) {
//            print("Notification: ", notification)
//            completionHandler(.noData)
//            return
//        }
        
        let userInfo = JSON(notification)
        print("info:=\(JSON(userInfo))")
        
        let jsonDict = JSON.init(parseJSON: userInfo["custom_data"].stringValue)
        print("JSONDICT: ", jsonDict)
        
        dictNotificationData = JSON(jsonDict)
        if jsonDict["job_status"].stringValue == createCase {
            appdelegate.tabBarVC = TaalTabBarVC()
            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
            appdelegate.tabBarVC.selectedIndex = 0
            appdelegate.window?.rootViewController = navigation
        }
        else if jsonDict["status_id"].stringValue == "1" {
            isAcceptedCaseNotification = true
            appdelegate.tabBarVC = TaalTabBarVC()
            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
            appdelegate.tabBarVC.selectedIndex = 1
            appdelegate.window?.rootViewController = navigation
        }
        else if jsonDict["status_id"].stringValue == "7" {
            isUserConfirmNotification = true
            appdelegate.tabBarVC = TaalTabBarVC()
            let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
            appdelegate.tabBarVC.selectedIndex = 1
            appdelegate.window?.rootViewController = navigation
        }
        else if  jsonDict["job_status"].stringValue == "send_message" {
            if isOnChatScreen == false {
                isNotificationChat = true
                appdelegate.tabBarVC = TaalTabBarVC()
                let navigation = UINavigationController(rootViewController: appdelegate.tabBarVC)
                appdelegate.tabBarVC.selectedIndex = 2
                appdelegate.window?.rootViewController = navigation
                
            }
        }
    }
    
    
    
    //MARK: - Firebase Messeging delegate methods
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        print("==== FCM Token:  ",fcmToken)
        connectToFcm()
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func connectToFcm() {
        // Won't connect since there is no token
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                //  self.instanceIDTokenMessage.text  = "Remote InstanceID token: \(result.token)"
            }
        }
        Messaging.messaging().shouldEstablishDirectChannel = false
    }
}


/*
 Build setting -> buil search -> enable bit code = yes if not work
 */
