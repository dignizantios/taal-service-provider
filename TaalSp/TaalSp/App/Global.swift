//
//  Global.swift
//  TaalSp
//
//  Created by Jaydeep on 25/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import FirebaseAuth
import MaterialComponents

//MARK:- Print Fonts

func setTableView(_ message: String, tableView: UITableView) {
    let noDataLabel = UILabel(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: tableView.frame.size.height))
    
    noDataLabel.textAlignment = .center
    noDataLabel.text = message
    noDataLabel.numberOfLines = 0
    noDataLabel.textColor = UIColor.appThemeBlueColor
    noDataLabel.font = themeFont(size: 18, fontname: .regular)
    noDataLabel.center = tableView.center
    tableView.backgroundView = noDataLabel
}


func printFonts() {
    let fontFamilyNames = UIFont.familyNames
    for familyName in fontFamilyNames {
        print("------------------------------")
        print("Font Family Name = [\(familyName)]")
        let names = UIFont.fontNames(forFamilyName: familyName )
        print("Font Names = [\(names)]")
    }
}

//MARK:- StoryBoard

let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let appdelegate = UIApplication.shared.delegate as! AppDelegate
let userDefault = UserDefaults.standard
let googleMapZoomLevel = 15.0
var isOnChatScreen = Bool()
var strOppositeUserId = String()


//let idToken : String = "12345"
var idToken : String = userDefault.value(forKey: "idToken") as! String
var playerID = "123456"
let screenHeight = UIScreen.main.bounds.size.height


//MARK: - Set Toaster
func makeToast(strMessage : String){
    let messageSnack = MDCSnackbarMessage()
    messageSnack.text = strMessage
    MDCSnackbarManager.show(messageSnack)
}

func randomString(length: Int) -> String {
    let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    return String((0...length-1).map{ _ in letters.randomElement()! })
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }
    
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return UIImage().jpegData(compressionQuality: quality.rawValue)
        //        return UIImageJPEGRepresentation(self, quality.rawValue)
    }
    
    func png(_ quality: JPEGQuality) -> Data? {
        return self.pngData()
    }
}

extension String {
    
    public func toURL() -> URL? {
        return URL(string: self)
    }
    
    func toTrim() -> String {
        let trimmedString = self.trimmingCharacters(in: .whitespacesAndNewlines)
        return trimmedString
    }
}


