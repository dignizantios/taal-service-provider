//
//  UIFont+Extension.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

enum themeFonts : String {
    case semiBold = "AdobeClean-BoldSemiCn"
    case regular = "AdobeClean-Regular"
    case semi = "AdobeClean-SemiCn"
    case medium = "AdobeClean-SemiCnIt"
    case roman = "AdobeClean-BoldSemiCnIt"
    case lightIt = "AdobeClean-LightIt"
    case light = "AdobeClean-Light"
    case boldIt = "AdobeClean-BoldIt"
    case bold = "AdobeClean-Bold"
    case it = "AdobeClean-It"
}

extension UIFont
{

}

func themeFont(size : Float,fontname : themeFonts) -> UIFont
{
    if UIScreen.main.bounds.width <= 320
    {
//        if isEnglish {
            return UIFont(name: fontname.rawValue, size: CGFloat(size) - 2.0)!
        /*} else {
            return UIFont(name: themeFonts.arLight.rawValue, size: CGFloat(size) - 2.0)!
        }*/
        
    }
    else
    {
//        if isEnglish {
            return UIFont(name: fontname.rawValue, size: CGFloat(size))!
       /* } else {
           return UIFont(name: themeFonts.arLight.rawValue, size: CGFloat(size))!
        }*/
    }
    
}
