//
//  Enum+Extension.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

enum enumForInvoiceDetail{
    case invoice
    case pending
    case accepted
    case addQuotation
}

enum enumForLoginRegisterOTP {
    case update
    case login
    case register
}

enum enumCheckRequestDetailWhichVC {
    case home
    case MyRequest
}

enum enumAddEditQuatation {
    case add
    case editPending
    case editAccepted
}

enum enumHistoryParent {
    case fromTab
    case fromProfile
}
