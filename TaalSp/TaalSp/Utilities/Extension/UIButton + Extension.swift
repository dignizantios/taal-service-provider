//
//  UIButton + Extension.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit


extension UIButton
{
    
    func setupThemeButtonUI(backColor:UIColor=UIColor.appThemeBlueColor,font:UIFont=themeFont(size: 20, fontname: .regular),fontColor:UIColor = .white)
    {
        self.setTitleColor(fontColor, for: .normal)
        self.backgroundColor = backColor
        self.titleLabel?.font = font
    }
    
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
        
    }
    
}
