//
//  UIColor+Extension.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import UIKit

extension UIColor {

    static var appThemeBlueColor: UIColor { return UIColor.init(red: 13/255, green: 65/255, blue: 100/255, alpha: 1.0) }
    static var appThemeOrangeColor: UIColor { return UIColor.init(red: 247/255, green: 148/255, blue: 29/255, alpha: 1.0) }
    static var appThemeGreenColor: UIColor { return UIColor.init(red: 0/255, green: 166/255, blue: 81/255, alpha: 1.0) }
    static var appThemeLightOrangeColor: UIColor { return UIColor.init(red: 245/255, green: 106/255, blue: 78/255, alpha: 1.0) }
    static var appThemeChatOrangeColor: UIColor { return UIColor.init(red: 254/255, green: 105/255, blue: 82/255, alpha: 1.0) }
    static var appThemeRedColor: UIColor { return UIColor.init(red: 237/255, green: 28/255, blue: 36/255, alpha: 1.0) }
    static var appThemeLightBlueColor: UIColor { return UIColor.init(red: 0/255, green: 118/255, blue: 163/255, alpha: 1.0) }
    static var appThemeBGColor: UIColor { return UIColor.init(red: 248/255, green: 248/255, blue: 247/255, alpha: 1.0) }
  
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
   

