//
//  NSNotificationName+Extension.swift
//  TaalSp
//
//  Created by Jaydeep on 21/11/19.
//  Copyright © 2019 Jaydeep. All rights reserved.
//

import Foundation
import UIKit

extension Notification.Name {
    static let didRefreshOrderList = Notification.Name("refreshOrderList")
    static let didRefreshOrderDetailScreen = Notification.Name("refreshOrderDetail")
    static let manageAddCartButton = Notification.Name("manageAddToCartButton")
}
